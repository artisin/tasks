//***************************************************************/
/* Publications */
/***************************************************************/
//////////////////*********** TODO ***********//////////////////
/// 1. Check!!!
/// 2. Seperate into logical files
///
//////////////////*********** TODO ***********//////////////////

//For publishing more then just the defults for Meteor.users
// Meteor.publish("userData", function () {
//   if (this.userId) {
//     return Meteor.users.find({_id: this.userId},
//                              {fields: {'roles': 1}});
//   } else {
//     this.ready();
//   }
// });

//publishes roles to every client without needing a subscription
Meteor.publish(null, function (){ 
  return Meteor.roles.find({});
});

//Publish user group
Meteor.publish("usergroup", function(filter) {
  check(arguments, [Match.Any]);
  return UserGroup.find(filter);
});


/***************************************************************/
/* Notification Subs */
/***************************************************************/
/*******************************/
/* New Message Count */
/*******************************/
Meteor.publish('newMessageCount', function () {
  var userId = this.userId;
  //In if statement otherwise it will throw an error when the user logs out
  if(typeof userId === 'string'){
    var options = {hasNotRead: userId},
        cursor = Messages.find(options);
    Counts.publish(this, 'newMessageCount', cursor);
  }
});
/*******************************/
// Inbox Message Count
/*******************************/
Meteor.publish('inboxMessageCount', function () {
  var userId = this.userId;
  //In if statement otherwise it will throw an error when the user logs out
  if(typeof userId === 'string'){
    var options = {recipiant_Id: userId},
        cursor = Messages.find(options);
    Counts.publish(this, 'inboxMessageCount', cursor);
  }
});




Meteor.publish('messages', function (cursor, options, limit) {
  check(arguments, [Match.Any]);
  console.log(arguments);
  //If limit apply it
  if (_.isNumber(limit)) {
    options.limit = limit;
  }
  return Messages.find(cursor, options);
});



//Message Thread
Meteor.publish('messageThread', function(cursor, sort, limit) {
  check(arguments, [Match.Any]);
  return Messages.find(cursor, sort, {limit: limit});
});

//Tags
Meteor.publish("tags", function() {
  check(arguments, [Match.Any]);
  // var options = {},
  //     key = 'users.'+ this.userId,
  //     value = 1;
  // options[key] = value;
  // options.tagName = 1;
  // options.tagColor = 1;
  var userId = this.userId;
  return Tags.find({user_Id: userId});
});



// Meteor.publishComposite('messages', function(cursor, options, limit){
//   check(arguments, [Match.Any]);
//   console.log(arguments)
//   //If limit apply it
//   if (_.isNumber(limit)) {
//     options.limit = limit;
//   }
//   return{
//     find: function() {
//       return Messages.find(cursor, options);
//     },
//     children: [
//       {
//         //Creates a local tag collection with the sub of messages
//         //For the diplay of the tag on the indv message level
//         collectionName: "localTags",
//         find: function(message) {
//           var selector = {},
//               key = 'users.'+this.userId,
//               value = message._id;
//           selector[key+'.inDocs'] = value;
//           return Partitioner.directOperation(function(){
//             var tagFields = {},
//                 userField = key,
//                 valueField = 1;
//             tagFields[userField] = valueField;
//             tagFields.tagName = 1;
//             tagFields.tagColor = 1;
//             return Tags.find(selector, {fields: tagFields});
//           });
//         }
//       }
//     ]
//   }
// })