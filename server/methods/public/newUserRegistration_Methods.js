
Accounts.onCreateUser(function(options, user) {
//Audit argument
//Null is failing to pass check as an object
//So I might come back and fix this latter but for not it works
check(options.token, Match.Any);
var groupRole = {},
    newGroupToken = Random.hexString(8);
groupRole[newGroupToken] = ['Admin'];
  //If no token assume admin
  //and create a custom group
  if (options.token === null) {
    Partitioner.setUserGroup(user._id, newGroupToken);
    user.roles = groupRole;
    Partitioner.directOperation(function () {
      return UserGroup.insert({
        firstName: 'Admin',
        lastName: 'Admin',
        email: {
          address:user.emails[0].address,
          verified: false
        },
        _groupId: newGroupToken,
        invitedSent: true,
        accountCreated: true,
        timestamp: Date.now()
      });
    });
    return user;
  }else{
    return user;
  }
});

Meteor.methods({
  /*******************************/
  /* New User Registration */
  //User who register with no token
  /*******************************/
  newUserRegistration: function (newUser) {
    check(newUser, {
      username: String,
      email: String,
      password: String,
      token: null
    });

    var valContext = Schema.newUser.namedContext('tokenRegForm');
    //Validation by simpleSchema (lib/schema/userSchema.js)
    if (!valContext.validate(newUser)) {
      var keys = valContext.invalidKeys();
      _.each(keys, function (value) {
        var error = value.name,
            message = valContext.keyErrorMessage(error);
        throw new Meteor.Error(error, message);
      });
    }else{
      return;
    }
  },
  /*******************************/
  /* Token Registration method*/
  // This is for users who enter a group token
  /*******************************/
  tokenRegistration: function(newUser) {
    check(newUser, {
      username: String,
      email: String,
      password: String,
      token: String
    });

    var adminInvite,
        valContext = Schema.newUser.namedContext('tokenRegForm');
    
    //Find the mathing token
    Partitioner.directOperation(function () {
      var options = {
        fields: {"_id": 1,"token": 1,"firstName": 1,"group": 1,"role": 1, }};
        adminInvite = UserGroup.findOne({token: newUser.token}, options);
    });
    
    //If no token match throw error
    if (!adminInvite) {
      throw new Meteor.Error("Bad Token Match", "Your Token did not match any tokens we have on file");
    }else if (!valContext.validate(newUser)) {
      //Validation by simpleSchema (lib/schema/userSchema.js)
      var keys = valContext.invalidKeys();
      _.each(keys, function (value) {
        var error = value.name,
            message = valContext.keyErrorMessage(error);
        throw new Meteor.Error(error, message);
      });
    }else {
      
      //Create the account
      var id = Accounts.createUser({
        username: newUser.username,
        email: newUser.email,
        password: newUser.password,
        firstName: adminInvite.firstName,
      });
      
      //Set index for new user
      Partitioner.setUserGroup(id, adminInvite.group);
      
      //Set role for the new user
      Roles.addUsersToRoles(id, [adminInvite.role], adminInvite.group);

      //Update adminInvite collection
      return UserGroup.update(adminInvite._id, {
        $set: {
          accountCreated: true
        },
        $unset: {
          token: ""
        }
      });
    }
  }
});