//////////////////*********** TODO ***********//////////////////
/// 1. I might want to remove the user from the admin invite
/// collection once the account is created. Think about it.
///
//////////////////*********** TODO ***********//////////////////


Meteor.methods({
  /*******************************/
  /* Send Invite Method */
  /* This is executed in the admin profile
  /*******************************/
  sendInvite: function (invitee) {
    check(invitee,{
      firstName: String,
      lastName: Match.Any, 
      email: String,
      phone: Match.Any, 
      role: String,
      company: Match.Any, 
      group: String,
      url: String});
    var token = Random.hexString(10);
    var url = invitee.url;
    //Insert new invitee into coloection
    return UserGroup.insert({
        firstName: invitee.firstName,
        lastName: invitee.lastName,
        email: {
          adress:invitee.email,
          verified: false
        },
        phone: invitee.phone,
        company: invitee.company,
        token: token,
        role: invitee.role,
        group: invitee.group,
        invitedSent: false,
        accountCreated: false,
        timestamp: Date.now()
    }, function(err){
      if(err){
        return console.error(err);
      }else {
        //Complie the email Template
        SSR.compileTemplate('sendInvite', Assets.getText('email/invite.html'));
        var emailTemplate = SSR.render('sendInvite', {
          token: token,
          url: url,
          urlWithToken: url + ("/" + token)
        });
        //Send the email
        Meteor.defer(function () {
          Email.send({
            to: invitee.emailAddress,
            from: "App Name <invite@app.com>",
            subject: "App Invite",
            html: emailTemplate
          });
        });
      }
    });
  },
  /*******************************/
  /* Invite Update */
  //Upon sending out the email udate the
  //adminInvite collection to reflect email has been sent
  /*******************************/
  updateInviteAccount: function(id){
    check(id, String);
    return UserGroup.update(id, {
      $set:{invitedSent: true}
    });
  }
});