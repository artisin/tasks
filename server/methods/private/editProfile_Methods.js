//////////////////*********** TODO ***********//////////////////
/// 1. check if it is nessary to verify the id
///
//////////////////*********** TODO ***********//////////////////

Meteor.methods({
  editProfile: function (params) {
    check(params, {
      _id: String,
      valueChange: String,
      newValue: String
    });
    var group = Partitioner.getUserGroup(this.userId),
        isAdmin = Roles.userIsInRole(this.userId, ['Admin'], group),
        profile = UserGroup.findOne({_userId: params._id}),
        newValue = params.newValue,
        selector = {};

    if (!(isAdmin || this.userId === params._id)) {
      throw new Meteor.Error(403, "Access denied.");
    }

    //Checks to see if there is a period 
    //since will need to be altered to be checked by collection2
    if (params.valueChange.indexOf('.') !== -1) {
      var keyToChange = params.valueChange;
          newValue = params.newValue;

      //The Following Tom-Fuckery code is thanks to collection2 shit ass
      //job in explained said schema standards. And although, I could 
      //refractor all this code I am not going to right now just cus I spent
      //all fucking night long playing with this god damn shit and I am just
      //a bit wound plus the following code is some silly shit so for historical
      //reason I will continue to use it. 
      var keyParts = [];
      var indices = [];
      //The following code will take a string and seperate it into parts
      //based on periods. (fuck.you.collection2) === [fuck, you, collection2]
      for(var i=0; i<keyToChange.length;i++) {
          if (keyToChange[i] === "."){
            if (indices.length === 0) {
              indices.push(i);
              keyParts.push(keyToChange.substr(0, i));
            }else{
              indices.push(i);
            }
          }
      }
      for (var j = 0; j < indices.length; j++) {
        keyParts.push(keyToChange.substr(indices[j]+1, (indices[j+1] - indices[j])-1));
        if (j+1 === indices.length) {
          keyParts.push(keyToChange.substr(indices[j]+1, indices[(j+1)]));
        }
      }

      //Clean and seperate keys values
      var keys = _.compact(keyParts),
          firstKey = _.first(keys),
          secondaryKeys = _.map(_.rest(keys), function (value) {
            return [value];
          }),
          keyTree = profile,
          value = keyTree[firstKey];
      
      //Asigns the new value
      //Hacky, refractor when time
      if (secondaryKeys.length === 1) {
        value[secondaryKeys] = params.newValue;
      }
      if (secondaryKeys.length === 2) {
        value[secondaryKeys[0]][secondaryKeys[1]] = params.newValue;
      }
      selector[firstKey] = value;
      UserGroup.update(profile._id, {$set: selector});
    }else{
      //Change value in UserGroup which is checked via a schema
      var key = params.valueChange;
          selector[key] = newValue;
      UserGroup.update(profile._id, {$set: selector});
      
    }

    
    //Check to see if the user role is being changed
    if (params.valueChange === 'role') {
      //Check to make sure the user is not dumb enough to
      //change their admin role and leaves no standing admin
      //this is an edge case but there are dumb people
      if (params.newValue !== 'Admin') {
        //Expensive Operation, possibly change
        var currentAdminCount = Roles.getUsersInRole('Admin', group).count();
        if (currentAdminCount === 1) {
          throw new Meteor.Error(403, "You currently are the only Admin and there must be at least one Admin")
        }else{
          Roles.setUserRoles(params._id, [params.newValue], group);
        }
      }
      Roles.setUserRoles(params._id, [params.newValue], group);
    }
      
  }
});