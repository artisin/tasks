Meteor.methods({
  tagFolderFilter: function (id) {
    check(arguments, [Match.Any]);
    var options = {},
        userId = this.userId,
        key = 'users.'+ userId,
        value = 1;
    options[key] = value;

    //Id is an array of id strings
    var messages = Tags.find({$or: id}, {fields: options}).fetch();
    messages = _.flatten(messages);

    var exsistingInDocs,
        output = [],
        messageCheck = _.map(messages, function (value) {
          if (value.users[userId] !== undefined) {
            return value.users[userId].inDocs !== undefined;
          }
        });

    //Initial check to make sure users has that tag inDocs
    if (!_.contains(messageCheck, false)) {
      //Finds all the matching documents
      var findAllDocs = function (docs) {
        return _.map(docs, function (value) {
          if (value.users[userId] === undefined) {
            return false;
          }
          return value.users[userId].inDocs;
        });
      };
      exsistingInDocs = findAllDocs(messages);
      _.each(exsistingInDocs, function (v, k, l) {
        //Push first group
        if (k === 0) {
          output.push(v);
        }else{
          //Compare the arrays to find common
          output = _.intersection(_.flatten(output), l[k]);
        }
      });
    }

    return _.flatten(output);
  }
});
//Tags.find({$or: filterQuerie}, {fields: options}).fetch();
//Tags.find({$and:[{_id:{$in:filterQuerie}}]}, {fields: options}).fetch();
//[ { _id: 'J54bsgKNZLMpiWRtD' }, { _id: 'yLCL8LmRLoQ2u6Sud' } ]
