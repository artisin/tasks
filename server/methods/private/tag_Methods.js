
Meteor.methods({
  /***************************************************************/
  /* Tag Methods */
  /***************************************************************/
  /*******************************/
  /* Add Message Tag */
  /*******************************/
  addMessageTag: function (tagObj) {
    check(tagObj, {
      tag_Id: String,
      doc_Id: Array,
    });

    var userId = this.userId,
        tagId = tagObj.tag_Id,
        docId = tagObj.doc_Id;

    /*******************************/
    // Update Tag Collection
    /*******************************/
    _.each(docId, function (_docId) {
     Tags.update({user_Id: userId, 'tags.tag_Id': tagId}, {
        $inc: {'tags.$.frequency': 1},
        $addToSet: {'tags.$.inDocs': _docId}
      });
    });

    /*******************************/
    // Update doc collection
    /*******************************/
    var docTagSelector = {},
        docTagKey = 'tags.'+userId;
    docTagSelector[docTagKey] = tagId;

    _.each(docId, function (_docId) {
      Messages.update({_id: _docId}, {$addToSet: docTagSelector});
    });

  },

  /*******************************/
  // Generate and Add new Tag
  /*******************************/
  addNewTag: function (tagObj) {
    check(tagObj, {
      tagName: String,
    });
    console.log('ran')
    var newTagName = tagObj.tagName,
        userId = this.userId,
        tagObjId;

    var findCurrentTags = function () {
      var tagObj = Tags.find({user_Id: userId}).fetch(),
          currentTags = tagObj[0].tags;
      //Set tagObj Id
      tagObjId = tagObj[0]._id;
      return currentTags;
    };

    var checkIfTagExsits = function () {
      var currentTags = findCurrentTags(),
          tagNames = _.map(currentTags, function (value) {
            return value.tagName;
          }),
          tagDoesNotExsits;
        _.each(tagNames, function (name) {
          if (name.toString().toLowerCase() === newTagName.toLowerCase()) {
            tagDoesNotExsits = false;
            throw new Meteor.Error(403, 'You already have a tag called '+name);
          }else{
            tagDoesNotExsits = true;
          }
        });
      return tagDoesNotExsits;
    };

    var generateNewTag = function () {
        
        //Generates Random Pantone Color for new labels 
        function tagColorGenerator () {
          var r = (Math.round(Math.random()* 127) + 127).toString(16);
          var g = (Math.round(Math.random()* 127) + 127).toString(16);
          var b = (Math.round(Math.random()* 127) + 127).toString(16);
          return '#' + r + g + b;
        }
      //In tag does not exsit create the new tag obj
      if (checkIfTagExsits()) {
        var newTagId = Random.id(),
            tagInfo = {
              tag_Id: newTagId,
              tagName: newTagName,
              tagColor: tagColorGenerator(),
              frequency: 0,
              inDocs:[],
              group: []
            };
        return {
          newTagObj: tagInfo,
          newTagId: newTagId,
        };
      }
    };

    var addNewTag = function  () {
      var newTag = generateNewTag(),
          newTagObj = newTag.newTagObj,
          newTagId = newTag.newTagId;
  

      //Add to tag collection
      Tags.update(tagObjId, {
        $addToSet: {
          'tags': newTagObj,
          'tagNames': newTagName,
          'tagIds': newTagId
        }
      });

      return newTag;
      // //Add to message Collection
      //  var docTagSelector = {},
      //      docTagKey = 'tags.'+userId;
      //  docTagSelector[docTagKey] = newTagId;

      //  Messages.update({_id: docId}, {$addToSet: docTagSelector});
    };

    return addNewTag();

  },

  /*******************************/
  /* Remove Message Tag */
  /*******************************/
  removeMessageTag: function (tagObj) {
    check(tagObj, {
      tag_Id: String, 
      doc_Id: Array
    });
    var userId = this.userId,
        tagId = tagObj.tag_Id,
        docId = tagObj.doc_Id;


    /*******************************/
    // Remove from tag collection
    /*******************************/
    _.each(docId, function (_docId) {
     Tags.update({user_Id: userId, 'tags.tag_Id': tagId}, {
        $inc: {'tags.$.frequency': -1},
        $pull: {'tags.$.inDocs': _docId}
      });
    });


   /*******************************/
   // Remove From doc collection
   /*******************************/
   var docTagSelector = {},
       docTagKey = 'tags.'+userId;
   docTagSelector[docTagKey] = tagId;

    _.each(docId, function (_docId) {
      Messages.update({_id: _docId}, {$pull: docTagSelector});
    });
  }
});