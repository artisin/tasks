//////////////////*********** TODO ***********//////////////////
/// 1. TAgs + stared
/// 2. Refractor
///
//////////////////*********** TODO ***********//////////////////

Meteor.methods({
  //Sender direct message
  newDirectMessage: function (newMessage) {
    check(newMessage, {
      recipiants: Array,
      sender: String,
      subject: String,
      message: String
    });
    var options= {fields: {firstName:1,lastName: 1, _userId:1}},
        sender = UserGroup.findOne({_userId: newMessage.sender}, options),
        recipiants = _.map(newMessage.recipiants, function (_id) {
           return UserGroup.findOne({_userId: _id}, options);
        }),
        //Puts the recipiants into a array of objs so the lookup on the 
        //client is more efficent for recipiant listing / and thier avatars
        recipiantList = _.map(recipiants, function (user) {
          var userObj = {},
              key = user._userId,
              value = (user.firstName+" "+user.lastName);
              userObj[key] = value;
          return userObj;
        });


    var params = {
      timestamp: Date.now(),
      senderName: (sender.firstName+" "+sender.lastName),
      sender_Id: sender._userId,
      recipiant_Id: newMessage.recipiants,
      recipiantList: recipiantList,
      subject: newMessage.subject, 
      message: newMessage.message,
      hasNotRead: newMessage.recipiants,
      hasRead: [sender._userId],
      parent:  [null],
      tags: {},
      path: []
    };
    //Insert
    var _id = Messages.insert(params);
    //Add path
    Messages.update(_id, {$addToSet: {path: _id}});
  },
  /***************************************************************/
  /* Message Actions (da buttons) */
  /***************************************************************/
  /*******************************/
  /* Message Reply */
  /*******************************/
  'messageReply': function (params) {
    check(params, {
      messageReply: String,
      messageId: String,
      parent: Array,
      path: Array,
      recipiant_Id: Array,
      recipiantList: Array,
      previousSender_Id: String,
      previousSenderName: String,
      subject:  String
    });

    var userId = this.userId,
        path = params.path,
        userPlacement,
        recipiant_Id;
    
    //If recipiant is replying to sender
    if (params.previousSender_Id !== userId) {
      //Remove sender from recipian_Id list
      recipiant_Id = _.without(params.recipiant_Id, userId);
      //Add new path
      //path.push(params.messageId);
      //Added privous sender to recipiant_Id list
      recipiant_Id.push(params.previousSender_Id);

      //Finds placment of all involed parties
      userPlacement = function () {
        var recipiantList = params.recipiantList,
            senderName;
          //Remove sender from recipiant list
          _.each(recipiantList, function (value, key) {
            if (_.keys(value).toString() === userId) {
              //Est new sender name
              senderName = _.values(value).toString();
              //Remove new sender from recip list
              delete recipiantList[key];
            }
          });
      
        //Added previous sender to recip list
        var selector = {},
            key = params.previousSender_Id,
            value = params.previousSenderName;
        selector[key] = value;
        recipiantList.push(selector);


        return {
          recipiantList: _.compact(recipiantList),
          senderName: senderName
        };
      };
    }

    //If sender is replying to his own message
    if (params.previousSender_Id === userId) {
      //Same as befor
      recipiant_Id = params.recipiant_Id;

      userPlacement = function () {
        return {
          recipiantList: params.recipiantList,
          senderName: params.previousSenderName,
        };
      };
    }

    //Parent
    var findParent = function () {
      var parent;
      //If no parent, parent is first message
      if (params.parent[0] === null) {
        parent = [params.messageId];
      }else{
        parent = params.parent;
      }
      return parent;
    };

    //Generates Reply Obj
    var generateReply = function () {
      var users = userPlacement(),
          parent = findParent(),
          timestamp = Date.now();
      return {
        message: params.messageReply,
        parent: parent,
        path: path,
        recipiant_Id: recipiant_Id,
        recipiantList: users.recipiantList,
        sender_Id: userId,
        senderName: users.senderName,
        hasNotRead: recipiant_Id,
        hasRead: [userId],
        timestamp: timestamp,
        tags: params.tags,
        subject: 'RE: '+params.subject
      };
    };
    //Reply obj
    var replyObj = generateReply();

    console.log(replyObj)

    var _id = Messages.insert(replyObj);
    //Add path
    Messages.update(_id, {$addToSet: {path: _id}});
  },
  /*******************************/
  /* Marked Message as read */
  /*******************************/
  markMessageAsRead: function (idArray) {
    check(idArray, Array);
    var user = this.userId;
    //Remove message from hasNot and place in reads
    _.each(idArray, function (value) {
      Messages.update(value, {
        $pull: {hasNotRead: user},
        $addToSet: {hasRead: user}
      });
    }); 
  },
  /*******************************/
  /* Mark Message as unread */
  /*******************************/
  markMessageAsUnRead: function (idArray) {
    check(idArray, Array);
    var user = this.userId;
    //Remove message from hasNot and place in reads
    _.each(idArray, function (value) {
      Messages.update(value, {
        $pull: {hasRead: user},
        $addToSet: {hasNotRead: user}
      });
    }); 
  },
  /*******************************/
  // Delete Message
  /*******************************/
  deleteMessage: function (params) {
    check(params, {
      messages: Array
    });
    var messageId = params.messages,
        userId = this.userId,
        fieldCheck = ['hasNotRead', 'hasRead', 'recipiant_Id', 'stared', 'tags'];


    _.each(messageId, function (id) {
      var message = Messages.findOne({_id: id}),
          isFieldEmpty;

        //Callback for asyncOrder
        var cb = function (funk) {
          return function (next) {
            funk();
            next();
          };
        };

        var removeTags = function () {
          var tagsList = message.tags[userId];
          console.log(messageId)
          //Remove from tag collection
          _.each(tagsList, function (tagId) {
            Tags.update({user_Id: userId, 'tags.tag_Id': tagId}, {
              $inc: {'tags.$.frequency': -1},
              $pull: {'tags.$.inDocs': id}
            });
          });
        };

        //Need to them remove tag
        var removeFromField = function () {
            _.each(fieldCheck, function (field) {
            //Find out where user id exists in field
            if (_.contains(message[field], userId)) {
              var selector = {};
              selector[field] = userId;

              //Remove user from said field
              //Update message var
              message[field] = _.without(message[field], userId);
              //Update collection
              Messages.update({_id: id}, {$pull: selector});
            }
          });
        };


      var checkField = function () {
        isFieldEmpty = _.map(fieldCheck, function (field) {
          if (message[field] !== undefined) {
            if (message[field].length === 0) {
              return true;
            }else{
              return false;
            }
          }
        });
      };


    var removeMessage = function () {
      if (_.every(isFieldEmpty, _.identity)) {
        console.log('message removed')
        Messages.remove(id);
      }
    };

    var asyncOrder = [
      cb(removeTags), 
      cb(removeFromField), 
      cb(checkField), 
      cb(removeMessage)
    ];

    _(asyncOrder).reduceRight(_.wrap, function() {return true})();

    
    });


  },
  addMessageStar: function (params) {
    console.log('ran')
    check(params, {
      docId: String
    });
    var _id = params.docId,
        userId = this.userId;

    Messages.update(_id, {
      $addToSet: {
        stared: userId
      }
    });
  },
  removeMessageStar: function (params) {
    check(params, {
      docId: String
    });
    var _id = params.docId,
        userId = this.userId;

    Messages.update(_id, {
      $pull: {
        stared: userId
      }
    });

  }
});