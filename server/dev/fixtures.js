/***************************************************************/
/***************************************************************/
/*  Startup
/*  Collection of methods and functions to run on server startup.
/***************************************************************/
/***************************************************************/
/***************************************************************/
/* Generate Admin Account */
/***************************************************************/
Meteor.startup(function () {
var fakeFixture = {
  getPhone: function () {
    return Math.floor(1000000000 + Math.random() * 900000000);
  },
  getName: function() {
    var namesLength = this.names.length,
        name = this.names[Math.floor(Math.random() * namesLength)];
    return this.capitalize(name);
  },
  getWord:  function(min, max) {
    var length = this.wordLengths[Math.floor(Math.random() * 16)];
    if(min && (length < min)) length = min;
    if(max && (length > max)) length = max;
    var word = '';
    for(var i = 0; i < length; ++i) {
      var count = this.syllabeCounts[Math.floor(Math.random() * 16)];
      word += this.syllabes[Math.floor(Math.random() * count)];
    }
    return word;
  },
  getLocation: function () {
    var location = this.fromArray(this.cities);
    return {
      city: location[0],
      state: location[1]
    };
  },
  getSentence: function(length) {
    if(!length) {
      var length = 4 + Math.floor(Math.random() * 8);
    }
    var ending = (Math.random() < 0.95) ? '.' : (Math.random() < 0.5) ? '!' : '?';
    var result = this.getWord();
    result = result.slice(0,1).toUpperCase() + result.slice(1).toLowerCase();
    for(var i = 1; i < length; ++i) {
      result += ' ' + this.getWord();
    }
    return result + ending;
  },
  getParagraph: function(length) {
    if(!length) {
      length = 6 + Math.floor(Math.random() * 8);
    }
    var result = this.getSentence();
    for(var i = 1; i < length; ++i) {
      result += ' ' + this.getSentence();
    }
    return result;
  },
  getDomain: function() {
    return this.getWord(2) + this.domains[Math.floor(Math.random() * 8)];
  },
  getEmail: function () {
    return (this.getName() + '@' +this.getDomain()).toLowerCase();
  },
  getCompany: function () {
    var name = this.getWord(5, 12);
    return this.capitalize(name);
  },
  getRandomDate: function(start, end) {
    if (!start) {
        start = new Date(1900, 0, 1).getTime();
    } else {
        start = start.getTime();
    }
    if (!end) {
        end = new Date(2100, 0, 1).getTime();
    } else {
        end = end.getTime();
    }
    return new Date(start + Math.random() * (end - start));
    //return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
  },
  getBollen: function () {
    return Math.round(Math.random()) === 0 ? true : false;
  },
  capitalize: function(str) {
    return str.slice(0,1).toUpperCase() + str.slice(1).toLowerCase();
  },
  fromArray: function(array) {
    return array[Math.floor(Math.random() * array.length)];
  },
  syllabes: [
  'the','ing','er','a','ly','ed','i','es','re','tion','in','e','con','y','ter','ex','al','de','com','o','di','en','an','ty','ry','u',
  'ti','ri','be','per','to','pro','ac','ad','ar','ers','ment','or','tions','ble','der','ma','na','si','un','at','dis','ca','cal','man','ap',
  'po','sion','vi','el','est','la','lar','pa','ture','for','is','mer','pe','ra','so','ta','as','col','fi','ful','get','low','ni','par','son',
  'tle','day','ny','pen','pre','tive','car','ci','mo','an','aus','pi','se','ten','tor','ver','ber','can','dy','et','it','mu','no','ple','cu',
  'fac','fer','gen','ic','land','light','ob','of','pos','tain','den','ings','mag','ments','set','some','sub','sur','ters','tu','af','au','cy','fa','im',
  'li','lo','men','min','mon','op','out','rec','ro','sen','side','tal','tic','ties','ward','age','ba','but','cit','cle','co','cov','daq','dif','ence',
  'ern','eve','hap','ies','ket','lec','main','mar','mis','my','nal','ness','ning','nu','oc','pres','sup','te','ted','tem','tin','tri','tro','up',
  ],
  wordLengths: [
    1, 1,
    2, 2, 2, 2, 2, 2, 2,
    3, 3, 3, 3,
    4, 4,
    5
  ],
  syllabeCounts: [
    10,15,20,25,
    30,35,40,45,
    50,75,100,125,
    150,175,175,175
  ],
  names: [
    'Abigail','Alice','Amelia','Angelina','Ann',
    'Ashley','Avery','Barbara','Brianna','Camila',
    'Chloe','Dorothy','Elizabeth','Ella','Emily',
    'Emma','Fiona','Florence','Gabrielle','Haley',
    'Hannah','Isabella','Jasmine','Jennifer','Jessica',
    'Juliette','Kate','Leah','Lily','Linda',
    'Lea','Madison','Makayla','Margaret','Maria',
    'Mariana','Mary','Megan','Mia','Olivia',
    'Patricia','Rachel','Samantha','Sarah','Sophie',
    'Susan','Taylor','Valeria','Victoria','Zoe',
    'Alexander','Anthony','Benjamin','Brandon','Carter',
    'Charles','Charlie','Christian','Christopher','Daniel',
    'David','Deven','Dylan','Elijah','Eric',
    'Ethan','Felix','Gabriel','George','Harry',
    'Hudson','Hunter','Jack','Jacob','James',
    'Jason','Jayden','Jeremiah','John','Joseph',
    'Joshua','Justin','Kevin','Liam','Logan',
    'Lucas','Matthew','Michael','Neil','Noah',
    'Oliver','Owen','Raphael','Richard','Robert',
    'Ryan','Samuel','Thomas','Tyler','William'
  ],
   domains: [
  '.net', '.org', '.edu', '.com',
  '.com', '.com', '.com', '.com',
  ],
  cities: [
    ['New York', 'New York'],
    ['Los Angeles', 'California'],
    ['Chicago', 'Illinois'],
    ['Houston', 'Texas' ],
    ['Philadelphia', 'Pennsylvania'] ,
    ['Phoenix', 'Arizona'],
    ['San Diego', 'California'],
    ['Dallas', 'Texas'],
    ['San Antonio', 'Texas' ],
    ['Detroit', 'Michigan'],
    ['San Jose', 'California'],  
    ['Indianapolis', 'Indiana'], 
    ['San Francisco', 'California'], 
    ['Jacksonville', 'Florida'], 
    ['Columbus', 'Ohio '], 
    ['Austin', 'Texas'], 
    ['Memphis', 'Tennessee'],  
    ['Baltimore', 'Maryland'], 
    ['Milwaukee', 'Wisconsin'],  
    ['Boston', 'Massachusetts'], 
    ['Charlotte', 'North Carolina'],
    ['El Paso', 'Texas'],  
    ['Washington', 'D.C.'],  
    ['Seattle', 'Washington'],
    ['Fort Worth', 'Texas'], 
    ['Denver', 'Colorado'],  
    ['Nashville-Davidson', 'Tennessee'], 
    ['Portland', 'Oregon'],  
    ['Oklahoma City', 'Oklahoma'],
    ['Las Vegas', 'Nevada'], 
  ]
};

  //Define MAIL_URL
  // process.env.MAIL_URL = 'smtp://artisin%40sandbox0e0e31b028ae4f5892566602df5d95b7.mailgun.org:123456@smtp.mailgun.org:587'

   if (Meteor.users.find().fetch().length === 0) {
    var allIds = [];
    var adminId = [];

    var createFakeUser = function (group) {
      var name = fakeFixture.getName(),
          phone = function () {
            if (Math.round(Math.random()) === 0 ? true : false) {
              return fakeFixture.getPhone();
            }else{
              return null;
            }
          };
      return {
        firstName: name,
        lastName: fakeFixture.getName(),
        email: {
          address: fakeFixture.getEmail(),
          verified: fakeFixture.getBollen()
        },
        phone: {
          cell: fakeFixture.getPhone(),
          work: phone(),
          home: phone(),
        },
        displayName: name,
        role: fakeFixture.fromArray(['Admin']),
        // role: fakeFixture.fromArray(['Viewer', 'User', 'Manager', 'Admin']),
        invitedSent: true,
        accountCreated: true,
        identicon: Random.hexString(6),
        timestamp: fakeFixture.getRandomDate(new Date(2012, 0, 1), new Date()),
        profile: {
          location: fakeFixture.getLocation(),
          company: fakeFixture.getCompany(),
          bio: fakeFixture.getParagraph()
        },
        _groupId: group
      };
    };

    var createFakeUserGroup = function (size, groupName) {
      var userGroup = [];
      for (var i = 0; i < size; i++) {
        userGroup.push(createFakeUser(groupName));
      }
      return userGroup;
    };

    var initFakeGroup = function (size, groupName) {
      _.each(createFakeUserGroup(size, groupName), function (userData) {
        var userName = userData.firstName + Random.hexString(3);
        var id = Accounts.createUser({
          username: userName,
          email: userData.email.address,
          password: "deadhead",
          group: userData.group
        });
        //Add to user group
        Partitioner.setUserGroup(id, userData._groupId);
        //Add to user collection
        Partitioner.directOperation(function () {
          //Add meteor user id to indv user in user group
          userData._userId = id;
          UserGroup.insert(userData);
        });
        //Set Role
        Roles.addUsersToRoles(id, userData.role,  userData._groupId);
        //Set index grouping
        console.log("User: " +userName+ " || Group: "+groupName+ " +Added");
        allIds.push(id);
      });
    };

    initFakeGroup(20, 'init');
    (function createAdmin () {
      var adminInfo = {
        firstName: 'admin',
        lastName: 'king',
        email: {
          address: 'admin@aol.com',
          verified: true
        },
        phone: {
          cell: fakeFixture.getPhone(),
          home: fakeFixture.getPhone(),
          work: fakeFixture.getPhone()
        },
        displayName: 'Admin',
        role: 'Admin',
        invitedSent: true,
        accountCreated: true,
        timestamp: Date.now(),
        profile: {
          location: fakeFixture.getLocation(),
          company: 'Admin',
          bio: fakeFixture.getParagraph()
        },
        identicon: Random.hexString(6),
      _groupId: 'init'
    };
    var id = Accounts.createUser({
      username: 'admin',
      email: 'Admin@aol.com',
      password: "deadhead",
      group: 'init'
    });
      //Set index grouping
      Partitioner.setUserGroup(id, 'init');
      //Add admin to group
      Partitioner.directOperation(function () {
        adminInfo._userId = id;
        UserGroup.insert(adminInfo);
      });
      //Set Roles
      Roles.addUsersToRoles(id, 'Admin',  'init');

      console.log('Admin Added');
      adminId.push(id);
      allIds.push(id);
    }());


    console.log(allIds)
/***************************************************************/
/* Tag Fixture Generation */
/***************************************************************/

  var generateUserTagTempalte = function (allIds, groupId) {
    return _.each(allIds, function (value) {
      var tagObj = {
        user_Id: value,
        tags: [],
        tagNames: [],
        tagIds: []
      };
      Partitioner.bindGroup(groupId, function () {
        Tags.insert(tagObj);
      });
    });
  };

  generateUserTagTempalte(allIds, 'init')

  var addDefualtTags = function (allIds, defualtTags) {
    return _.each(allIds, function (_userId) {
      var tagTmpl;
      Partitioner.directOperation(function () {
        tagTmpl = Tags.findOne({user_Id: _userId});
      });
      Partitioner.bindUserGroup(_userId, function () {
        _.each(defualtTags, function (tagValue) {
          var tagId = Random.id();
          var tagInfo = {
                tag_Id: tagId,
                  tagName: tagValue.name,
                  tagColor: tagValue.color,
                  frequency: tagValue.count,
                  inDocs: [],
                  group: []
              };


          Tags.update(tagTmpl._id, {
            $addToSet: {
              'tags': tagInfo,
              'tagNames': tagValue.name,
              'tagIds': tagId
            },
          });
        });
      });
    });
  };




  var defualtTagGroup = [
    {name: 'important', color: '#c0392b', count: 2},
    {name: 'completed', color: '#91d096', count: 3},
    {name: 'todo', color: '#88cbf4', count: 5},
    {name: 'new', color: '#e74c3c', count: 12},
    {name: 'in Progress', color: '#2c3e50', count: 22},
    {name: 'fun', color: '#f1c40f', count: 1},

  ];
  
  addDefualtTags(allIds, defualtTagGroup);




/***************************************************************/
/* Messages */
/***************************************************************/



    var createFixtureMessage = function (group) {

      function generateSender () {
        return fakeFixture.fromArray(allIds);
      }

      function senderInfo () {
        var sender = generateSender(),
            options = {fields: {firstName:1,lastName: 1, _userId:1}};
        var info = Partitioner.directOperation(function () {
          return UserGroup.findOne({_userId: sender}, options)
        });
        return {
          senderName: (info.firstName+" "+info.lastName),
          _id: info._userId
        };
      }

      function generateRecipant (number) {
        var recipiants = [];
        // for (var i = 0; i < number; i++) {
        //   recipiants.push(fakeFixture.fromArray(allIds));
        // }
        return adminId;
      }

      var recipiantsList = generateRecipant(fakeFixture.fromArray([1, 1, 1, 1, 2, 5, 3]));

      function generateReadStatus () {
        var hasRead  = [],
            hasNotRead = [];
        for (var i = 0; i < recipiantsList.length; i++) {
          if (fakeFixture.fromArray([true, false])) {
            hasRead.push(recipiantsList[i]);
          }else{
            hasNotRead.push(recipiantsList[i]);
          }
        }
        return {
          hasRead: hasRead,
          hasNotRead: hasNotRead
        };
      }
      var readStatus = generateReadStatus();

      function generateRecipiantList () {
        var options = {fields: {firstName:1,lastName: 1, _userId:1}};
        var userInfo = _.map(recipiantsList, function (_id) {
          return Partitioner.directOperation(function () {
            return UserGroup.findOne({_userId: _id}, options);
          });
        });
        var list = _.map(userInfo, function (user) {
          var userObj = {},
              key = user._userId,
              value = (user.firstName+" "+user.lastName);
              userObj[key] = value;
          return userObj;
        });
        return list;
      }

      var sender = senderInfo();
      
      function generateTagList () {
        var tagList = {};
        _.each(recipiantsList, function (value) {
          tagList[value] = [];
        });
        tagList[sender._id] = [];
        return tagList;
      }

      var subject = fakeFixture.getWord(3, 6),
          message = fakeFixture.getParagraph(),
          rList = generateRecipiantList(),
          tagList = generateTagList();
      return{
        _groupId: group,
        hasNotRead: readStatus.hasNotRead,
        hasRead: readStatus.hasRead,
        subject: subject,
        message: message,
        recipiantList: rList,
        recipiant_Id: recipiantsList,
        senderName: sender.senderName,
        sender_Id: sender._id,
        tags: tagList,
        timestamp: fakeFixture.getRandomDate(new Date(2012, 0, 1), new Date())
      };
    };


    var initMessages = function (numberOfMessages) {
      for (var i = 0; i < numberOfMessages; i++) {
        Partitioner.directOperation(function () {
          console.log('Message: '+i+' Added')
          var messageId = Messages.insert(createFixtureMessage('init'));
          Messages.update(messageId, {$set: {
            path: [messageId],
            parent: [null]
          }});
        });
      }
    };

    initMessages(500);

    // function initFakeFix (size, groupName) {
    //   _.each(createFakeUserGroup(size, groupName), function (userData, key) {
    //     var userName = userData.firstName + Random.hexString(25);
    //     var id = Accounts.createUser({
    //       username: userName,
    //       email: userData.email.address,
    //       password: "deadhead",
    //       group: userData.group
    //     });
    //     //Set Roles
    //     Roles.addUsersToRoles(id, userData.role,  userData._groupId);
    //     //Add to user group
    //     Partitioner.directOperation(function () {
    //       UserGroup.insert(userData);
    //     });
    //     //Set index grouping
    //     console.log(key+"-User: " +userName+ " || Group: "+groupName+ " +Added");
    //   });
    // }
    // initFakeFix(1000, 'G1');
    // initFakeFix(1000, 'G2');
    // initFakeFix(1000, 'G3');
    // initFakeFix(1000, 'G4');
    // initFakeFix(1000, 'G5');
    // initFakeFix(1000, 'G6');
    // initFakeFix(1000, 'G7');
    // initFakeFix(1000, 'G8');
    // initFakeFix(1000, 'G9');
    // initFakeFix(1000, 'G10');
  }

});