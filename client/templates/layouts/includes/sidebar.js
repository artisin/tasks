/***************************************************************/
/* TODO
/ Have a active state tab
/***************************************************************/

//***************************************************************/
/* Varibles */
/***************************************************************/

Template.sidebar.created = function () {
  Session.set('isSidebarActive', 'home');
};

//***************************************************************/
/* Template States*/
/***************************************************************/
Template.sidebar.rendered = function () {
  var tm = TweenMax;
  //Defualt active tab
  $('.ui.accordion').accordion();
  //Makes sure the sidebars hieght is always 100%
  $("#sidebar").height(Math.max($("body").height(),$("#sidebar").height()));
  $( window ).resize(function() {
    $("#sidebar").height(Math.max($("body").height(),$("#sidebar").height()));
  });
  //GSAP Animation Slide-In
  TweenMax.set('#sidebar_Container', {autoAlpha: 0, xPercent:-20});
  TweenMax.to('#sidebar_Container', 0.75, {xPercent:0});
  TweenMax.to('#sidebar_Container', 1, {autoAlpha: 1}, "-=0.25");

  //Messages Defualt Styles
  tm.set('#sidbarMesssagesSubMenu', {css:{
    paddingTop: 0, paddingBottom: 0, opacity: 0, backgroundColor: "rgba(0, 0, 0, 0.0)"}}); 
  tm.set('.messagesSubMenuItems', {css:{height: 0, padding: 0, opacity: 0, margin:0}});
};

//***************************************************************/
/* Helpers */
/***************************************************************/
Template.sidebar.helpers({
  isAdmin: function () {
    //Checks is user is the admain
    var user = Meteor.user(),
        group = Roles.getGroupsForUser(user).toString();
    if (Roles.userIsInRole(user, ['Admin'], group)) {
      return true;
    }else{
      return false;
    }
  },
  projectHome: function () {
    var userLocation = Session.get('userLocation');
    if (userLocation === 'home') {
      return true;
    }else{
      return false;
    }
  },
  /*******************************/
  // Messages
  /*******************************/
  isMessagesActive: function () {
    var messageActive = Session.get('isSidebarActive'),
        isMessagesActive = false,
        messageSubMenu = $('#sidbarMesssagesSubMenu'),
        subItems = $('.messagesSubMenuItems'),
        tl = new TimelineMax();
    
    messageActive === 'messages' ? isMessagesActive = true : isMessagesActive = false;
    
    //Fade In Animation
    if(isMessagesActive){
    tl.to(messageSubMenu, 0.5, {css:{
      paddingTop: 11, paddingBottom: 11, opacity: 1, backgroundColor: "rgba(0, 0, 0, 0.03)"}}, "init")
      .to(subItems, 0.75, {css:{height: 52, opacity: 1}}, "init");
    }
    //Fade Out  Animation
    if (!isMessagesActive) {
      tl.to(messageSubMenu, 0.5, {css:{
        paddingTop: 0, paddingBottom: 0, opacity: 0, backgroundColor: "rgba(0, 0, 0, 0.0)"}}, "init")
        .to(subItems, 0.75, {css:{height: 0, padding: 0, opacity: 0}}, "init");
    }
    
    return isMessagesActive;
  },
  notificationsCount: function () {
    var count = Counts.get('newMessageCount'); 
    if (count !== 0) {
      return {
        count: count,
        'labelColor': 'red'
      };
    }else{
      return {
        count: count,
        'labelColor': 'gray'
      };
    }
  }
});

//***************************************************************/
/* Events */
/***************************************************************/

Template.sidebar.events({

  'click .sidebarTB_Home': function () {
    Session.set('isSidebarActive', 'home');
  },
  'click #sidebarMessages': function (e, tmpl) {
    e.preventDefault();
    Router.go('messages.link', {messageType: 'inboxMessages'});
    return Session.set('isSidebarActive', 'messages');
  },
  /***************************************************************/
  /* Global Events */
  /***************************************************************/
  'click #logout': function (e) {
    e.preventDefault();
    Meteor.logout();
  }
});


//***************************************************************/
/* Helpers */
/***************************************************************/
Template.sidebar.helpers({
  removeProject: function () {
    return Session.get('removeProjectActive');
  }
});