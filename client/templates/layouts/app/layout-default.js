Template.LayoutDefault.rendered = function () {
  var windowHeight = $(window).height();
  Session.set('windowHeight', windowHeight);
  $( window ).resize(function() {
    var windowHeight = $(window).height();
    Session.set('windowHeight', windowHeight);
  });
};