/***************************************************************/
/* All Logic for this tempalte is in the controller */
//This is just for the animation
/***************************************************************/

Template.admin.rendered = function () {
  var tm = TweenMax,
      tl = new TimelineMax(),
      header = this.$('.adminHeader'),
      menu = this.$('.adminMenu'),
      tmpl = this.$('.adminDynamicTmpl');

  tm.set(header, {yPercent:-25, autoAlpha: 0});
  tm.set(menu, {autoAlpha:0});
  tm.set(tmpl, {autoAlpha:0});

  tl.to(header, 0.5, {yPercent:0, autoAlpha: 1})
    .to(menu, 0.5, {autoAlpha:1}, "init")
    .to(tmpl, 0.5, {autoAlpha:1}, "init");
};

Template.admin.destroyed = function () {
  var tm = TweenMax,
    tl = new TimelineMax(),
    header = this.$('.adminHeader'),
    menu = this.$('.adminMenu'),
    tmpl = this.$('.adminDynamicTmpl');

  tl.to(header, 0.5, {yPercent:-25, autoAlpha: 0})
    .to(menu, 0.5, {autoAlpha:0}, "init")
    .to(tmpl, 0.5, {autoAlpha:0}, "init");
};