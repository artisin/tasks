/***************************************************************/
/***************************************************************/
/*  Controller: newUserStatus
/*  Template: /client/views/newUserStatus.html
/***************************************************************/
/***************************************************************/

/***************************************************************/
/* Template States */
/***************************************************************/
/*******************************/
/* Template Created */
/*******************************/
Template.newUserStatus.created = function() {};
/*******************************/
/* Template Rendered */
/*******************************/
Template.newUserStatus.rendered = function() {
  $('.ui.checkbox').checkbox();
  $('.checkbox').checkbox('attach events', '.selectAll.button', 'check');
  $('.checkbox').checkbox('attach events', '.deselectAll.button', 'uncheck');  
};

/***************************************************************/
/* Helpers */
/***************************************************************/
Template.newUserStatus.helpers({
  invite: function () {
    var options= {
      sort: {dateInvited: -1},
      fields: {name: 1, email: 1, role: 1, token: 1, 
              inviteSent: 1, invitedSent: 1, timestamp: 1}};
    console.log(UserGroup.find({}, options).fetch())
    return UserGroup.find({}, options);
  }
});

/***************************************************************/
/* Events */
/***************************************************************/
Template.newUserStatus.events({
  'click .newUserStatus_Checkbox': function (e) {
    var targetRow = $(e.target).closest("tr");
    if (!targetRow.hasClass("tableHighlighted")) {
      $(e.target).closest("tr").addClass("tableHighlighted");
    }else{
      $(e.target).closest("tr").removeClass("tableHighlighted");
    }
  },
  'click .selectAll': function () {
    var table = $('.tableBody');
    $('.tableBody').find('tr').addClass("tableHighlighted");
  },
  'click .deselectAll': function () {
    $('.tableBody').find('tr').removeClass("tableHighlighted");
  }
});