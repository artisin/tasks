/***************************************************************/
/***************************************************************/
/*  Controller: adminAddUser
/*  Template: /client/views/adminAddUser.html
/***************************************************************/
/***************************************************************/

//////////////////*********** TODO ***********//////////////////
/// 1. Group: As of right now when the admin sends a invite it
/// creates a group obj that is send to the db and when the new
/// user regs it pulls that obj and matches and assigns the group
/// alough there might be a need for many diffrent groups so the 
/// admin should have to specify if I decied to allow there to 
/// more then just one group per.
///
/// 2. Decied to use or ditch the checkbox
/// 
/// 3. Set schema to validate new invite
//////////////////*********** TODO ***********//////////////////

/***************************************************************/
/* Template States */
/***************************************************************/
/*******************************/
/* Template Created */
/*******************************/
Template.adminAddUser.created = function() {
  this.newUserEmail = new ReactiveVar();
};
/*******************************/
/* Template Rendered */
/*******************************/
Template.adminAddUser.rendered = function() {
  //Template Fade In
  App.animateTmpl.container('#admin_NewUserForm');
  //UI activation
  this.$('.dropdown').dropdown({transition: 'drop'});
  this.$('.ui.checkbox').checkbox();
  /*******************************/
  /* Form Validation */
  /*******************************/
  return this.$('#admin_NewUserForm').validate({
    rules: {
      firstName: {
        required: true,
        minlength: 3,
        maxlength: 20,
      },
      lastName: {
        required: false,
        minlength: 3,
        maxlength: 20,
      },
      emailAddress: {
        required: true,
        email: true
      },
      phone1: {
        required: false,
        number: true,
        minlength: 3,
        maxlength: 3
      },
      phone2: {
        required: false,
        number: true,
        minlength: 3,
        maxlength: 3
      },
      phone3: {
        required: false,
        number: true,
        minlength: 4,
        maxlength: 4
      }
    },
    focusCleanup: true,
    highlight: function(element) {
      $(element).parent().addClass("error");
    },
    unhighlight: function(element) {
      $(element).parent().removeClass("error");
    },
    messages: {
      firstName: {
        required: "Please enter first name",
        minlength: "Please use at least three characters.",
        maxlength: "You username cannot be 15 characters or longer"
      },
      lastName: {
        required: "Please enter first name",
        minlength: "Please use at least three characters.",
        maxlength: "You username cannot be 15 characters or longer"
      },
      emailAddress: {
        required: "Please enter your email address.",
        email: "Please enter a valid email address."
      },
      phone1: {
        number: "Please only use numbers.",
        minlength: "The area code cannot be shorter then three numbers",
        maxlength: "The area code cannot be longer then three numbers"
      },
      phone2: {
        number: "Please only use numbers.",
        minlength: "The xxx code cannot be shorter then three numbers",
        maxlength: "The xxx code cannot be longer then three numbers"
      },
      phone3: {
        number: "Please only use numbers.",
        minlength: "The xxx code cannot be shorter then three numbers",
        maxlength: "The xxx code cannot be longer then three numbers"
      },           
    },
  });
};

/***************************************************************/
/* Helpers */
/***************************************************************/
Template.adminAddUser.helpers({
  userRoles: function () {
    return Roles.getAllRoles();
  },
  //Displays the new user email in success message
  successEmailDisplay: function () {
    return Template.instance().newUserEmail.get();
  }
});

/***************************************************************/
/* Events */
/***************************************************************/
Template.adminAddUser.events({
  'submit #admin_NewUserForm': function (e, tmpl) {
    e.preventDefault();
    //Get the inputs from the fields
    var firstName = App.inputValue('firstName', tmpl),
        lastName = App.inputValue('lastName', tmpl),
        email = App.inputValue('emailAddress', tmpl).toLowerCase(),
        phone = Number(App.inputValue('phone1', tmpl)+App.inputValue('phone2', tmpl)+App.inputValue('phone3', tmpl)), 
        role = App.inputValue('userRole', tmpl),
        companyName = App.inputValue('companyName', tmpl),
        group = Roles.getGroupsForUser(Meteor.user()).toString(),
        url = window.location.origin + "/register";


    //Create a invte obj for the method call
    var invitee = {
      firstName: firstName,
      lastName: lastName,
      email: email,
      phone: phone,
      role: role,
      company: companyName, 
      group: group,
      url: url
    };

    //Clean the invitee and convert and empty fields to null
    function clean (obj) {
      var cleanInvitee = {};
      _.each(obj, function (value, key) {
        if (value === "") {
          if (key==='role') {value = value;}
          else{value=null;}
        }
        if (_.isNumber(value) && value.toString().length !== 10) {value=null;}
        cleanInvitee[key] = value;
      });
      return cleanInvitee;
    }
    //Cleaned Invitee obj
    var cleanInvitee = clean(invitee);

    //If the role input is left empty alert the user with modal
    //And tell them it will be set as 'user' (defult)
    if (cleanInvitee.role === "") {
        //Display modal overlay
        Modal.overlay({
          template: 'modalConfirm',
          dataContext:{
            title: 'No Role Selected',
            icon: 'warning red',
            content: 'Warning you have not selected a role, it will defualt as user. Do you wish to proceed?',
            _reactiveVar: Template.instance(),
            action:{
              //If they wish to proceed without selecting a 
              //option it will defualt as just a 'user'
              confirmModal: function() {
                //Apply role to user
                cleanInvitee.role = 'user';
                //Set RecVar Email Var for success Message
                this._reactiveVar.newUserEmail.set(cleanInvitee.email);
                return adminSendInvite(cleanInvitee); 
              },
              //Close overlay and do nothign
              cancelModal: function() {
                return;
              }
            }
          },
      });
    }else{
      Template.instance().newUserEmail.set(cleanInvitee.email);
      adminSendInvite(cleanInvitee);
    }

  //Send invite method call
  function adminSendInvite (cleanInvitee) {
    Meteor.call('sendInvite', cleanInvitee, function (error, res) {
      if(error){
      //Error popup message
      ErrorMessage.insert({errormessage: error.reason});
      }else{
      //On success update the account to indicate the email
      //has been sent to the user  
      var id = res;
      Meteor.call('updateInviteAccount', id, function(error){
        if (error) {
          //Error popup message
          ErrorMessage.insert({errormessage: error.reason});
        }else{
          //Reset Form
          $('#admin_NewUserForm').trigger('reset');

          var tl = new TimelineMax(),
          formContainer = $('.adminAddUser_Container'),
          success = $('.adminAddUserSuc_Container');

          //GSAP timeline
          tl.to(formContainer, 0.5, {autoAlpha:0,
                onComplete:hideAndShow,
                onCompleteParams:[formContainer, success]})
          .to(success, 0.75, {autoAlpha:1})
          .to(success, 0.75, {autoAlpha:0,
              onComplete:hideAndShow,
              onCompleteParams:[success, formContainer]}, "+=2")
          .to(formContainer, 0.5, {autoAlpha:1});
          
          //Hides the main Container and shows the succsess
          function hideAndShow (pram1, pram2){
            pram1.hide();
            pram2.show();
          }
        }
      });
      }
    });
  }

  }
});