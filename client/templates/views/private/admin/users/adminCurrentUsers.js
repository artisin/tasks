/***************************************************************/
/***************************************************************/
/*  Controller: adminCurrentUsers
/*  Template: /client/views/adminCurrentUsers.html
/***************************************************************/
/***************************************************************/

/***************************************************************/
/* Template States */
/***************************************************************/
/*******************************/
/* Template Created */
/*******************************/
Template.adminCurrentUsers.created = function() {
  this.activeCollection = new  ReactiveVar({accountCreated: true});
};
/*******************************/
/* Template Rendered */
/*******************************/
Template.adminCurrentUsers.rendered = function() {
  //Template Fade In
  App.animateTmpl.container('#adminCurrentUsers', ".collectionTable");
  
};

/***************************************************************/
/* Helpers */
/***************************************************************/
Template.adminCurrentUsers.helpers({
  hasAccounts: function () {
    var filter = Template.instance().activeCollection.get();
    return UserGroup.find(filter).count() === 0 ? false : true;

  },
  accountType: function () {
    var account = Template.instance().activeCollection.get().role,
        plural = account+"'s.";
    //Defult, on load tmpl
    if (account === undefined) {return "All Account's.";}
    return plural;
  },
  allAccounts: function () {
    return UserGroup.find({accountCreated: true}).count();
  },
   totalAdmins: function () {
    return UserGroup.find({accountCreated: true, role: 'Admin'}).count();
  },
  totalManagers: function () {
    return UserGroup.find({accountCreated: true, role: 'Manager'}).count();
  },
  totalUsers: function () {
    return UserGroup.find({accountCreated: true, role: 'User'}).count();
  },
  totalViewers: function () {
    return UserGroup.find({accountCreated: true, role: 'Viewer'}).count();
  },
  tableSettings: function () {
    var filter = Template.instance().activeCollection.get(), 
        collection = UserGroup.find(filter);
        return {
            collection: collection,
            rowsPerPage: 10,
            showFilter: true,
            fields: [
                { key: 'displayName', label: 'Display Name'},
                { key:'firstName', label: 'First Name' },
                { key:'lastName', label: 'Last Name' },
                { key: 'phone', label: 'Phone' },
                { key: 'role', label: 'Role' },
                { key: 'email.address', label: 'Email' },
                { key: 'profile.company', label: 'Company' },
                { key: 'profile.location.state', label: 'Location- State' },
                { key: 'profile.location.city', label: 'Location- City' },
                { key: 'timestamp', label: 'Date Joined', hidden: true},
            ]
        };
  }
});

/***************************************************************/
/* Events */
/***************************************************************/
Template.adminCurrentUsers.events({
  'click .stats .segment': function (evt, tmpl) {
    var current = tmpl.$('.tertiary');
    if (current.length) {
      current.removeClass('tertiary');
    }
    var target = $(evt.currentTarget)
    target.removeClass('secondary');
    target.addClass('tertiary');

    var selectedCollection = $(evt.currentTarget).data().name;
    if (selectedCollection === 'All Acount') {
      Template.instance().activeCollection.set({
        accountCreated: true
     });
    }else{
      Template.instance().activeCollection.set({
          accountCreated: true, role: selectedCollection
      });
    }

    //Due to edge cases when switching between views
    //this ensures that the all the ui works properly
    Meteor.setTimeout(function () {
      $('.ui.checkbox').checkbox();
      $('.checkbox').checkbox('attach events', '.selectAll.button', 'check');
      $('.checkbox').checkbox('attach events', '.deselectAll.button', 'uncheck');
    }, 100);
    
    //Cntrolls the fade in animation 
    Meteor.defer(function () {
      App.animateTmpl.containerDelay('.collectionTable');
    });   
  },
});