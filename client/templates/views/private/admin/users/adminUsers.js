/***************************************************************/
/***************************************************************/
/*  Controller: adminUsers
/*  Template: /client/views/adminUsers.html
/***************************************************************/
/***************************************************************/

//////////////////*********** TODO ***********//////////////////
/// 1. Create a way to display group individual users in tabel
/// for the current user tab, that is efficent. Possibly use
/// indexes for the mongo, or create a seperate collection for
/// soley just groups info
///
//////////////////*********** TODO ***********//////////////////


/***************************************************************/
/* Template States */
/***************************************************************/
/*******************************/
/* Template Created */
/*******************************/
Template.adminUsers.created = function() {
  this.tableDataContext = new ReactiveVar(null);
};
/*******************************/
/* Template Rendered */
/*******************************/
Template.adminUsers.rendered = function() {
  $('.ui.accordion').accordion();
};

/***************************************************************/
/* Helpers */
/***************************************************************/
Template.adminUsers.helpers({
  userTable: function (e, tmpl) {
    return Roles.getAllRoles();
  },
  getTableTemplate: function () {
    return 'userDataTable';
  },
  getTableDataContext: function () {
    //To costly
    // var role = Template.instance().tableDataContext.get();
    // if (role !== null) {
    //   var group = Roles.getGroupsForUser(Meteor.userId()).toString();
    //          console.log(role)
    //   return {
    //     userTable: function () {
    //       return Roles.getUsersInRole(role, group);
    //     }
    //   }
    // }else{
    //   return;
    // }
  }
});

/***************************************************************/
/* Events */
/***************************************************************/
Template.adminUsers.events({
  'click .currentUsers_Title': function (e, tmpl) {
    Template.instance().tableDataContext.set(this.name);
  }
});