/***************************************************************/
/***************************************************************/
/*  Controller: editProfile
/*  Template: /client/views/editProfile.html
/***************************************************************/
/***************************************************************/

//////////////////*********** TODO ***********//////////////////
/// 1. Possible jquery validation for better ui
/// 2. Animation
///
//////////////////*********** TODO ***********//////////////////

/***************************************************************/
/* Template States */
/***************************************************************/
/*******************************/
/* Template Created */
/*******************************/
Template.editProfile.created = function() {
  Session.set('isSuccess', null);
  Session.set('isEditing', null);
};
/*******************************/
/* Template Rendered */
/*******************************/
Template.editProfile.rendered = function() {
  $('.dropdown').dropdown({transition: 'drop'});
};

/***************************************************************/
/* Helpers */
/***************************************************************/
Template.editProfile.helpers({});

/***************************************************************/
/* Events */
/***************************************************************/
Template.editProfile.events({
  //Field Change
  'click .editField, .editText': function (evt) {
    //evt.preventDefault();
    var input = $(evt.target).closest('.input').children('input'),
        inputParent = $(input).parent(),
        nameVal = $(input).attr('name');
        
   var activeEditing = _.flatten(Session.get('isEditing')),
       newEditing = [];
   if (_.contains(activeEditing, nameVal)) {
      newEditing = activeEditing;
      Session.set('isEditing', _.without(newEditing, nameVal));
      $(input).prop("disabled", true);
      var newValue = $(input).val();
      var params = {
        _id: Meteor.userId(),
        valueChange: nameVal,
        newValue: newValue,
      };
      //call
      Meteor.call('editProfile', params, function (error) {
        if (error) {
          console.log(error)
          App.errorMessage(error.reason);
          App.inputError(inputParent, error.reason);
        }else{
          Session.set('isSuccess', nameVal);
        }
      });
   }else{
    //Enable field
    $(input).prop("disabled", false);
    newEditing.push(activeEditing);
    newEditing.push(nameVal);
    Session.set('isEditing', newEditing);
    // console.log(activeEditing)
   }
 },
  //Bio Change
  'click .editArea': function (evt) {
    //evt.preventDefault();
    var input = $(evt.target).parent().find('textarea'),
        inputParent = $(input).parent(),
        nameVal = $(input).attr('name');
        console.log(input)
   var activeEditing = _.flatten(Session.get('isEditing')),
       newEditing = [];
   if (_.contains(activeEditing, nameVal)) {
      newEditing = activeEditing;
      Session.set('isEditing', _.without(newEditing, nameVal));
      $(input).prop("disabled", true);
      var newValue = $(input).val();
      var params = {
        _id: Meteor.userId(),
        valueChange: nameVal,
        newValue: newValue,
      };
      //call
      Meteor.call('editProfile', params, function (error) {
        if (error) {
          console.log(error)
          App.errorMessage(error.reason);
          App.inputError(inputParent, error.reason);
        }else{
          Session.set('isSuccess', nameVal);
        }
      });
   }else{
    //Enable field
    $(input).prop("disabled", false);
    newEditing.push(activeEditing);
    newEditing.push(nameVal);
    Session.set('isEditing', newEditing);
   }
 },
 //Role Change
 'click #roleEdit, .editText': function (evt) {
    var input = $(evt.target).parent(),
        disableSelector = $(input).children('.dropdown'),
        spanSelector = $(input).children().find('span'),
        nameVal = $(spanSelector).attr('name'),
        id = Meteor.userId(),
        group = Partitioner.group();
  
  
    //Verifies use is an admin since only admin can change roles
    if (Roles.userIsInRole(id, ['Admin'], group)) {
  
      var toggleState = function(role) {
        //Toggle Class To Disable
        disableSelector.toggleClass('disabled');
        //Remove elm from isEditing session
        Session.set('isEditing', _.without(newEditing, nameVal));
        //Set defualt text back
        role === undefined ? $(spanSelector[0]).text('Current Role - Admin') :
        $(spanSelector[0]).text('Current Role - '+role);
       } 
      //Gets the current elms that are being edited
      var activeEditing = _.flatten(Session.get('isEditing')),
      newEditing = [];

      //Checks the state -- edit = false || save = true 
      if (_.contains(activeEditing, nameVal)) {
        newEditing = activeEditing;
        
        //Grab the new role value
        //--The selector could be improved upon
        var newValue = $(spanSelector[0]).text().trim();
        //Obj for method call
        var params = {
          _id: Meteor.userId(),
          valueChange: nameVal,
          newValue: newValue,
        };

        //Checks to makes sure user slelected a valid role
        //if not it will throw an error label on input
        if (params.newValue === "Current Role - Admin") {
          App.inputError(input, 'If you want to change the users role you must select a role');
          toggleState();
          return false;
        }

        if (params._id === Meteor.userId()) {
          Modal.overlay({
            template: 'modalConfirm',
            dataContext:{
              title: 'Are You User You Want To Change Your Role?',
              icon: 'warning red',
              content: 'Warning, you are about to change your own role. If you change your role from Admin you will be unable to change it back and only another Admin can promote your back to your original Admin role. Do you wish to proceed?',
              action:{
                confirmModal: function() {
                  editProfile(params);
                },
                //Close overlay and do nothign
                cancelModal: function() {
                  toggleState();
                  return false;
                }
              }
            }
          });
        }else{
          editProfile(params);
        }

        //call
        var editProfile = function (params) {
          Meteor.call('editProfile', params, function (error) {
            if (error) {
              App.errorMessage(error.reason);
              App.inputError(input, error.reason);
            }else{
              //Toggle State
              toggleState(params.newValue);
            }
          });
        };
      }else{
        //Toggle Class To Enable
        disableSelector.toggleClass('disabled');
        //Push exisiting isEditing elms to empty array
        newEditing.push(activeEditing);
        //Add the new isEditing elms to arry
        newEditing.push(nameVal);
        //Set the session to include all new and old elms
        //which will be recalced by the reg. UI helper 
        Session.set('isEditing', newEditing);
      }
    };
 }
});