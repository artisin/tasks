/******************************************************************************/
/******************************************************************************/
/*  messageDisplay
// Controlls the logic of the display of the messages
/******************************************************************************/
/******************************************************************************/

/*******************************/
// Created
/*******************************/
Template.messageDisplay.created = function () {
  //If user is searching for a tag
  this.isTagSearchActive = new ReactiveVar(false);
  this.tagSearchResults = new ReactiveVar({}); 
  this.tagSearchValue = ReactiveVar(null);

  //is message reply active
  this.isMessageReply = new ReactiveVar(false);
  this.isMessageHidden = new ReactiveVar({_id: null, state: false, height: 0});
  
  var uiComponents = function () {
    var self = this;
    return {
      init: function () {
        this.tagAdder();
      },
      tagAdder: function () {
        $('.indvMessageTagAdder').dropdown({
          transition: 'horizontal flip',
          action: 'hide',
          fullTextSearch: false
        });
      }
    };
  };
  this.data.uiComponents = uiComponents.call(this);
}; 

/*******************************/
// Rendered
/*******************************/
Template.messageDisplay.rendered = function () {
  //Tag dropdown
  this.data.uiComponents.init();
}

/***************************************************************/
/* Helpers */
/***************************************************************/
Template.messageDisplay.helpers({
  isTagSearchActive: function () {
    return Template.instance().isTagSearchActive.get();
  },
  commonTags: function () {
    var selector = {},
        key = ('users.'+Meteor.userId()+'.frequency');
    selector[key] = -1;
    //Filter by frequency
    return Tags.find({}, {sort: selector, limit: 4}).fetch();
  },
  tagSearchResults: function () {
    var resultsInstance = Template.instance().tagSearchResults.get();
    if (resultsInstance.count > 0) {
      return resultsInstance.results;
    }
  },
  noTagSearchResults: function () {
    console.log(Template.instance().tagSearchResults.get().count === 0)
      if (Template.instance().tagSearchResults.get().count === 0) {
        return true;
      }else{
        return false;
      }
  },
  tagSearchValue: function () {
    return Template.instance().tagSearchValue.get();
  },
  //Gets local tag collection, and publishes tag to corresponding message
  currentTags: function (e, tmpl) {
    var userId = Meteor.userId(),
        self = this;
    var tags = _.map(LocalTags.find().fetch(), function(value){
      if (_.contains(value.users[userId].inDocs, self._id)) {
        return {
          tagName: value.tagName,
          tagColor: value.tagColor,
          _id: value._id
        };
      }
    });
    return _.compact(tags);
  },
  recipiantList: function () {
    var recipiantList = _.map(_.compact(this.recipiantList), function (recipiant) {
      //Avodies errors when message is hidden and page ref
      // if (recipiant === null) {return false}
      return {
        recipiant_Id: _.keys(recipiant),
        recipiantName: _.values(recipiant)
      };
    });
    return recipiantList;
  },
  oneOrMoreRecipiants: function () {
    var recipiantList = this.recipiantList;
    if (recipiantList !== undefined) {
      return recipiantList.length === 1 ? false : true;
    }
  },

  messageDisplay: function () {
    console.log(this.message.length)
    var message = this.message,
        messageLength = message.length,
        showMore = false;

    if (messageLength > 500) {
      showMore = true;
    }

    return{
      message: message,
      showMore: showMore
    }

  },
  /*******************************/
  // Message Read
  /*******************************/
  isMessageRead: function () {
    if (_.contains(this.hasRead, Meteor.userId())) {
      return true;
    }
    return false;
  },
  /*******************************/
  // Message Reply
  /*******************************/
  isMessageReply: function () {
    var tpI = Template.instance();
    var activateForm = Meteor.wrapAsync(function () {
      return tpI.isMessageReply.get();
    });
    return activateForm();
  },
  /*******************************/
  // Hide Message
  /*******************************/
  isMessageHidden: function () {
    var tpI = Template.instance(),
        message = tpI.isMessageHidden.get(),
        tl = new TimelineMax();
    //Fade to Button
    if (message.state) {
      Meteor.setTimeout(function () {
        var container = $(tpI.firstNode).children('.hiddenMessage');
        TweenMax.to(container, 0.5, {autoAlpha:1});
      }, 100);
    }
    //Fade Message back
    if (!message.state && tpI.firstNode !== null && message._id === this._id) {
      var container = $(tpI.firstNode),
          messageHeight = message.height;
      //Clean up inline styles to adviod dom fuckups
      var cleanAnim = function () {
        $(tpI.firstNode).removeAttr('style');
      };
      tl.to(container, 1,  {css:{height: messageHeight}})
        .to(container, 1.9, {autoAlpha:1, onComplete:cleanAnim}, "-=0.8");
    }
    //Template helpers
    return {
      display: message.state,
      sender: this.senderName,
      subject: this.subject
    };
  }
});


/***************************************************************/
/* Events */
/***************************************************************/
Template.messageDisplay.events({
  'click .indvMessageTagAdder': function (e) {
    e.preventDefault();
    Template.instance().tagSearchResults.set({});
    Template.instance().isTagSearchActive.set(false);
    $('.tagSearch').val('');
    $('.tagSearch').focus();
  },
  'keyup .tagSearch': function (e) {
    e.preventDefault();
    var value = $(e.target).val().trim(),
        tpI = Template.instance();
        tpI.isTagSearchActive.set(true);
        tpI.tagSearchValue.set(value);
    EasySearch.search('tags',  value, function (err, data) {
      // use data.results and data.total
      if (err) {
        console.log(err);
      }else{
        tpI.tagSearchResults.set({
          count: data.total, 
          results: data.results
        });
      }
    });
  },
  'keydown .tagSearch': function (e, tmpl) {
    var newTag = $('.addNewTag');
    var tagObj = {
      tagName: newTag.children('span').text().trim(),
      messageId: tmpl.data._id
    };
    if (e.keyCode === 13) {
      var exsistingTag = $('.addTag');
    console.log(exsistingTag)
      if (newTag.length === 1 || exsistingTag.length === 1) {
        addNewTag(tagObj);
      };
    }
    function addNewTag (tagObj) {
      return Meteor.call('addTag', tagObj, function (error) {
        if (error) {
          App.errorMessage(error.reason);
        }else{
          var hideTagMenu = function () {
            $('.tagMenu').addClass('hidden');
            $('.tagMenu').removeClass('visible');
            $('.tagMenu').attr('style', '');
            $('.indvMessageTagAdder').removeClass('active visible');
          };
          TweenMax.to('.tagMenu', 0.25, {scale: 0, autoAlpha:0,
                      onComplete:hideTagMenu});
        }
      });
    }
  },
  //Adds an exsisting tag
  'click .addTag': function (e, tmpl) {
    e.preventDefault();
    console.log(tmpl)
    var tagName = $(e.target).text().trim(),
    tagObj = {
      tagName: tagName,
      messageId: tmpl.data._id,
      user_Id: Meteor.userId()
    };
    Meteor.call('addTag', tagObj, function (error) {
      if (error) {
        console.log(error)
        App.errorMessage(error.reason);
      }
    });
  },
  //Adds a newley created tag
  'click .addNewTag': function (e, tmpl) {
    e.preventDefault();
    var tagName = $(e.currentTarget).children('span').text().trim();
    if (tagName.length <= 2) {
      App.errorMessage('Sorry, labels must be three characters or longer.');
    }else{
      var tagObj = {
        tagName: tagName,
        messageId: tmpl.data._id,
        user_Id: Meteor.userId()
      };
      Meteor.call('addTag', tagObj, function (error) {
        if (error) {
          App.errorMessage(error.reason);
        }
    });    
    }
  },
  /*******************************/
  // Show Full Message
  /*******************************/
  'click .showFullMessageButton': function (e, tmpl) {
    e.preventDefault();
    console.log(this)
  },
  /*******************************/
  // Delete Tag
  /*******************************/
  'click .deleteTag': function (e, tmpl) {
    e.preventDefault();
    var tagObj = {
      tag_Id: this._id,
      doc_Id: [tmpl.data._id]
    };
    Meteor.call('removeMessageTag', tagObj, function (error) {
      if (error) {
        console.log(error);
        App.errorMessage(error.reason);
      };
    });
  },
  //Avatar popup
  //If the user just mouses over the avatar a popup is added to the dom
  //but it is not removed since the popup never had a chance to be displayed
  //small issue and does not adverserly affect anything but possibly fix this later
  'mouseenter .avatarPopUp': function (e) {
    $(e.target)
      .popup({
        //inline   : true,
        hoverable: true,
        position : 'bottom center',
        transition: 'fade down',
        delay: {
          show: 200,
          hide: 200
        },
        onHide: function () {
          $('.popup').remove();
        },
      })
    ;
  },
  /*******************************/
  // Message Read
    //Marks Message as read
  /*******************************/
  'click .messageRead': function (e, tmpl) {
    e.preventDefault();
    var messageContainer = $(tmpl.firstNode),
        tl = new TimelineMax();

    tl.to(messageContainer, 0.75, {autoAlpha:0})
      .to(messageContainer, 0.75, {height: 0, padding: 0, onComplete:markAsRead}, "=-0.25")

    function markAsRead () {
      return Meteor.call('markMessageAsRead', [tmpl.data._id,], function (error) {
      if (error) {
        console.log(error);
        App.errorMessage(error.reason);
      };
    });
    }
  },

  /*******************************/
  // Messge Reply - in line
  /*******************************/
  'click .messageReply': function (e, tmpl) {
    var messageBody = $(e.currentTarget).parents('.comments'),
        messageBodyHeight = messageBody.outerHeight(),
        actionButtons = $(e.currentTarget).parents('.messageActions'),
        tl = new TimelineMax(),
        tmI = Template.instance();
        // messageFrom;
    function activateForm () {
      tmI.isMessageReply.set(true);
    }
    tl.to(messageBody, 0.5, {height: (messageBodyHeight+227), onComplete: activateForm})
      .to(actionButtons, 0.25, {autoAlpha:0});
    //Find out a better way to display form
    //for some reason it takes 800ms to recoqnize the form has been placed
    //into the dom
    Meteor.setTimeout(function () {
      $('.messageReplyForm').show()
      TweenMax.to('.messageReplyForm', 0.5, {autoAlpha:1});
    }, 800);
  },
  'click .cancleReplyButton': function (e) {  
    e.preventDefault();
    var messageBody = $(e.currentTarget).parents('.comments'),
        messageBodyHeight = messageBody.outerHeight(),
        messageReplyForm =  $(messageBody).children('.messageReplyForm'),
        //"Bullshit"
        tl = new TimelineMax(),
        tmI = Template.instance();

    function deactiaveFrom () {
      tmI.isMessageReply.set(false);
      App.cleanAnim(messageBody);
    }
    tl.to(messageReplyForm, 0.5, {autoAlpha:0})
      .to(messageBody, 0.5, {height: (messageBodyHeight-227), onComplete: deactiaveFrom})
      //Bullshit that will not find its fucking home
      .to('.messageActions', 0.25, {autoAlpha:1});
  },
  'click .submitReplyButton': function (e, tmpl) {
    e.preventDefault();
    var reply = App.inputValue('messageReply', tmpl),
        data = tmpl.data;
    //Param obj for call
    var params = {
        messageReply: reply,
        parent: [data._id],
        path: data.path,
        recipiant_Id: data.recipiant_Id,
        recipiantList: data.recipiantList,
        sender_Id: data.sender_Id,
        previousSenderName: data.senderName
      };
      //Call to method
      Meteor.call('messageReply', params, function (error) {
        if (error) {
          console.log(error);
          App.errorMessage(error.reason);
        };
    });
  },
  /*******************************/
  // Hide Message Button
  /*******************************/
  'click .messageHide': function (e, tmpl) {
    e.preventDefault();
    var container = $(tmpl.firstNode),
        children = $(tmpl.firstNode).children('.indvMessage_Container').children('.comments'),
        tl = new TimelineMax(),
        tpI = Template.instance(),
        messageHight = $(container).outerHeight(),
        self = this;

    //Set recVar
    function messageIsHidden () {
      tpI.isMessageHidden.set({_id: self._id, state: true, height: messageHight}); 
    }

    //Tween animation
    tl.to(children, 0.5, {autoAlpha:0, onComplete:messageIsHidden}, "init")
      .to(container, 0.5, {css: {height: '88px'}}, "init");

  },
  /*******************************/
  // Unhide Message 
  /*******************************/
  'click .unHideMessage': function (e, tmpl) {
    e.preventDefault();
    var message = $(tmpl.firstNode),
        tl = new TimelineMax(),
        tpI = Template.instance(),
        messageHieght = tpI.isMessageHidden.get().height,
        id = tpI.isMessageHidden.get()._id; 
    
    //Sets recVar
    function messageIsHidden () {
      tpI.isMessageHidden.set({_id: id, state: false, height: messageHieght});
    }

    //Tween Animation
    tl.to(message, 0.5, {autoAlpha:0, onComplete:messageIsHidden});
  },
});