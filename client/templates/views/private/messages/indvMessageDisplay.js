/******************************************************************************/
/******************************************************************************/
/*  Controller:  
/*  Template: /client/views/templateLocation
/******************************************************************************/
/******************************************************************************/

//////////////////*********** Note ***********//////////////////
/// 1. This template is created for each indv message be careful
/// resources and helpers
///
//////////////////*********** Note ***********//////////////////


/***************************************************************/
/* Template States */
/***************************************************************/
/*******************************/
// Created
/*******************************/
Template.indvMessageDisplay.created = function () {
  // this.isTagsActive = new ReactiveVar(false);
  // //If user is searching for a tag
  // this.isTagSearchActive = new ReactiveVar(false);
  // this.tagSearchResults = new ReactiveVar({}); 
  // this.tagSearchValue = ReactiveVar(null);
  this.activeThread = new ReactiveVar(null);

};
/*******************************/
// Rendered
/*******************************/
Template.indvMessageDisplay.rendered = function () {
  var star = this.data.stared;
  if (star !== undefined && _.contains(star, Meteor.userId())) {
    var message = App.dataTarget('.messageList', 'templateid', this.data._id),
        star = $(message).find('.star');
    $(star).rating('set rating', 1);
  }
};
/*******************************/
// Destroyed
/*******************************/
Template.indvMessageDisplay.destroyed = function () {
};


/***************************************************************/
/* Template Helpers */
/***************************************************************/
Template.indvMessageDisplay.helpers({
  messageInfo: function () {
    var userId = Meteor.userId(),
        messageThread = Session.get('messageThread'),
        selectedMessages = Session.get('selectedMessages'),
        self = this;

      //Read or not read message
      function typeOfMessage () {
        var hasNotReadArray = self.hasNotRead,
            messageType;
        if (_.contains(hasNotReadArray, userId)) {
          messageType = 'newMessage';
        }else{
          messageType = 'readMessage';
        }
        return messageType;
      }

      //Detemins what messages are active based on thread
      function isMessageReply () {
        if (messageThread.active) {
          var parentThread = messageThread.parentThread;

          //No parent thread
          if (!parentThread) {
            if (self._id === messageThread.messageId) {
              return 'activeThread';
            }
          }else{
            //Parent Thread
            if (_.contains(self.path, parentThread)) {
              if (self._id === messageThread.messageId) {
                return 'mainActiveThread'; 
              }else{
                return 'activeThread';
              }
            }
          }
        }
      }

    //Creats a class of the id for selection
    function getId () {
      return self._id;
    }

    /**
     * Finds out if the message should be toggled for selection
     * @return {class} for hightlight if checked
     */
    function getSelected () {
      if (!_.contains(selectedMessages, self._id)) {
        //Recheck all boxes if checked
        Meteor.setTimeout(function () {
          var message = App.dataTarget('.messageList', 'templateid', self._id),
              checkbox = $(message).find('.checkbox');
          $(checkbox).checkbox('uncheck');
        }, 0);
      }
      if (_.contains(selectedMessages, self._id)) {
        //Recheck all boxes if checked
        Meteor.setTimeout(function () {
          var message = App.dataTarget('.messageList', 'templateid', self._id),
              checkbox = $(message).find('.checkbox');
          $(checkbox).checkbox('check');
        }, 0);
        return 'highlightSelection';
      }
    }

    //These run with every message thread, possibly heavy and needs some love
    return {
      type: function () {
        return typeOfMessage();
      },
      isReply: function () {
        if (messageThread.active) {
          return isMessageReply();
        }
      },
      getId: function () {
        return getId();
      },
      isSelected: function () {
        //Only run if there are messages selGected
        // if (selectedMessages.length !== 0) {
        // }
        return getSelected(); 
      }
    }; 
  },

  //Determins if user has tags 
  hasTags: function () {
    var tags = Template.instance().data.tags,
        userId = Meteor.userId(),
        userTags = tags[userId];
    return userTags.length !== 0 ? true : false;
  }
});

/***************************************************************/
/* Template Events */
/***************************************************************/
Template.indvMessageDisplay.events({
  //Opens message thread and controls primary logic by setting 'messageThread'
  'click .openMessageThread': function (e) {
    e.preventDefault();
    
    //In case user is deleting a tag we do not want it to then also open the thread
    if (!$(e.target).hasClass("deleteTag")) {
      //If no parent
      if (this.parent[0] === null) {
        Session.set('messageThread', {active: true, messageId: this._id,  parentThread: false});
      }else{
        //If parent
        Session.set('messageThread', {active: true, messageId: this._id, parentThread: this.parent[0]});
      }


      Meteor.call('markMessageAsRead', [this._id], function (error) {
        if (error) {
          console.log(error);
          App.errorMessage(error.reason);
        }
      });
    }

  },


  /*******************************/
  //Avatar popup
  /*******************************/
  'mouseenter .avatarPopUp': function (e) {
    //Popup logic
    $(e.currentTarget).popup({
      position : 'bottom center',
      transition: 'fade down',
      hoverable: true,
      delay: {
        show: 50,
        hide: 150
      },
    });

    //Remove Popup
    $('.popup_Container').mouseleave(function () {
      Meteor.setTimeout(function () {
        //Check to make sure popup is hidden, since this will fire
        //when the user mose geos over the :before
        if ($('.popup').popup('is hidden')) {
          $('.popup').remove();
        }
      }, 300);
    });

  },
  /***************************************************************/
  /* Toggle Selector */
  /***************************************************************/
  //
  'click .toggleMessage': function (e) {
    e.preventDefault();
    var currentSelections = Session.get('selectedMessages'),
        newSelections;

    //Add messge to selection session
    if ($(e.currentTarget).hasClass("checked")) {
      newSelections = _.union(currentSelections, [this._id]);
      Session.set('selectedMessages', newSelections);
    }else{
      //Remove message
      newSelections = _.without(currentSelections, this._id);
      Session.set('selectedMessages', newSelections);
    }
  },
  /***************************************************************/
  /* Star Selector */
  /***************************************************************/
  'click .star': function (e, tmpl) {
    e.preventDefault();
    var params = {
      docId: this._id
    };

    //See if user has star
    if (_.contains(this.stared, Meteor.userId())) {
      removeStar();
    }else{
      addStar();
    }

    //Adds star
    function addStar () {
      Meteor.call('addMessageStar', params, function (error) {
        if (error) {
          console.warn(error);
          App.errorMessage(error.reason); 
        }else{
          var tl = new TimelineMax(), 
              node = $(tmpl.firstNode);

          //Cleans up inline styles
          var cleanNode = function () {
            console.log('cleaned')
            $(node).removeAttr("style");
          };

          //Anim timeline
          tl.to(node, 0.05, {background: 'rgba(255, 230, 35, 0)'})
            .to(node, 0.5, {background: 'rgba(255, 230, 35, 1)'})
            .to(node, 1.5, {background: 'rgba(255, 230, 35, 0)', onComplete:cleanNode}, "+=1.5");

          //Success Message
          App.successMessage('Star Added To Message');

        }
      });
    }
    //Removes Star
    function removeStar () {
      Meteor.call('removeMessageStar', params, function (error) {
        if (error) {
          console.warn(error);
          App.errorMessage(error.reason);
        }else{
          App.successMessage('Star Removed From Message');
        }
      });
    }
  },


});