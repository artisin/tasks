/***************************************************************/
/***************************************************************/
/*  Controller: messages
/*  Template: /client/views/messages.html
/***************************************************************/
/***************************************************************/


/***************************************************************/
/* Template States */
/***************************************************************/
/*******************************/
/* Template Created */
/*******************************/
Template.messages.created = function() {
  //console.log('messages created')

  /*******************************/
  // UI components
  /*******************************/
  var uiComponents = function () {
    var self = this;
    return {
      init: function () {
        console.log('init')
        this.messageComponents().init();
        this.nav().init();
      },
      nav: function () {
        var selfRef = this;
        return {
          init: function () {
            this.folderDropdown();
          },
          folderDropdown: function () {
            $('.messageFolderDropdown').dropdown({
              //settings
            });
            //Context
            var messageType = Session.get('messageTemplateContext').replace('Messages', '');
            //Set the defualt selection based on context
            messageType = messageType.toString();
            messageType = _.capitalize(messageType);
            $('.messageFolderDropdown').dropdown('set value', messageType);
          },
        };
      },
      messageComponents: function () {
          return {
            init: function () {
              var selfRef = this;
              Meteor.setTimeout(function () {
                selfRef.checkbox();
                selfRef.star();
                selfRef.tagAdder();
                selfRef.tagRemover();
              }, 10);
            },
            checkbox: function () {
              $('.ui.checkbox').checkbox();
            },
            star: function () {
              $('.ui.rating')
                .rating({
                  rating: 0,
                  maxRating: 1
                })
              ;
            },
            tagAdder: function () {
              $('.tagAdder').dropdown({
                action: 'hide',
                onShow: function () {
                  $(this).find('.tagSearch').val('');
                },
                onHide: function () {
                  //Reset Session
                  Session.set('tagAdder', {active: false, _id: null});
                }
              });
            },
            tagRemover: function () {
              $('.tagRemover').dropdown({
                action: 'hide',
                onHide: function () {
                  Session.set('tagRemover', {active: false});
                }
              });
            }
          };
        },
        /*---------------------------------------------*/
        // Selector
        /*---------------------------------------------*/
        selector: function () {
          var selfRef = this;
          return {
            /*-----------------------------*/
            /// For Single page
            /*-----------------------------*/
            pageSelection: function () {
              /**
               * Gets the id of the messeage via data attr
               * @param  {string} dataClass - class in which the dataAttr is
               * @return {[_id]}
               */
              var getMessageId = function (dataClass) {
                var messages = $('.messageList').find(dataClass);
                return _.map(messages, function (indvMessage) {
                  return $(indvMessage).data().templateid;
                });
              };

              /**
               * Unions new selection plus current
               * @param  {[_id]} newSelections id's
               * @return {[_id]}
               */
              var unionSelection = function (newSelections) {
                var currentSelections = Session.get('selectedMessages'),
                    unionSelected = _.union(currentSelections, newSelections);
                return unionSelected;
              };
              //Select All Messages on Page
              var all = function () {
                var allMessages = getMessageId('.indvMessage_Container');
                Session.set('selectedMessages', unionSelection(allMessages));
              };
              //Deselect all messages on page
              var none = function () {
                var allMessages = getMessageId('.indvMessage_Container'),
                    currentSelections = Session.get('selectedMessages'),
                    withoutSelection = _.map(currentSelections, function (message) {
                      if (!_.contains(allMessages, message)) {
                        return message;
                      }
                    });
                withoutSelection = _.compact(withoutSelection);
                Session.set('selectedMessages', withoutSelection);
              };
              //Select All Unread Messages on page
              var unRead = function () {
                var unReadMessages = getMessageId('.newMessage');
                Session.set('selectedMessages', unionSelection(unReadMessages));
              };
              //Select All Read Messages on page
              var read = function () {
                var readMessages = getMessageId('.readMessage');
                Session.set('selectedMessages', unionSelection(readMessages));
              };
              //Select All Stared MEssages on page
              var stared = function () {
                var allStars = $('.messageList').find('.star'),
                    selectedStars = _.map(allStars, function (star) {
                      if ($(star).rating('get rating') === 1) {
                        return star;
                      }
                    });
                //Clean
                selectedStars = _.compact(selectedStars);
                //Get id of stared messages
                var dataAttr = _.map(selectedStars, function (star) {
                  var parent = $(star).closest('.indvMessage_Container');
                  return getMessageId(parent);
                });
                Session.set('selectedMessages', unionSelection(_.flatten(dataAttr)));
              };

              return {
                all: all,
                none: none,
                unRead: unRead,
                read: read,
                stared: stared
              };
            },
            /*-----------------------------*/
            /// For Folder
            /*-----------------------------*/
            folderSelection: function () {
              /**
               * Queries Message Collection
               * @param  {selector} selector - mongodb
               * @param  {options} options  - mongodb
               * @return {cursor obj} - fetch()
               */
              var queryFolder = function (selector, options) {
                options = options === undefined ? {fields: {_id: 1}} : options;
                return selfRef.data.customCursor(selector, options);
              };
              /**
               * Extracts the _id from cursor
               * @param  {cursor} objList
               * @return {[_id]}
               */
              var generateIdList = function (objList) {
                return _.map(objList, function (obj) {
                  return obj._id;
                });
              };
              /**
               * Unions between two arrays
               * @param  {[_id]} newSelections
               * @return {[_id]}
               */
              var unionSelection = function (newSelections) {
                var currentSelections = Session.get('selectedMessages');
                return _.union(currentSelections, newSelections);
              };
              /**
               * Generates a mongoDb 'and' Selector
               * @param  {selector} newSelector - second part of selector
               * @return {selector query}
               */
              var generateAndSelector = function (newSelector) {
                var currentSelector = currentCursor(),
                    selectorArray = [],
                    selectorModifier = {},
                    modifierKey = '$and';
                //Push into array
                selectorArray.push(currentSelector);
                selectorArray.push(newSelector);
                //Create 'and' query
                selectorModifier[modifierKey] = selectorArray;
                return selectorModifier;
              };
              //Queries the controller to find current cursor
              var currentCursor = function () {
                return selfRef.data.currentCursor();
              };
              //Select All In Current Folder
              var all = function () {
                var selector = currentCursor(),
                    cursor = queryFolder(selector),
                    cursorIds = generateIdList(cursor);
                Session.set('selectedMessages', unionSelection(cursorIds));
              };
              //Select None In Current Folder - dependant on where user is (read/inbox/ect)
              var none = function () {
                var currentSelections = Session.get('selectedMessages'),
                    selector = currentCursor(),
                    cursor = queryFolder(selector),
                    cursorIds = generateIdList(cursor),
                    withoutFolder = _.map(currentSelections, function (id) {
                      if (!_.contains(cursorIds, id)) {
                        return id;
                      }
                    });
                withoutFolder = _.compact(withoutFolder);
                Session.set('selectedMessages', withoutFolder);
              };
              //Select All Un-Read In Current Folder
              var unRead = function () {
                var userId = Meteor.userId(),
                    selector = {hasNotRead: userId},
                    andSelector = generateAndSelector(selector),
                    cursor = queryFolder(andSelector),
                    cursorIds = generateIdList(cursor);
                Session.set('selectedMessages', unionSelection(cursorIds));
              };
              //Select All Read In Current Folder
              var read = function () {
                var userId = Meteor.userId(),
                    selector = {hasRead: userId},
                    andSelector = generateAndSelector(selector),
                    cursor = queryFolder(andSelector),
                    cursorIds = generateIdList(cursor);
                Session.set('selectedMessages', unionSelection(cursorIds));
              };
              //Select All Stared In Current Folder
              var stared = function () {
                var userId = Meteor.userId(),
                    selector = {stared: userId},
                    andSelector = generateAndSelector(selector),
                    cursor = queryFolder(andSelector),
                    cursorIds = generateIdList(cursor);
                Session.set('selectedMessages', unionSelection(cursorIds));
              };
              return {
                all: all,
                none: none,
                unRead: unRead,
                read: read,
                stared: stared
              };
            },
          };
        },
      };
    };
  
  this.messageUiComponents = uiComponents.call(this);
  this.uiSelector = uiComponents().selector;

  /***************************************************************/
  /* Vars */
  /***************************************************************/
  //*******************************/
  // Sessions
  /*******************************/
  
  //Default Messsage Cursor Filter
  Session.set('messageCursorFilter', undefined);

  //List of toggled/selected messages, added in indvMess
  Session.set('selectedMessages', []);

  //Message Thread - used for message thread template
  Session.set('messageThread', {active: false, messageId: null, parentThread: false});

  //Search Value
  Session.set('searchValue', null);

  //Tag Adder
  Session.set('tagAdder', {active: false, _id: null});

  //Tag Remover
  Session.set('tagRemover', {active: false});

  /*******************************/
  // Rec
  /*******************************/
  //Tag Search
  //If user is searching for a tag
  this.isFolderTagSearchActive = new ReactiveVar(false);
  this.tagFolderSearchResults = new ReactiveVar({}); 
  this.tagFolderSearchValue = ReactiveVar(null);

  this.tagFolderActiveTags = new ReactiveVar([]);
  this.isTagFolderActive = new ReactiveVar(false);

};


/*******************************/
/* Template Rendered */
/*******************************/


Template.messages.rendered = function() {
  Session.set('messageListHeight', $('.messageList').outerHeight());
  var tpI = Template.instance();

  //init ui compnets when there is a new message
  Tracker.autorun(function () {
    var count = Counts.get('inboxMessageCount');
      tpI.messageUiComponents.messageComponents().init();
  });

  //init ui components after searching
  Tracker.autorun(function () {
    var instance = EasySearch.getComponentInstance(
      { id : 'messageSearch', index : 'messages' }
    );
    //After Searching
    if (instance.get('searchingDone')) {
      tpI.messageUiComponents.messageComponents().init();
    }
    //If no longer Surching
    if (instance.get('currentValue') !== undefined) {
      if (instance.get('currentValue').length === 0) {
        tpI.messageUiComponents.messageComponents().init();
      }
    }
  });


//Action Bar
$('.dropdown').dropdown({
  action: 'hide',
  allowCategorySelection: true,
});

};


/***************************************************************/
/* Helpers */
/***************************************************************/
Template.messages.helpers({
  /*******************************/
  // Setup
  /*******************************/
  //Message Display
  messageSetup: function () {
    if (this.messageSubReady()) {
      var tpI = Template.instance();
      //Ui Componets
      //Runs everytime current page changes
      Session.get('currentPage');
      tpI.messageUiComponents.init();
    }
  },
  //Thread active
  //needed to make sure no extra data/sub is being sent when not needed
  isThreadActive: function () {
    var isActive = Session.get('messageThread').active;
    return isActive;
  },
  //*******************************/
  //  Nav Helpers
  /*******************************/

  newMessageCount: function () {
    var count = Counts.get('newMessageCount'); 
    if (count !== 0) {
      return {
        count: count,
        'labelColor': 'red'
      };
    }else{
      return {
        count: count,
        'labelColor': 'gray'
      };
    }
  },
  isFolderTagSearchActive: function () {
    return Template.instance().isFolderTagSearchActive.get();
  },
  commonTags: function () {
    var selector = {},
        key = ('users.'+Meteor.userId()+'.frequency');
    selector[key] = -1;
    //Filter by frequency
    return Tags.find({}, {sort: selector, limit: 4}).fetch();
  },
  tagFolderSearchResults: function () {
    var resultsInstance = Template.instance().tagFolderSearchResults.get();
    if (resultsInstance.count > 0) {
      return resultsInstance.results;
    }
  },
  noTagFolderagSearchResults: function () {
      if (Template.instance().tagFolderSearchResults.get().count === 0) {
        return true;
      }else{
        return false;
      }
  },
  nothingFoundMessage: function () {
    // console.log('nothing Found')
    // console.log(this.noMessageFound())
    return this.noMessageFound();
  }, 
  tagFolderSearchValue: function () {
    return Template.instance().tagFolderSearchValue.get();
  },
  tagFolderFilterActive: function () {
    var tpI = Template.instance().isTagFolderActive,
        navButtons = $('.messageNavButton'),
        tm = TweenMax;

    if (tpI.get()) {
      tm.to(navButtons, 0.4, {autoAlpha:0, onComplete:showTmpl});
    }
    
    function showTmpl () {
      tpI.set(true);
      Meteor.setTimeout(function () {
        tm.to(navButtons, 1, {autoAlpha:1});
      }, 100);
    }

    return tpI.get();
  },
  activeFolderTags: function () {
    var tpI = Template.instance(),
        currentActiveTags = tpI.tagFolderActiveTags.get();

    if (!_.isEmpty(currentActiveTags)) {
      Meteor.call('tagFolderFilter', currentActiveTags, function (error, result) {
        if (error) {
          App.errorMessage(error.reason);
        }else{
          if (!_.isEmpty(result)) {
            Session.set('messageCursorFilter', result);
            Session.set('messageTemplateContext', 'tagFolder');
            Router.go('messages.link', 
              {messageType:'tagFolder'});
          }else{
            Session.set('messageCursorFilter', 'noMessages');
            Session.set('messageTemplateContext', 'tagFolder');
            Router.go('messages.link', 
              {messageType:'noMessagesToDisplay'});
          }
        }
      });
    }else{
      //Sets defaults and sends user to newMessages
      tpI.isTagFolderActive.set(false);
      Session.set('messageTemplateContext', 'inboxMessages');
      Session.set('messageCursorFilter', undefined);
      Router.go('messages.link', {messageType: 'inboxMessages'});
    }

    //Gets tag info and map name and color
    var tags = Tags.find({}, 
        {fields: {tagColor: 1, tagName: 1}}, {sort:{tagName: 1}}).fetch(),
        tagInfo = _.map(tags, function (value) {
            if (_.contains(currentActiveTags, value._id)) {
              return value;
            }
          });
    tagInfo = _.compact(tagInfo);
    if (!_.isEmpty(tagInfo)) {
      return tagInfo; 
    }
  },

  /*******************************/
  // Sub-Nav Helpers
  /*******************************/
  //Used for the title to indicate where the user is
  currentContext: function () {
    var context = Session.get('messageTemplateContext');
    //Inbox
    if (context === 'inboxMessages') {return 'Inbox'}
    //New
    if (context === 'newMessages') {return 'New Messages'};
    //Read
    if (context === 'readMessages') {return 'Read Messages'};
    //Sent
    if (context === 'sentMessages') {return 'Sent Messages'};
  },
  //If searching - search value
  messageSearchValue: function () {
    return Session.get('searchValue');
  }
});

/***************************************************************/
/* Events */
/***************************************************************/
Template.messages.events({
  //Menu Icon
  'click #messagesMenuIcon': function (e) {
    e.preventDefault();
    var active = Template.instance().isSidebarActive;
    if (!active.get()) {
      $('#messagesMenuIcon').toggleClass('active');
      TweenMax.to('#messageSideBar', 0.5, {xPercent:-98});
      active.set(true);
    }else{
      $('#messagesMenuIcon').toggleClass('active');
      TweenMax.to('#messageSideBar', 0.5, {xPercent:0});
      active.set(false);
    }
  },

  /***************************************************************/
  /* Nav Bar */
  /***************************************************************/
  //Inbox Messages
  'click .inboxMessages': function (e) {
    e.preventDefault();
    Session.set('messageTemplateContext', 'inboxMessages');
    Router.go('messages.link', {messageType: 'inboxMessages'});
  },

  //New Messages
  'click .newMessages': function (e) {
    e.preventDefault();
    Session.set('messageTemplateContext', 'newMessages');
    Router.go('messages.link', {messageType: 'newMessages'});
  },
  //Read Messages
  'click .readMessages': function (e) {
    e.preventDefault();
    Session.set('messageTemplateContext', 'readMessages');
    Router.go('messages.link', {messageType: 'readMessages'});
  },
  //Read Messages
  'click .sentMessages': function (e) {
    e.preventDefault();
    Session.set('messageTemplateContext', 'sentMessages');
    Router.go('messages.link', {messageType: 'sentMessages'});
  },

  /*-------------------------------------------------------------*/
  /*-------------------------------------------------------------*/
  /* Action Bar
  /*-------------------------------------------------------------*/

  /*-----------------------------*/
  /// Delete Selected Messasge
  /*-----------------------------*/
  'click .deleteSelectedMessages': function (e, tmpl) {
    e.preventDefault();
    var selections = Session.get('selectedMessages'),
        messageThread = Session.get('messageThread');
    var params = {
      messages: selections
    };

    //If thread is open and user deletes that message close thread
    if (_.contains(selections, messageThread.messageId)) {
      messageThread.closeThread = true;
      Session.set('messageThread', messageThread);
    }

    Meteor.call('deleteMessage', params, function (error) {
      if (error) {
        console.log(error)
      }else{
        //Clear session of selected items
        Session.set('selectedMessages', []);
        App.successMessage('Message Deleted.');
      }
    });
  },

  /*-------------------------------------------------------------*/
  /*-------------------------------------------------------------*/
  /* Selector
  /*-------------------------------------------------------------*/
  
  /*---------------------------------------------*/
  // Current Page
  /*---------------------------------------------*/
  //All
  'click .cp_All': function (e) {
    e.preventDefault();
    Template.instance().uiSelector().pageSelection().all();
  },
  //None
  'click .cp_None': function (e) {
    e.preventDefault();
    Template.instance().uiSelector().pageSelection().none();
  },
  //Un-Read
  'click .cp_UnRead': function (e) {
    e.preventDefault();
    Template.instance().uiSelector().pageSelection().unRead();
  },
  //Read
  'click .cp_Read': function (e) {
    e.preventDefault();
    Template.instance().uiSelector().pageSelection().read();
  },
  //Stared
  'click .cp_Stared': function (e) {
    e.preventDefault();
    Template.instance().uiSelector().pageSelection().stared();
  },

  /*---------------------------------------------*/
  // Folder Selector
  /*---------------------------------------------*/
  'click .cf_All': function (e) {
    e.preventDefault();
    Template.instance().uiSelector().folderSelection().all();
  },
  'click .cf_None': function (e) {
    e.preventDefault();
    Template.instance().uiSelector().folderSelection().none();
  },
  'click .cf_UnRead': function (e) {
    e.preventDefault();
    Template.instance().uiSelector().folderSelection().unRead();
  },
  'click .cf_Read': function (e) {
    e.preventDefault();
    Template.instance().uiSelector().folderSelection().read();
  },
  'click .cf_Stared': function (e) {
    e.preventDefault();
    Template.instance().uiSelector().folderSelection().stared();
  },

  /*-----------------------------*/
  /// None
  /*-----------------------------*/
  'click .selectorNone': function (e) {
    e.preventDefault();
    Session.set('selectedMessages', []);
  },



  /*******************************/
  // Mark Message As Read
  /*******************************/
  'click .markAsRead': function (e) {
    e.preventDefault();
    var currentSelections = Session.get('selectedMessages'),
        userId = Meteor.userId(),
        tpI = Template.instance();

    /**
     * Queries minimongo to find unread messasges from the selcted messages
     * @return {[obj]} - with only the _id
     */
    var findUnread = function () {
      return Messages.find({$and: [
            {_id: {$in: currentSelections}}, 
            {hasNotRead: {$in: [userId]}}]},
            {fields: {_id: 1}}).fetch();
    };

    /**
     * Maps out ids
     * @return {[_id]}
     */
    var idList = function () {
      return _.map(findUnread(), function (obj) {
        return obj._id;
      });
    };

    Meteor.call('markMessageAsRead', idList(), function (error) {
      if (error) {
        console.log(error);
        App.errorMessage(error.reason);
      }else{
        //Reset selection
        tpI.messageUiComponents.unSelectAllMessages();
      }
    });
    
  },

  //Marks messsages as unread
  'click .markAsUnread': function (e) {
    e.preventDefault();
    var currentSelections = Session.get('selectedMessages'),
        userId = Meteor.userId(),
        tpI = Template.instance();
     /**
     * Queries minimongo to find read messasges from the selected messages
     * @return {[obj]} - with only the _id
     */
    var findUnread = function () {
      return Messages.find({$and: [
            {_id: {$in: currentSelections}}, 
            {hasRead: {$in: [userId]}}]},
            {fields: {_id: 1}}).fetch();
    };
    /**
     * Maps out ids
     * @return {[_id]}
     */
    var idList = function () {
      return _.map(findUnread(), function (obj) {
        return obj._id;
      });
    };

    Meteor.call('markMessageAsUnRead', idList(), function (error) {
      if (error) {
        console.log(error);
        App.errorMessage(error.reason);
      }else{
        //Reset selection
        tpI.messageUiComponents.unSelectAllMessages();
      }
    });    
  },

  /*******************************/
  // Tag Adder
  /*******************************/
  'click .groupTag': function (e) {
    e.preventDefault();
    var currentSelections = Session.get('selectedMessages');
    Session.set('tagAdder', {
      active: true,
      groupSelection: true,
      _id: currentSelections
    });
  },


  /*******************************/
  // Message Search
  /*******************************/
  //Search Input Value for messageSearchValue helper
  'keyup #messageSearch': function (e) {
    Session.set('searchValue', $(e.currentTarget).val());
  },

  'click .tagFolder': function (e, tmpl) {
    e.preventDefault();
    Template.instance().tagFolderSearchResults.set({});
    Template.instance().isFolderTagSearchActive.set(false);
    Session.set('messageTemplateContext', 'tagFolderMessages');
    tmpl.$('.tagFolderSearch').val('');
  },
  'keyup .tagFolderSearch': function (e) {
    e.preventDefault();
    var value = $(e.target).val().trim(),
        tpI = Template.instance();
        tpI.isFolderTagSearchActive.set(true);
        tpI.tagFolderSearchValue.set(value);
    EasySearch.search('tags',  value, function (err, data) {
      // use data.results and data.total
      if (err) {
        console.log(err);
      }else{
        tpI.tagFolderSearchResults.set({
          count: data.total, 
          results: data.results
        });
      }
    });
  },

  //Adds an exsisting tag
  'click .tagFolderFilter': function (e, tmpl) {
    e.preventDefault();
    //Session.set('messageCursorFilter', {tag: this.tagName});
    var id = this._id,
        hash = this.tagName,
        tpI = Template.instance(),
        currentActiveTags = tpI.tagFolderActiveTags.get();

    //Set the actual tags
    if (!_.contains(currentActiveTags, id)) {
      tpI.tagFolderActiveTags.set(_.compact(_.union(currentActiveTags, id)));
    }
    //Activates tmpl cahgne
    if (!tpI.isTagFolderActive.get()) {
      tpI.isTagFolderActive.set(true);
    }
  },
  'click .removeIndvTagFilter': function (e) {
    e.preventDefault();
    var tpI = Template.instance().tagFolderActiveTags,
        currentActiveTags = tpI.get(),
        newActiveTags = _.without(currentActiveTags, this._id);
    
    tpI.set(newActiveTags);
  },
});



