/******************************************************************************/
/******************************************************************************/
/*  Controller: controllerName 
/*  Template: /client/views/templateLocation
/******************************************************************************/
/******************************************************************************/

//////////////////*********** TODO ***********//////////////////
/// 1. Do we need/want a limit?
/// 2. When user scrolls past first message possibly display a to top icon
///   
///
//////////////////*********** TODO ***********//////////////////


/***************************************************************/
/* Template States */
/***************************************************************/
/*******************************/
// Created
/*******************************/
Template.messageThread.created = function () {
  //console.log('thread Created')
  /*******************************/
  // Subscriptions
  /*******************************/
  //Rec Var for filtering
  this.sortOption = new ReactiveVar({timestamp: -1});

  //Sub
    var messageThread = Session.get('messageThread'),
        //threadId = messageThread.messageId,
        threadSort = this.sortOption.get(),
        group = Partitioner.group(),
        subs = new SubsManager(),
        threadCursor;

    var parentSub = function () {
      //Parent Thread
      var messageThread = Session.get('messageThread'),
          parentThread = messageThread.parentThread;
      threadCursor = {path: {$all: [parentThread]}};
      return this.messageThreadHandle = subs.subscribe('messages', 
        threadCursor,
        threadSort,
        group);
    }; 
    this.parentSub = parentSub;

    //Called from  message sub helper, if user selecets first message of thread
    //and there are siblings we must modify the sub since the original sub was
    //under the impression there was no parent
    var modifySub = function () {
      var messageThread = Session.get('messageThread'),
          parentThread = messageThread.parentThread;
      threadCursor = {path: {$all: [parentThread]}};
      return this.messageThreadHandle = subs.subscribe('messages', 
        threadCursor,
        threadSort,
        group);
    };
    this.modifySub = modifySub;


  /*******************************/
  // UI components
  /*******************************/
  var uiComponents = function () {
    var self = this,
        meassageWrapper = '#message_TmplWrapper',
        threadWrapper = '.messageThread_Wrapper';
    return {
      tagAdder: function () {
        Meteor.setTimeout(function () {
          $('.tagAdder').dropdown({
            action: 'hide'
          });
        }, 50);
      },
      messageThread: function () {
        var tl = new TimelineMax();
        return {
          init: function () {
            tl.to(threadWrapper, 0.75, {xPercent:50, opacity: 1, force3D:true}, "init")
              .to(meassageWrapper, 0.75, {width: '50%', force3D:true}, "init");
          },
          destroy: function () {
            tl.to(threadWrapper, 0.75, {xPercent:100, opacity: 0, force3D:true}, "init")
              tl.to(meassageWrapper, 0.75, {width: '100%', force3D:true}, "init");
          }
        };
      },
      messageReply: function (e, messageId) {
        var tpI = Template.instance(),
            tl = new TimelineMax(),
            target = e.currentTarget,
            container = $(target).closest(".threadMessage"),
            containerChildren = container.find('.messageReply_Container').children(),
            replyContainer = containerChildren.siblings('.form_Container'),
            footer = containerChildren.siblings('.footer'),
            containerHeight = container.outerHeight();
        return {
          init: function () {
          tl.to(container, 0.5, {height: containerHeight + 182}, "init")
            .to(footer, 0.5, {autoAlpha:0, onComplete:initReply}, "init")
            .to(replyContainer, 0.5, {autoAlpha: 1});
            function initReply () {
              tpI.isMessageReply.set({active: true, messageId: messageId});
            }
          },
          destroy: function () {
            tl.to(replyContainer, 0.5, {opacity: 0})
              .to('.messageReplyForm', 0.1, {css: {display: 'none'}})
              .to(container, 0.5, {height: containerHeight - 182})
              .to(footer, 0.75, {autoAlpha:1, onComplete:complete});

            //Completed animation
            function complete () {
              tpI.isMessageReply.set({active: false, messageId: null});
            }

          }
        };
      },
      messageValidation: function  () {
        return $('.messageReplyForm').validate({
          rules: {
            messageReply: {
              required: true,
              minlength: 3,
              maxlength: 2000,
            },
          },
          focusCleanup: true,
          highlight: function(element) {
            $(element).parent().addClass("error");
          },
          unhighlight: function(element) {
            $(element).parent().removeClass("error");
          },
          messages: {
            messageReply: {
              required: "Please enter a message to reply!",
              minlength: "Please use at least three characters.",
              maxlength: "Sorry your message cannot be longer than 2000 characters"
            },
          },
        });
      }
    };
  };
  this.threadUiComponents = uiComponents.call(this);


  //*******************************/
  // Events
  /*******************************/
  //Submits a reply on the thread
  var submitReply = function (e, tmpl, reply) {
    var tpI = Template.instance();
    
    var params = {
      messageReply: reply,
      messageId: this._id,
      parent: this.parent,
      path: this.path,
      recipiant_Id: this.recipiant_Id,
      recipiantList: this.recipiantList,
      previousSender_Id: this.sender_Id,
      previousSenderName: this.senderName,
      subject: this.subject
    };

    var messageThread = Session.get('messageThread');
    //Clean up animation
    tpI.threadUiComponents.messageReply(e).destroy();
    //Send Reply after animation completed
    Meteor.setTimeout(function () {
      //debugger;
      Meteor.call('messageReply', params, function (error, result) {
          if (error) {
            console.log(error);
            App.errorMessage(error.reason);
          }
      });
    }, 1500);
  };
  this.submitReply = submitReply;



  //*******************************/
  // Rec - Vars
  /*******************************/

  //Current Thread Id
  this.currentThread = new ReactiveVar(null);

  //Has the user activated the reply
  this.isMessageReply = new ReactiveVar({active: false});

  //Id of newest message, for reply, since only the newest message can be reply
  this.newestThread = new ReactiveVar(null);
  
  //Thread count
  this.threadCount = new ReactiveVar(0);

  //Scroll to top icon - active if user in not at top of page
  this.scrollToTopIcon = new ReactiveVar(false);

  //Sets Defaults
  var setDefualts = function () {
    this.isMessageReply.set({active: false});
    this.threadUiComponents.tagAdder();
  };
  this.setDefualts = setDefualts;

  //Closes Thread
  var closeThread = function () {
    this.threadUiComponents.messageThread().destroy();
    Session.set('messageThread', {active: false, messageId: null, parentThread: null});
  };
  this.closeThread = closeThread;

};

/*******************************/
// Rendered
/*******************************/
Template.messageThread.rendered = function () {
  TweenMax.set('.messageThread_Wrapper', {xPercent:100});
  this.threadUiComponents.tagAdder();
  var tl = new TimelineMax(),
      self = this;

  //Animation to ScrollTo message
  Tracker.autorun(function () {
    var messageThread = Session.get('messageThread'),
        messageId = (messageThread.messageId),
        threadCount = self.threadCount.get();

    //Set defualt margin-bottom incase it was changed
    $('.noMoreMessages').css({"margin-bottom": 0});

    //Only scrollTo if there is more then one thread
    if (threadCount > 2) {
      Meteor.setTimeout(function () {
        var scrollPosition = $('.threadBody_Container').scrollTop(),
            target = $(App.dataTarget('.threadMessage', 'thread_id', messageId)),
            notFirstPost = ($(target).position().top !== 95);

        //Set No more Messages margin-bottom for better ux on the scroll
       $('.noMoreMessages').css({"margin-bottom": $('.messageThread_Container').height()-95});

        //Only scrollTo if the user has selected a thread that is not the first post
        if (notFirstPost) {
          tl.to('.threadBody_Container', 2, {scrollTo:{
              y:($(target).position().top-95)+scrollPosition}, ease:Quad.easeOut})
            .to($(target), 0.75, {boxShadow: "0px 3px 19px 0px rgba(50, 50, 50, 0.75)"})
            .to($(target), 0.75, {boxShadow: "0px 0px 0px 1px rgba(39, 41, 43, 0.15), 0px 1px 4px 0px rgba(0, 0, 0, 0.15)"})
        }
      }, 100);
    }

    //Sets defaults is thread is no longer active
    if (messageThread.closeThread === true) {
      self.closeThread();
    }

  });


};

/*******************************/
// Destroyed
/*******************************/
Template.messageThread.destroyed = function () {
  console.log('destroy');
};


/***************************************************************/
/* Template Helpers */
/***************************************************************/
Template.messageThread.helpers({
  /*******************************/
  // Setup
  /*******************************/
  //Message Sub Ready
  threadSubReady: function () {
    var tpI = Template.instance(),
        messageThread = Session.get('messageThread'),
        subHandle;

    //Parent Sub
    if (messageThread.parentThread) {
      subHandle = tpI.parentSub();
      if (subHandle.ready()) {
        return true;
      }
    }

    //indv Sub  
    if (!messageThread.parentThread) {
      var messageId = messageThread.messageId;
      var newThreadObj = {};
      newThreadObj.active = messageThread.active;
      newThreadObj.messageId = messageId;
      newThreadObj.parentThread = messageId;
      //Set new session
      Session.set('messageThread', newThreadObj);
      //Calls the mod sub function and assigns a sub handle
      subHandle = tpI.modifySub();
      if (subHandle.ready()) {
        return true;
      }
    }


  },
  //UI init
  isThreadActive: function () {
    var messageThread = Session.get('messageThread'),
        tpI = Template.instance(),
        currentThread = tpI.currentThread.get();

    //Checks to see is session has been set
    if (messageThread.active) {
      tpI.threadUiComponents.messageThread().init();
    }else{
      tpI.setDefualts();
    }

    //Sets currentThread is not set
    if (currentThread === null) {
      tpI.currentThread.set(messageThread.messageId);
    }

    //Check to see if currentThread differs from session if so defaults
    if (currentThread !== null && currentThread !== messageThread.messageId) {
      tpI.setDefualts();
    }

    return messageThread.active;
  },

  /*******************************/
  // Cusor info
  /*******************************/
  messageThread: function () {
    var messageThread = Session.get('messageThread'),
        parentThread = messageThread.parentThread,
        tpI = Template.instance(),
        sortOption = tpI.sortOption.get(),
        filterOptions = {},
        messageInfo;
    
    //Set filter options
    filterOptions.sort = sortOption;

    //Has Parent Messages
    if (parentThread) {
      messageInfo = Messages.find({path: {$all: [parentThread]}}, filterOptions);
      var firstMessage = messageInfo.fetch()[0];
      //Sets newest message for message reply helper
      tpI.newestThread.set(firstMessage._id);
     
      //Sets thread count
      var messageCount = messageInfo.count();
      tpI.threadCount.set(messageCount);
      
    }


    return messageInfo;
  },

  /*******************************/
  /* Thread Details -Sender/Reciver/count
  /*******************************/
  threadCount: function () {
    return Template.instance().threadCount.get();
  },
  threadSender: function () {
    var sender;
    if (this.sender_Id === Meteor.userId()) {
      sender = '<i class="user icon"></i>';
    }else{
      sender = this.senderName;
    }
    return sender;
  },
  threadRecipiants: function () {
    var recipiantId = this.recipiant_Id,
        recipiantName;
    //if since recipiant
    if (recipiantId.length === 1) {
      if (recipiantId[0] === Meteor.userId()) {
        recipiantName = '<i class="user icon"></i>';
      }else{
        recipiantName = _.map(this.recipiantList, function (value) {
          return _.values(value);
        });
      }
    }else{
      recipiantName = _.map(this.recipiantList, function (value, key) {
        //console.log(value)
        //console.log(key)
        return 'Mutiple';
      });
    }
    return recipiantName;
  },
  //Color of thread
  segmentType: function () {
    return Meteor.userId() === this.sender_Id ? 'blue' : 'green';
  },
  //custom class for scrollTo
  messageId: function () {
    return this._id;
  },
  /*******************************/
  // Actions
  /*******************************/
  //Can only reply to newest message
  isNewestMessage: function () {
    var newestThread = Template.instance().newestThread.get();
    return newestThread === this._id ? true : false;
  },
  isMessageReply: function (id) {
    var tpI = Template.instance(),
        reply = tpI.isMessageReply.get();

    if (reply.active) {
      Meteor.setTimeout(function () {
        tpI.threadUiComponents.messageValidation();
      }, 10);
    }

    //Only return true for active reply
    return reply.messageId === id ? true : false;
  },

  /*******************************/
  /* Tags */
  /*******************************/
  //Gets local tag collection, and publishes tag to corresponding message
  currentTags: function () {
    var userId = Meteor.userId(),
        self = this;
    var tags = _.map(LocalTags.find().fetch(), function(value){
      if (_.contains(value.users[userId].inDocs, self._id)) {
        return {
          tagName: value.tagName,
          tagColor: value.tagColor,
          _id: value._id
        };
      }
    });
    var hasTags = _.compact(tags).length !== 0 ? true : false,
        ifTagHeight = hasTags ? '25px' : '50px';
    return {
      tag: _.compact(tags),
      hasTags: hasTags,
      ifTagHeight: ifTagHeight
    };
  },
});


/***************************************************************/
/* Template Events */
/***************************************************************/
Template.messageThread.events({
  'click .backButton': function (e) {
    e.preventDefault();
    var tpI = Template.instance();
    tpI.threadUiComponents.messageThread().destroy();
    Meteor.setTimeout(function () {
      Session.set('messageThread', {active: false, messageId: null, parentThread: false});
    }, 750);
  },
  /*******************************/
  // Action Buttons
  /*******************************/
  //Reply
  'click .threadReply': function (e) {
    e.preventDefault();
    var messageId = this._id;
    Template.instance().threadUiComponents.messageReply(e, messageId).init();
  },
  //Cancle Repy
  'click .cancleReplyButton': function (e) {
    e.preventDefault();
    Template.instance().threadUiComponents.messageReply(e).destroy();
  },
  'click .submitReplyButton': function (e, tmpl) {
    e.preventDefault();
    var reply = App.inputValue('messageReply', tmpl),
        messageReply = Template.instance().submitReply;

    //Message Reply  
    messageReply = _.bind(messageReply, this, e, tmpl, reply);
    messageReply();
  },
  //For when the user hits enter to submit reply
  'keypress textarea.messageReply': function (e, tmpl) {
    var reply = App.inputValue('messageReply', tmpl);

    //Enter Key
    if (e.which === 13) {
      var messageReply = Template.instance().submitReply;
      //Message Reply
      messageReply = _.bind(messageReply, this, e, tmpl, reply);
      messageReply();
    }
  },
  'click .deleteMessage': function (e, tmpl) {
    e.preventDefault();
    console.log('delete ') 
  },
});