/***************************************************************/
/***************************************************************/
/*  Controller: Recover Password
/*  Template: /client/views/public/recover-password.html
/***************************************************************/
/***************************************************************/

/***************************************************************/
/* Template States */
/***************************************************************/

Template.recoverPassword.rendered = function() {
  /*******************************/
  /* GSAP Animation */
  /*******************************/
  var tm = TweenMax,
      tl = new TimelineMax(),
      LayoutContainer = $('#PublicContainer'),
      //Intro
      mianLock = $('.mainIcon'),
      headerContent = $('.mainHeaderContent'),
      email = $('.emailInput'),
      buttons = $('.recoverButtons_Container'),
      //Sucsess
      successIcon = $('.sucessIcon'),
      mainSecHead = $('.mainSuccsessHeader'),
      subSecHead =$('.subSuccsessHeader'),
      succsessButton = $('.succsessButton');

  //Set Eleements
  //Into Elements
  tm.set(mianLock, {autoAlpha:0, transformPerspective:400, 
                perspective:400, transformStyle:"preserve-3d"});
  tm.set([headerContent, email, buttons], {autoAlpha:0});
  //Success Elements
  tm.set([successIcon, mainSecHead, subSecHead, succsessButton],{autoAlpha:0});

  //Timeline
  tl.to(LayoutContainer , 0.2, {autoAlpha:1})
    .to(mianLock, 2.5, {autoAlpha:1, rotationX:360, rotationY:360, force3D: true})
    .to(headerContent, 1, {autoAlpha:1}, "init-=2")
    .to(email, 1, {autoAlpha:1}, "init-=1.5")
    .to(buttons, 1, {autoAlpha:1}, "init-=1");


  /*******************************/
  /* Form Jquery Validation */
  /*******************************/
  return $('#recoverPassword').validate({
    rules: {
      emailAddress: {
        required: true,
        email: true
      }
    },
    highlight: function(element) {
      $(element).parent().addClass("error");
    },
    unhighlight: function(element) {
      $(element).parent().removeClass("error");
    },
    messages: {
      emailAddress: {
        required: "Please enter your email address",
        email: "Please enter a valid email address."
      }
    }
  });
};

/***************************************************************/
/* Helpers */
/***************************************************************/


/***************************************************************/
/* Events */
/***************************************************************/
Template.recoverPassword.events({
  'submit #recoverPassword': function(e, tmpl) {
    e.preventDefault();
    //Grab the user email
    var email = tmpl.find('[name="emailAddress"]').value.trim()
    return Accounts.forgotPassword({
        email: email
      }, function(error) {
        if (error) {
          inputError($('#recoverP_emialInput'), error.reason)
          //Create a error in local collection
          ErrorMessage.insert({errormessage: error.reason})
        }else{
        /*******************************/
        /* GSAP Success Animation */
        /*******************************/
        //If Success, fadeout and hide display
        var tl2 = new TimelineMax(),
            mainContainer = $('#recoverP_Container'),
            successContainer = $('#recoverP_Success'),
            successIcon = $('.sucessIcon'),
            mainSecHead = $('.mainSuccsessHeader'),
            subSecHead =$('.subSuccsessHeader'),
            succsessButton = $('.succsessButton');
        
        //Timeline
        tl2.to(mainContainer, 0.5, {autoAlpha:0, 
              onComplete:hideAndShow})
            .to(successIcon, 0.75, {autoAlpha:1}, "init")
            .to(mainSecHead, 0.75, {autoAlpha:1}, "init")
            .to(subSecHead, 0.75, {autoAlpha:1})
            .to(succsessButton, 0.75, {autoAlpha: 1});
        }
        //Hides the main Container and shows the succsess
        function hideAndShow(){
          mainContainer.hide();
          successContainer.show();
        }
      });
  },
  'click .succsessButton': function () {
    //Hides container when user leaves page
    $('#recoverP_Success').hide();
  }
});