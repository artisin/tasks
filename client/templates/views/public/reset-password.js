/***************************************************************/
/***************************************************************/
/*  Controller: Reset Password
/*  Template: /client/views/public/reset-password.html
/***************************************************************/
/***************************************************************/

/***************************************************************/
/* Template States */
/***************************************************************/

/*******************************/
/* Tmpl Created */
/*******************************/
Template.resetPassword.created = function() {};

/*******************************/
/* Tmpl Rendered */
/*******************************/
Template.resetPassword.rendered = function() {
  /*******************************/
  /* GSAP Animation */
  /*******************************/
  var tm = TweenMax,
      tl = new TimelineMax(),
      LayoutContainer = $('#PublicContainer'),
      lock = $('.unlock'),
      content = $('.content'),
      newPassword = $('.newPassword_Container'),
      repeatPassword = $('.repeatPassword_Container'),
      buttons = $('.resetButtons_Container');

  //Set Elements
  tm.set(lock, {autoAlpha:0, transformPerspective:400, 
                perspective:400, transformStyle:"preserve-3d"});
  tm.set([content, newPassword, repeatPassword, buttons], {autoAlpha:0});

  //Timeline
  tl.to(LayoutContainer , 0.2, {autoAlpha:1})
    .to(lock, 2.5, {autoAlpha:1, rotationX:360, rotationY:360, force3D: true})
    .to(content, 0.5, {autoAlpha:1}, "-=2")
    .to(newPassword, 0.5, {autoAlpha:1}, "-=1.5")
    .to(repeatPassword, 0.5, {autoAlpha:1}, "-=1")
    .to(buttons, 0.5, {autoAlpha:1}, "-=0.5");

  /*******************************/
  /* Form Jquery Validation */
  /*******************************/
  return $('#resetPassword_Form').validate({
    rules: {
      newPassword: {
        required: true,
        minlength: 6
      },
      repeatNewPassword: {
        required: true,
        minlength: 6,
        equalTo: "[name='newPassword']"
      }
    },
    highlight: function(element) {
      $(element).parent().addClass("error");
    },
    unhighlight: function(element) {
      $(element).parent().removeClass("error");
    },
    messages: {
      newPassword: {
        required: "Please enter a new password.",
        minlength: "Password must be at least six characters."
      },
      repeatNewPassword: {
        required: "Please repeat your new password.",
        equalTo: "Your password does not match."
      }
    },
  });
};

/***************************************************************/
/* Helpers */
/***************************************************************/
Template.resetPassword.helpers({});

/***************************************************************/
/* Events */
/***************************************************************/
Template.resetPassword.events({
  'submit #resetPassword_Form': function(e, t) {
      //Grabs the users reset token and new password
      var token = Session.get('resetPasswordToken');
      var newPassword = t.find('#newPassword').value;
      //Resets the user's password
      Accounts.resetPassword(token, newPassword, function(error) {
        if (error){
           ErrorMessage.insert({errormessage: error.reason});
        }
      });
    return false; 
    }
});