/***************************************************************/
/***************************************************************/
/*  Controller: Login
/*  Template: /client/views/public/login.html
/***************************************************************/
/***************************************************************/

/***************************************************************/
/* Template States */
/***************************************************************/
/*******************************/
/* Tmpl Created */
/*******************************/
Template.login.created = function() {};

/*******************************/
/* Tmpl Rendered */
/*******************************/
Template.login.rendered = function() {
  
  /*******************************/
  /* GreenSock */
  /*******************************/
  //Var declaration
  var tm = TweenMax,
      tl = new TimelineMax(),
      LayoutContainer = $('#PublicContainer'),
      icon = $('.sign'),
      mainHeader = $('.mainHeader'),
      subHeader = $('.sub'), 
      form = $('#application-login'),
      button = $('.loginRegisterButton_Container'),
      divider = $('.divider_Container');
  
  //Set Position
  tm.set(icon, {autoAlpha:0});
  tm.set(mainHeader, {xPercent:-110, force3D:true, autoAlpha:0});
  tm.set(subHeader, {xPercent:110, force3D:true, autoAlpha:0});
  tm.set(form, {autoAlpha:0});
  tm.set(button, {autoAlpha:0});
  tm.set(divider, {height: '0px', force3D:true, autoAlpha:0});

  //Timeline
  tl.to(LayoutContainer, 0.2, {autoAlpha:1})
    .to(icon, 1, {autoAlpha:1}, "header")
    .to(mainHeader, 1, {xPercent:0, force3D:true, autoAlpha:1}, "header")
    .to(subHeader, 1, {xPercent:0, force3D:true, autoAlpha:1}, "header")
    .to(form, 2.5, {autoAlpha:1}, "init-=0.25")
    .to(button, 2.5, {autoAlpha:1}, "init-=0.25")
    .to(divider, 1, {height: '233px', force3D:true, autoAlpha:1}, "init-=0.25");

  /*******************************/
  /* Form Jquery Validation */
  /*******************************/
  return $('#application-login').validate({
    rules: {
      logincredential: {
        required: true,
      },
      password: {
        required: true
      }
    },
    highlight: function(element) {
      $(element).parent().addClass("error");
    },
    unhighlight: function(element) {
      $(element).parent().removeClass("error");
    },
    messages: {
      logincredential: {
        required: "Please enter your username or email address to login."
      },
      password: {
        required: "Please enter your password to login."
      },
    },
  });
};


/***************************************************************/
/* Events */
/***************************************************************/
Template.login.events({
  'submit #application-login': function(e, tmpl) {
    e.preventDefault();

    var loginCredential= tmpl.find('[name="logincredential"]').value.trim(),
        userPassword = tmpl.find('[name="password"]').value,
        filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/,
        userCredential = null;

    //Determins if user iputs username or emial
    if (filter.test(loginCredential)) {
      userCredential = loginCredential;
    }else{
      userCredential = loginCredential;
    }

    return Meteor.loginWithPassword(userCredential, userPassword, function(error) {
      if (error) {
        
        //If statments append error class and label to input if error
        if (error.reason === "User not found") {
          App.inputError($('.input-username'), error.reason);}
        if (error.reason === "Incorrect password") {
          App.inputError($('.input-password'), error.reason);}

        //Error popup message
        App.errorMessage(error.reason);
      }
    });
  },
});
