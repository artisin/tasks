/***************************************************************/
/***************************************************************/
/*  Controller: register
/*  Template: /client/views/public/register.html
/***************************************************************/
/***************************************************************/
/***************************************************************/
/* Template States */
/***************************************************************/
/*******************************/
/* Tmpl Created */
/*******************************/
Template.register.created = function() {
  //Controlls the tmpl injection of the token input
  this.userInputToken = new ReactiveVar(false);
  //Determins if the token input is empty
  //As of right now this var does not really do anything
  //It is more for demo of the modal 
  this.userTokenEmpty = new ReactiveVar(true);
};

/*******************************/
/* Tmpl Rendered */
/*******************************/
Template.register.rendered = function() {
  /*******************************/
  /* Greensock */
  /*******************************/
  var tm = TweenMax,
      tl = new TimelineMax(),
      LayoutContainer = $('#PublicContainer'),
      headerIcon = $('.angle'),
      mainHeader = $('.mainHeader'),
      subHeader = $('.sub'),
      container = $('.formRegister_Container'),
      ribbon = $('.tokenRibbon'),
      f1 = $('.f1'),
      f2 = $('.f2'),
      f3 = $('.f3'),
      f4 = $('.f4'),
      button = $('.registerButton_Container '),
      member = $('.allReadyMember');

  //Set Positions
  tm.set([
    headerIcon,container,
    f1,f2,f3,f4, ribbon,
    button, member], {autoAlpha:0});
  tm.set(mainHeader, {xPercent: -110, force3D:true, autoAlpha:0});
  tm.set(subHeader, {xPercent: 110, force3D:true, autoAlpha:0});

  //Timeline
  tl.to(LayoutContainer, 0.2, {autoAlpha:1})
    .to(headerIcon, 1, {autoAlpha:1}, "header")
    .to(mainHeader, 1, {xPercent: 0, force3D:true, autoAlpha:1}, "header")
    .to(subHeader, 1, {xPercent: 0, force3D:true, autoAlpha:1}, "header")
    .to(container, 0.5, {autoAlpha:1}, "-=0.5")
    .to(ribbon, 0.5, {autoAlpha:1}, "-=0.25")
    .to(f1, 0.5, {autoAlpha:1}, "-=0.25")
    .to(f2, 0.5, {autoAlpha:1}, "-=0.25")
    .to(f3, 0.5, {autoAlpha:1}, "-=0.25")
    .to(f4, 0.5, {autoAlpha:1}, "-=0.25")
    .to(button, 1, {autoAlpha:1}, "-=0.25")
    .to(member, 1, {autoAlpha:1}, "-=0.75");

  /*******************************/
  /* Form Jquery Validaion */
  /*******************************/
  return $('#register_Form').validate({
    rules: {
      username: {
        required: true,
        minlength: 3,
        maxlength: 15,
      },
      emailAddress: {
        required: true,
        email: true
      },
      password: {
        required: true,
        minlength: 6
      },
      cpassword: { 
        required: true, 
        equalTo: "#password", 
        minlength: 6
      }
    },
    focusCleanup: true,
    highlight: function(element) {
      $(element).parent().addClass("error");
    },
    unhighlight: function(element) {
      $(element).parent().removeClass("error");
    },
    messages: {
      username: {
        required: "Please enter a username to sign up",
        minlength: "Please use at least three characters.",
        maxlength: "You username cannot be 15 characters or longer"
      },
      emailAddress: {
        required: "Please enter your email address.",
        email: "Please enter a valid email address."
      },
      password: {
        required: "Please enter a password to sign up.",
        minlength: "Please use at least six characters."
      },
      cpassword: {
        required: "Please enter a password to sign up.",
        minlength: "Please use at least six characters.",
        equalTo: "Please enter matching passwords."
      }
    },
  });
};
/***************************************************************/
/* Helpers */
/***************************************************************/
Template.register.helpers({
  tokenAdded: function(){
    return Session.get('emailToken');
  },
  addManualToken: function () {
    return Template.instance().userInputToken.get();
  }
});

/***************************************************************/
/* Events */
/***************************************************************/
Template.register.events({
  //New User Registration Form Submit
 'submit #register_Form': function (e, tmpl) {
    e.preventDefault();
    //Grab the user reg data
    var registerName = App.inputValue('username', tmpl).trim(),
        registerEmail = App.inputValue('emailAddress', tmpl).trim(),
        registerPassword = App.inputValue('password', tmpl),
        token = null,
        inputToken = Template.instance().userInputToken.get();
    //User inputs
    var newUser = {
        username: registerName,
        email: registerEmail,
        password: registerPassword,
        token: token};


    /*******************************/
    /* Token Check */
    // Determins how to go about adding user
    /*******************************/   
    if(Session.get('emailToken')){
      token = Session.get('emailToken');
      newUser.token = token;
      tokenRegistration(newUser);}
    //If user has activated the manual token input
    else if (inputToken){
      token = App.inputValue('manualToken', tmpl);
      newUser.token = token;
      token !== "" ? tokenRegistration(newUser) : token=null;
      //If user has left the token input blank confim they want to procceed
      if (token===null && inputToken) {
        Modal.overlay({
          template: 'modalConfirm',
          dataContext:{
            title: 'Token',
            icon: 'warning red',
            content: 'Warning you have selected to input a token yet the field is empty. Would you still like to proceed?',
            _reactiveVar: Template.instance().userTokenEmpty
          },
          action:{
            confirmModal: function() {
              return addNewUser(newUser);
            },
            cancelModal: function() {
              return this._reactiveVar.set(true);
            }
          }
        });
      }
    }
    //Reg without a token
    if (!inputToken) {addNewUser(newUser);}


    /*******************************/
    /* Add User Functions */
    /*******************************/
    //Added new user (without token) 
    function addNewUser (newUser){
      Meteor.call('newUserRegistration', newUser, function (error) {
        if(error){
          errorReporting(error);
        }else{
          Accounts.createUser(newUser, function (error) {
            if (error) {
              errorReporting(error);
            }
          });
        }
      });
    }
    //Add new user (with token)
    function tokenRegistration (newUser) {
      Meteor.call('tokenRegistration', newUser, function (error) {
        //Checks to make use username/email are avalible and if token is valid
        if (error) {
          errorReporting(error);
        }else{
            //If all is dandy lets log the user in
            Meteor.loginWithPassword(newUser.email, newUser.password, function(error) {
              if (error) {
                return ErrorMessage.insert({errormessage:error.reason});
              }else{
                return Router.go('/');
              }
            });
          }
      });
    }


    /*******************************/
    /* Error Reporting */
    // Will flag and display any errors
    /*******************************/
    function errorReporting (error) {
        //Username errors, will dipslay error on new label
      if (error.reason === "Username already exists.") {
        App.inputError($('.register-username'), error.reason);}
      if (error.error === "username") {
        App.inputError($('.register-username'), error.reason);}

      //Email errors, will dipslay error on new label
      if(error.reason === "Email already exists."){
        App.inputError($('.register-email'), error.reason);}
      if(error.error === "email"){
        App.inputError($('.register-email'), error.reason);}

      //Password errors, will dipslay error on new label
      if(error.error === "password"){
        App.inputError($('#password'), error.reason);}

      //Token errors, will dipslay error on new label
      if (error.error === "Bad Token Match") {
        App.inputError($('.manualToken'), error.reason);}
      
      //Error popup message
      ErrorMessage.insert({errormessage: error.reason});
    }
  },
  //Token Ribion animation and tempalate transition
  'click .ribbonContent': function(e, tmpl){
    var elm1 = $('.addToken'),
        elm1a = $('.allReadyMember_Container'),
        elm2 = $('.addingToken'),
        elm2a = $('.addTokenInput');
    //Blaze Template Transition
    App.animateComp.components(
      Template.instance().userInputToken, 
      tmpl, 
      [elm1, elm1a], 
      [elm2, elm2a], 
      1, 
      'Quad.easeIn' 
    );
  }
});

