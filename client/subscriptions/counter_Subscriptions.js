/***************************************************************/
/* Subscriptions */
/***************************************************************/

//Moved sub to controller 
//UserGroup
// Deps.autorun(function() {
//   console.log('group')
//   var group = Partitioner.group();
//   Meteor.subscribe("UserGroup", group);
// });

// Deps.autorun(function() {
//   var group = Partitioner.group();
//   Meteor.subscribe('notifications', 'init');
// });

//New Message Count
Meteor.subscribe('newMessageCount');

//Inbox Message Count
Meteor.subscribe('inboxMessageCount');