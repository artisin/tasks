 /***************************************************************/
/* Local Error Collection */
/***************************************************************/
//Establish local collection
SuccessMessage = new Mongo.Collection(null);


/***************************************************************/
/* Helper */
/***************************************************************/
Template.SuccessMessage.helpers({
  Success: function () {
    //Controls success handeling
    //Only one success will be displayed at a time
    var successCount = SuccessMessage.find().count();
    var currentSuccess = SuccessMessage.find().fetch()[0];
    if (successCount === 0) {
      return false;
    }else if (successCount === 1){
      //Cleanup func removes the success message after 4s
      cleanUp();
      return [currentSuccess];
    }else{
      SuccessMessage.remove({_id: currentSuccess._id});
    }

    //success Message Removal
    function cleanUp () {
      Meteor.setTimeout(function () {
        SuccessMessage.remove({_id: currentSuccess._id});
      }, 4500);
    }
  }
});

/***************************************************************/
/* Events */
/***************************************************************/
Template.SuccessMessage.events({
  'click .close': function () {
    //Message fadeout upon x click
    $('.message').fadeOut();
    //Remove success message
    var oldError = SuccessMessage.find().fetch()[0]
    SuccessMessage.remove({_id: oldError._id});
  }
});