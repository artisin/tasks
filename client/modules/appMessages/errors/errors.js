 /***************************************************************/
/* Local Error Collection */
/***************************************************************/
//Establish local collection
ErrorMessage = new Mongo.Collection(null);


/***************************************************************/
/* Helper */
/***************************************************************/
Template.ErrorMessage.helpers({
  Error: function () {
    //Controls error handeling
    //Only one error will be displayed at a time
    var errorCount = ErrorMessage.find().count();
    var currentError = ErrorMessage.find().fetch()[0];
    if (errorCount === 0) {
      return false;
    }else if (errorCount === 1){
      //Cleanup func removes the error message after 4s
      cleanUp();
      return [currentError];
    }else{
      ErrorMessage.remove({_id: currentError._id});
    }

    //Error Message Removal
    function cleanUp () {
      Meteor.setTimeout(function () {
        ErrorMessage.remove({_id: currentError._id});
      }, 4500);
    }
  }
});

/***************************************************************/
/* Events */
/***************************************************************/
Template.ErrorMessage.events({
  'click .close': function () {
    //Message fadeout upon x click
    $('.message').fadeOut();
    //Remove error message
    var oldError = ErrorMessage.find().fetch()[0]
    ErrorMessage.remove({_id: oldError._id});
  }
});