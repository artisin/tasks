/******************************************************************************/
/******************************************************************************/
// Reactive Table
/******************************************************************************/
/******************************************************************************/
//////////////////*********** TODO ***********//////////////////
/// 1. tfoot covers up last row
///
//////////////////*********** TODO ***********//////////////////

/***************************************************************/
/* Local Vars */
/***************************************************************/
var get = function(obj, field) {
  var keys = field.split('.');
  var value = obj;

  _.each(keys, function (key) {
	  if (_.isObject(value) && _.isFunction(value[key])) {
		  value = value[key]();
	  } else if (_.isObject(value) && !_.isUndefined(value[key])) {
		  value = value[key];
	  } else {
		  value = null;
	  }
  });

  return value;
};

var parseFilterString = function (filterString) {
	var startQuoteRegExp = /^[\'\"]/;
	var endQuoteRegExp = /[\'\"]$/;
	var filters = [];
	var words = filterString.split(' ');

	var inQuote = false;
	var quotedWord = '';
	_.each(words, function (word) {
		if (inQuote) {
			if (endQuoteRegExp.test(word)) {
				filters.push(quotedWord + ' ' + word.slice(0, word.length - 1));
				inQuote = false;
				quotedWord = '';
			} else {
				quotedWord = quotedWord + ' ' + word;
			}
		} else if (startQuoteRegExp.test(word)) {
			if (endQuoteRegExp.test(word)) {
				filters.push(word.slice(1, word.length - 1));
			} else {
				inQuote = true;
				quotedWord = word.slice(1, word.length);
			}
		} else {
			filters.push(word);
		}
	});
	return filters;
};

var getFilterQuery = function (filter, fields) {
	var numberRegExp = /^\d+$/;
	var queryList = [];
	if (filter) {
		var filters = parseFilterString(filter);
		_.each(filters, function (filterWord) {
			var filterQueryList = [];
			_.each(fields, function (field) {
				var filterRegExp = new RegExp(filterWord, 'i');
				var query = {};
				query[field.key || field] = filterRegExp;
				filterQueryList.push(query);

				if (numberRegExp.test(filterWord)) {
					var numberQuery = {};
					numberQuery[field.key || field] = parseInt(filterWord, 10);
					filterQueryList.push(numberQuery);
				}
			});
			if (filterQueryList.length) {
				var filterQuery = {'$or': filterQueryList};
				queryList.push(filterQuery);
			}
		});
	}
	return queryList.length ? {'$and': queryList} : {};
};

var updateFilter = _.debounce(function (template, filterText) {
	template.data.filter.set(filterText);
	template.data.currentPage.set(0);
	template.data.tableFixer();
	template.data.uiComponents.body();
}, 200);


var getDefaultFalseSetting = function (key, templateData) {
	if (!_.isUndefined(templateData[key]) &&
		templateData[key]) {
		return true;
	}
	if (!_.isUndefined(templateData.settings) &&
		!_.isUndefined(templateData.settings[key]) &&
		templateData.settings[key]) {
		return true;
	}
	return false;
};

var getDefaultTrueSetting = function (key, templateData) {
	if (!_.isUndefined(templateData[key]) &&
		!templateData[key]) {
		return false;
	}
	if (!_.isUndefined(templateData.settings) &&
		!_.isUndefined(templateData.settings[key]) &&
		!templateData.settings[key]) {
		return false;
	}
	return true;
};

var getPageCount = function () {
	var rowsPerPage = this.rowsPerPage.get();
	var filterQuery = getFilterQuery(this.filter.get(), this.fields);
	var count = this.collection.find(filterQuery).count();
	return Math.ceil(count / rowsPerPage);
};

/***************************************************************/
/* Setup for template created */
/***************************************************************/
var setup = function () {
	this.data.settings = this.data.settings || {};
	var collection = this.data.collection || this.data.settings.collection || this.data;

	if (!(collection instanceof Mongo.Collection)) {
		if (_.isArray(collection)) {
			// collection is an array
			// create a new collection from the data
			var data = collection;
			collection = new Mongo.Collection(null);
			_.each(data, function (doc) {
				collection.insert(doc);
			});
		} else if (_.isFunction(collection.fetch)) {
			// collection is a cursor
			// create a new collection that will reactively update
			var cursor = collection;
			collection = new Mongo.Collection(null);

			// copy over transforms from collection-helper package
			collection._transform = cursor._transform;
			collection._name = cursor.collection._name;

			var addedCallback = function (doc) {
				collection.insert(doc);
			};
			var changedCallback = function (doc, oldDoc) {
				collection.update(oldDoc._id, doc);
			};
			var removedCallback = function (oldDoc) {
				collection.remove(oldDoc._id);
			};
			cursor.observe({added: addedCallback, changed: changedCallback, removed: removedCallback});
		} else {
			console.log("reactiveTable error: argument is not an instance of Mongo.Collection, a cursor, or an array");
			collection = new Mongo.Collection(null);
		}
	}
	this.data.collection = collection;

	var fields = this.data.fields || this.data.settings.fields || {};
	if (_.keys(fields).length < 1 ||
		(_.keys(fields).length === 1 &&
		 _.keys(fields)[0] === 'hash')) {
		fields = _.without(_.keys(collection.findOne() || {}), '_id');
	}

	var sortKey = 0;
	var sortDirection = 1;

	var normalizeField = function (field) {
		if (typeof field === 'string') {
			return {key: field, label: field};
		} else {
			return field;
		}
	};

	var parseField = function (field, i) {
		if (field.sort) {
			sortKey = i;
			if (field.sort === 'desc' || field.sort === 'descending'  || field.sort === -1) {
				sortDirection = -1;
			}
		}
		return normalizeField(field);
	};

	fields = _.map(fields, parseField);
	this.data.fields = fields;
	this.data.sortKey = new ReactiveVar(sortKey);
	this.data.sortDirection = new ReactiveVar(sortDirection);

	var visibleFields = [];
	_.each(fields, function (field, i) {
		if (!field.hidden || (_.isFunction(field.hidden) && !field.hidden())) {
		  visibleFields.push(i);
		}
	});
	this.data.visibleFields = new ReactiveVar(visibleFields);


	var rowClass = this.data.rowClass || this.data.settings.rowClass || function() {return ''};
	if (typeof rowClass === 'string') {
		var tmp = rowClass;
		rowClass = function(obj) { return tmp; };
	}
	this.data.rowClass = rowClass;

	this.data.class = this.data.class || this.data.settings.class || 'table table-striped table-hover';
	this.data.id = this.data.id || this.data.settings.id || _.uniqueId('reactive-table-');

	this.data.showNavigation = this.data.showNavigation || this.data.settings.showNavigation || 'always';
	this.data.showNavigationRowsPerPage = getDefaultTrueSetting('showNavigationRowsPerPage', this.data);
	this.data.rowsPerPage = new ReactiveVar(this.data.rowsPerPage || this.data.settings.rowsPerPage || 10);
	this.data.currentPage = new ReactiveVar(0);

	this.data.filter = new ReactiveVar(null);
	this.data.showFilter = getDefaultTrueSetting('showFilter', this.data);

	this.data.showColumnToggles = getDefaultFalseSetting('showColumnToggles', this.data);
	
	if (_.isUndefined(this.data.useFontAwesome)) {
		if (!_.isUndefined(this.data.settings.useFontAwesome)) {
			this.data.useFontAwesome = this.data.settings.useFontAwesome;
		} else if (!_.isUndefined(Package['fortawesome:fontawesome'])) {
			this.data.useFontAwesome = true;
		} else {
			this.data.useFontAwesome = false;
		}
	}

	//How many rows selected
	this.selectedUserCount = new ReactiveVar(0);

	//If table is scrollable
	this.data.isScrollable = new ReactiveVar(false);
	this.data.scrollPosition = new ReactiveVar(0);
	
	this.data.reactiveTableSetup = true;


  /*******************************/
  // Semantic Components
  /*******************************/
	var uiComponents = function () {
		var self = this;
		return{
	  	init: function () {
	  		this.nav();
	  		this.body();
	  		this.scrollBar();
	  	},
	  	nav: function () {
	  		var selfRef = this;
			  //Dropdown for the table filter 
			  $('.itemFilter').dropdown({
			  	action: 'nothing'
			  });
			  //Dropdown for row filter
			  $('.rowFilter').dropdown({
			  	action: 'select',
			  	//Changes how many items per row when selected
			  	onChange: function (value) {
			  		if (value !== undefined) {
			  			self.data.currentPage.set(0);
							self.data.rowsPerPage.set(Number(value));
							self.data.tableFixer();
							//Reinit body components
							setTimeout(function () {
								selfRef.body();
							}, 10);
			  		}
			  	}
			  });
	  	},
	  	scrollBar: function () {
	  		var tableID = "#"+self.data.id,
	  				container = '#reactiveTableScroller',
	  				thead = $(tableID).find("tr.mainThead"),
	  				tbody = $(tableID).find("tbody"),
						tfoot = $(tableID).find("tfoot"),
						tHeadPosition;

				$(container).mCustomScrollbar({
				  theme:"rounded-dark",
				  callbacks: {
						//When scroller is displayed
						onOverflowY: function () {
							self.data.isScrollable.set(true);
							tHeadPosition = self.data.tHeadPosition.get();
							//Fix posision
							$(thead).css({position: "fixed"});
							$(tfoot).css({position: "fixed"});
							//Fixer
							self.data.tableFixer();
							//Custom margin for
							$('.mCSB_container').css({marginRight: 16});
						},
						//When scroller is hidden
						onOverflowYNone: function () {
							self.data.isScrollable.set(false);
							//UnFix position
							$(thead).css({position: ""});
							$(tfoot).css({position: ""});
							// fuck.find('thead').css({position: 'fixed'})
							$('.mCSB_container').css({marginRight: 0});
						},
						whileScrolling: function () {

						if (this.mcs.top > -7) {
							TweenMax.to($(thead), 0.15, {top: tHeadPosition+'px'});
							self.data.scrollPosition.set(0);
						}else{
							TweenMax.to($(thead), 0.15, {top: (tHeadPosition-9)+'px'});
							self.data.scrollPosition.set(1);
						}
						},
				  }
				});
	  	},
	  	body:  function () {
	  		var selfRef = this;
	  		//Need to wait of ui compenets to load for various event calls
	  		 Meteor.setTimeout(function () {
		  		//Pagnation dropdown page selector
				  $('#recTbl_dropdownSearch').dropdown({
				  	onChange: function(value){
				  		if (value !== undefined) {
				  			self.data.currentPage.set(Number(value - 1));
				  			selfRef.body();
				  		}
				  	}
				  });
				  //Toggle checkbox 
				  $('.userCheckbox').checkbox();
				  $('.userCheckbox').checkbox('attach events', '.selectAll.button', 'check');
				  $('.userCheckbox').checkbox('attach events', '.deselectAll.button', 'uncheck'); 
	  		 }, 50);
	  	}
		};
	};

	this.data.uiComponents = uiComponents.call(this);


	/*******************************/
	// Table Fixer
	/*******************************/
	this.data.tHeadPosition = new ReactiveVar(null);
	this.data.cellSizes = new ReactiveVar();
	var tableFix = function (table, container, firstCellWidth) {
	  var $table = $(table),
	      $container = $(container),
	      containerPostition = ($(window).height()-$(container).outerHeight()),
	      tHeadPosition = containerPostition+11,
	      tableHead = $table.find("thead"),
	      tableFoot = $table.find("tfoot"),
  			tableBody = $table.find("tbody"),
  			tableRows = $(tableBody).find(".tableRow"),
  			firstRow = tableRows[1],
  			firstTextCell = $(firstRow).find("td")[1],
  			fontSize = $(firstTextCell).css('font-size'),
	      cellSizes = [],
	      cellsOuterWidth = [],
	      self = this;
	      firstCellWidth = firstCellWidth ? firstCellWidth : 82;

	  this.tHeadPosition.set(tHeadPosition);
	  return {
	    init: function () {
	     	return this.findCellSizes();
	    },
	    findCellSizes: function () {
	    	//Clear cell sizes
	    	cellSizes.length = 0;

	      var getAvgCellSize = function () {
	        var numberOfVisableFields = self.visibleFields.get().length+1,
	            tableWidth = $table.outerWidth(),
	            avgCellWidth = (Math.floor((tableWidth-firstCellWidth)/(numberOfVisableFields-1)));
	        return avgCellWidth;
	      };

	      var getTextWidth = function (text) {
	          var font = fontSize+'px '+'arial';
	          // re-use canvas object for better performance
	          var canvas = getTextWidth.canvas || (getTextWidth.canvas = document.createElement("canvas"));
	          var context = canvas.getContext("2d");
	          context.font = font;
	          var metrics = context.measureText(text);
	          return metrics.width;
	      };

		  	//Calcs all text widths of cells, if width is bigger than average the 
		  	//cells will be calculated by themselfs, otherwise they will have a 
		  	//uniform size
		    var checkCellSizes = function () {
		      var avgCellWidth = getAvgCellSize(),
		          nonAvgCells = false,
		          textWidths = [];

		      //This is sorta janky, but I give up on finding a better solution
		      $.each($(firstRow).find("td"), function (index, value) {
		        if (index !== 0) {
		        var textValue = $(value).text(),
		            textWidth = Math.floor(getTextWidth(textValue));
		          //Finds text larger then cell width
		          if (textWidth+100 > avgCellWidth) {
		          	textWidths.push(textWidth);
		            textWidths = _.map(textWidths, function (value) {
		            	value += 200;
		            	return value; 
		            });
		          }
		          //Controlls return
		          if (!_.isEmpty(textWidths)) {
		          	if (_.max(textWidths) > avgCellWidth) {
		          		nonAvgCells = true;
		          	}
		          }
		        }
		      });
		      return nonAvgCells;
		    };

		    //If no nonAvgCells all cells will then be set to equal widths
		    if (!checkCellSizes()) {
		    	$.each($(firstRow).find("td"), function (index) {
		    		var size;
		    		if (index === 0) {
		    			size = firstCellWidth;
		    		}else{
		    			size = getAvgCellSize();
		    		}
		    		cellSizes.push(size);
		    	});
		    }

		    self.cellSizes.set(cellSizes);
	    	this.applyCellSize();
	    },
	    //Applys all the sizing to said cells
	    applyCellSize: function () {
	    	//Reset widths
	    	cellsOuterWidth.length = 0;
	    	
	    	//Creates and set the cellsOuterWidth array for thead and tfoot sizing
	    	if (cellSizes.length !== 0) {
	    		//If cells are to be equal
	    		//Sets tbody cell widths
	    		$.each($(tableRows).find("td"), function (index, value) {
	    			$(value).css({width: cellSizes[index]});
	    		});
	    		//Finds tbody cells outerwidth for thead/tfoot
	    		$.each($(tableRows), function (index, value) {
						if (index === 0) {
							$.each($(value).find("td"), function (i, v) {
								cellsOuterWidth.push($(v).outerWidth());
							});
						}
	    		});
	    	}else{
	    			//If cells are to auto calc
		    		$.each($(tableRows).find("td"), function (index, value) {
		    			$(value).css({width: ''});
		    		});
		    		//Finds tbody cells outerwidth for thead/tfoot
		    		$.each($(tableRows), function (index, value) {
						if (index === 0) {
							$.each($(value).find("td"), function (i, v) {
								cellsOuterWidth.push($(v).outerWidth());
							});
						}
	    		});
	    	}

	    	//Thead sizing
	    	var theadCellHeight = []; 
		    $.each($(tableHead).find("tr"), function (index, value) {
		    	$.each($(value).find("th"), function (i, v) {
		    		theadCellHeight.push($(v).outerHeight());
		    		//pushed height for tbodyFixture
						$(v).css({width: cellsOuterWidth[i]});
		    	});
		    });

		    //Fake tbody sizing for when table is scrollabe
		    if (self.isScrollable.get()) {
		    	var tbodyFixtureHeight =  _.last(theadCellHeight);
			    $.each($(tableBody).find(".tbodyFixture"), function (index, value) {
			    	$.each($(value).find("td"), function (i, v) {
							$(v).css({height: tbodyFixtureHeight});
			    	});
			    });
		    }

		    //Tfoot sizing
		    $.each($(tableFoot).find("th"), function (index, value) {
		    	if (index === 0) {
		    		$(value).css({width: firstCellWidth});
		    	}else{
		    		$(value).css({width: ($(firstRow).outerWidth()-firstCellWidth)});
		    	}
		    });


		    //IF scrollable set elm positions
		    if (self.isScrollable.get()) {
		    	this.setElmPositions();
		    }

	    },
	    //Sets thead and tfoot position
	    setElmPositions: function () {
	    	//nav
    		$('.reactiveTableMenu').css({top: containerPostition-31}); 
 
	    	//thead
	    	//if thead is at scroll start set otherwise mcs scoller plugin 
	    	//takes care fo positioning
	    	if (!self.scrollPosition.get()) {
	    		$(tableHead).find("tr").css({top: tHeadPosition});
	    	}
	    	//tfoot
	    	$(tableFoot).css({top: containerPostition+$container.outerHeight()-122});
	    },
	    //Resizes cells on filter sort
	    cellReSize: function () {
	    	var cellsWidths = self.cellSizes.get(),
	    			reSizeOuterWidth = [];

	    	console.log(cellsWidths)
	    	//Calc cell widths
	    	if (cellsWidths.length !== 0) {
	    		//If cells are to be equal
	    		//Sets tbody cell widths
	    		$.each($(tableRows).find("td"), function (index, value) {
	    			$(value).css({width: cellsWidths[index]});
	    		});
	    		//Finds tbody cells outerwidth for thead/tfoot
	    		$.each($(tableBody).find("tr"), function (index, value) {
						if (index === 0) {
							$.each($(value).find("td"), function (i, v) {
								reSizeOuterWidth.push($(v).outerWidth());
							});
						}
	    		});
	    	}else{
	    			//If cells are to auto calc
		    		$.each($(tableBody).find("td"), function (index, value) {
		    			$(value).css({width: ''});
		    		});
		    		//Finds tbody cells outerwidth for thead/tfoot
		    		$.each($(tableBody).find("tr"), function (index, value) {
						if (index === 0) {
							$.each($(value).find("td"), function (i, v) {
								reSizeOuterWidth.push($(v).outerWidth());
							});
						}
	    		});
	    	}

	    	//Thead height calc for fake fixture
	    	//There are some edge cases in which this done correctly change the
	    	//size but fuck it.
	    	var theadCellHeight = []; 
		    $.each($(tableHead).find("tr"), function (index, value) {
		    	$.each($(value).find("th"), function (i, v) {
		    		theadCellHeight.push($(v).outerHeight());
		    	});
		    });

		    //Fake tbody height sizign
	    	var tbodyFixtureHeight =  _.last(theadCellHeight);
		    $.each($(tableBody).find(".tbodyFixture"), function (index, value) {
		    	$.each($(value).find("td"), function (i, v) {
						$(v).css({height: tbodyFixtureHeight});
		    	});
		    });

	    	//Thead sizing
		    $.each($(tableHead).find("tr"), function (index, value) {
		    	$.each($(value).find("th"), function (i, v) {
						$(v).css({width: reSizeOuterWidth[i]});
		    	});
		    });

		    //Tbody resizing for edge cases
    		$.each($(tableBody).find("tr"), function (index, value) {
					$.each($(value).find("td"), function (i, v) {
						$(v).css({width: reSizeOuterWidth[i]});
					});
	  		}); 

	    }
	  };
	};

	//Positions/Sizes table and accessories
	this.data.tableFixer = function () {
		var table = tableFix.apply(this, ['table', '.reactiveTable_Container', 82]);
		return Meteor.setTimeout(function () {
			table.init();
		}, 0);
	};

	//Recals widths for cells and applyes
	this.data.cellReSize = function () {
		var table = tableFix.apply(this, ['table', '.reactiveTable_Container', 82]);
		return Meteor.setTimeout(function () {
			table.cellReSize();
		}, 0);
	};

	//Invoke fixer on setup
	// this.data.tableFixer();
	
	//ReInit pag selector
	Meteor.setTimeout(function () {
		if ($('#recTbl_dropdownSearch').length) {
			$('#recTbl_dropdownSearch').dropdown();
		}
	}, 50);
};

/***************************************************************/
/* Template Created */
/***************************************************************/
Template.reactiveTable.created = setup;

/***************************************************************/
/* Template Rendered */
/***************************************************************/
Template.reactiveTable.rendered = function () {
	this.data.text = new ReactiveVar($('.reactiveTable_Container'));
  App.animateTmpl.container('#reactiveTable_Container');

	//*******************************/
	// Init UI Components
	/*******************************/
	this.data.uiComponents.init();

  /*******************************/
  // Positioning/sizing
  /*******************************/
  var self = this;
	function positionElements () {
		var windowHeight = Session.get('windowHeight'),
	      tableContainerWidth = $('.reactiveTable_Container').outerWidth();
	  //Table posisiton
	  $('.reactiveTable_Container').css({height: windowHeight - 365});
	  //Table Nav size
	  $('.reactiveTableMenu').css({width: tableContainerWidth});
	  //Table pagnation position and <size></size>
	  $('.recTblNav_Container').css({ width: tableContainerWidth, top: windowHeight - 80});

	  //Table cells
	  self.data.tableFixer();
	}
	positionElements();

  $( window ).resize(function() {
  	positionElements();
  });

  /*******************************/
  // Setup
  /*******************************/
  this.data.tableFixer();
	if (!this.data.reactiveTableSetup) {
		setup.call(this);
	}
};


/***************************************************************/
/* Helpers */
/***************************************************************/
Template.reactiveTable.helpers({
	'setup' : function () {
		if (!this.reactiveTableSetup) {
			setup.call(Template.instance());
		}
	},

	'getField': function (object) {
		var fn = this.fn || function (value) { return value; };
		var key = this.key || this;
		var value = get(object, key);
		return fn(value, object);
	},

	'getFieldIndex': function () {
		return _.indexOf(Template.parentData(1).fields, this);
	},

	'getKey': function () {
		return this.key || this;
	},

	'getLabel': function () {
		return _.isString(this.label) ? this.label : this.label();
	},

	'isSortKey': function () {
		var parentData = Template.parentData(1);
		return parentData.sortKey.get() == _.indexOf(parentData.fields, this);
	},

	'isSortable': function () {
		return (this.sortable == undefined) ? true : this.sortable;
	},

	'isVisible': function () {
		var topLevelData;
		if (Template.parentData(2) && Template.parentData(2).reactiveTableSetup) {
		  topLevelData = Template.parentData(2);
		} else {
		  topLevelData = Template.parentData(1);
		}
		var visibleFields = topLevelData.visibleFields;
		var fields = topLevelData.fields;
		Session.set('tableColumnCount', visibleFields.get().length);
		return _.include(visibleFields.get(), _.indexOf(fields, this));
	},

	//Controlls collum count for formating of tfoot th's
 	'columnCount': function  () {
		return Session.get('tableColumnCount');
	},
	'isAscending' : function () {
		var sortDirection = Template.parentData(1).sortDirection.get();
		return (sortDirection === 1);
	},

	'sortedRows': function () {
		var sortDirection = this.sortDirection.get();
		var sortKeyIndex = this.sortKey.get();
		var sortKeyField = this.fields[sortKeyIndex] || {};

		var limit = this.rowsPerPage.get();
		var currentPage = this.currentPage.get();
		var skip = currentPage * limit;
		var filterQuery = getFilterQuery(this.filter.get(), this.fields);

		if (sortKeyField.fn && !sortKeyField.sortByValue) {
			var data = this.collection.find(filterQuery).fetch();
			var sorted =_.sortBy(data, function (object) {
				return sortKeyField.fn(object[sortKeyField.key], object);
			});
			if (sortDirection === -1) {
				sorted = sorted.reverse();
			}
			return sorted.slice(skip, skip + limit);
		} else {
			var sortKey = sortKeyField.key || sortKeyField;
			var sortQuery = {};
			sortQuery[sortKey] = sortDirection;

			return this.collection.find(filterQuery, {
				sort: sortQuery,
				skip: skip,
				limit: limit
			});
		}
	},

	//Controlls if the duplicate thead is shown
	'isScrollable': function () {
		return this.isScrollable.get();
	},

	//Search filter input
	'filter' : function () {
		return this.filter.get() || '';
	},

	//Makes sure user has atleast one column of data displayed
	'isFilterColDisabled': function () {
		var visibleFields = Template.instance().data.visibleFields.get(),
				numberOfVisableFields = visibleFields.length;
		if (numberOfVisableFields <= 1) {
			return "Disabled";
		}
	},

	/*******************************/
	// Navigation Helpers
	/*******************************/
	'showNavigation' : function () {
		if (this.showNavigation === 'always') return true;
		if (this.showNavigation === 'never') return false;
		return getPageCount.call(this) > 1;
	},
	'getPageCount' : getPageCount,
	'getRowsPerPage' : function () {
		return this.rowsPerPage.get();
	},
	'getCurrentPage' : function () {
		return 1 + this.currentPage.get();
	},
	//Determins what how many pages
	'recTbl_Size': function (value) {
		return getPageCount.call(this) > value;
	},
	//Determins what page is active
	'page_active': function (value) {
		var currentPage = 1 + this.currentPage.get(),
		    rowsPerPage = this.rowsPerPage.get(),
				filterQuery = getFilterQuery(this.filter.get(), this.fields),
				count = this.collection.find(filterQuery).count(),
				lastPage = currentPage === Math.ceil(count / rowsPerPage);
		if (currentPage === value) {
			return 'active';
		}
		//Last Page Active
		if (lastPage && value === 'last') {
			return 'active';
		}
		//Selector Active
		if (value === 'selector' && !lastPage && currentPage !== 1 && currentPage !== 2 && currentPage !== 3) {
			return 'active';
		}
	},
	//Creates list of pages for pagnation
	'recTbl_pageList': function () {
		var totalPages = getPageCount.call(this) + 1,
				pageList = [];
		if (totalPages > 1) {
			for (var i = 1; i < totalPages; i++) {
				pageList.push(i);
			}
		}
		return pageList;
	},
	//Helper for page list - displays number
	'recTbl_pageNumber': function () {
		return this;
	},
	'page_buttonState': function () {
		var disabled = {},
				currentPage = 1 + this.currentPage.get(),
				rowsPerPage = this.rowsPerPage.get(),
				filterQuery = getFilterQuery(this.filter.get(), this.fields),
				count = this.collection.find(filterQuery).count(),
				lastPage = currentPage === Math.ceil(count / rowsPerPage);
		currentPage === 1 ? disabled.prev = 'disabled' : disabled.prev = '';
		lastPage ? disabled.next = 'disabled' : disabled.next = '';
		return disabled;
	},
	//Direct Message Button State
	buttonState: function () {
		return Template.instance().selectedUserCount.get() === 0 ? 'disabled' : '';
	},
});

/***************************************************************/
/* Events */
/***************************************************************/
Template.reactiveTable.events({
	/*******************************/
	// Nav/Header EVents
	/*******************************/
	//Sort Items
	'click .reactive-table .sortable': function (event) {
		event.preventDefault();
		var tpI = Template.instance(),
				target = $(event.target).is('i') ? $(event.target).parent() : $(event.target),
				sortIndex = parseInt(target.attr('index'), 10),
				currentSortIndex = tpI.data.sortKey.get();

		if (currentSortIndex === sortIndex) {
			var sortDirection = -1 * tpI.data.sortDirection.get();
			tpI.data.sortDirection.set(sortDirection);
			tpI.data.cellReSize();
		} else {
			tpI.data.sortKey.set(sortIndex);
			tpI.data.cellReSize();
		}
		//UI body components reload
		tpI.data.uiComponents.body();
	},

	//Change display fields
	'change .reactive-table-columns-dropdown input': function (event) {
		var tpI = Template.instance(),
				target = $(event.target),
				index = parseInt(target.attr('index'), 10),
				currentVisibleFields = tpI.data.visibleFields.get();
		
		if(_.include(currentVisibleFields, index)){
			tpI.data.visibleFields.set(_.without(currentVisibleFields, index));
		}else{
			tpI.data.visibleFields.set(currentVisibleFields.concat(index));
		}
		//Recalcs cell widths
		tpI.data.tableFixer();
	},

	//Search input
	'keyup .reactive-table-filter .reactive-table-input, input .reactive-table-filter .reactive-table-input': function (event) {
		var tpI = Template.instance(),
				filterText = $(event.target).val();
		updateFilter(tpI, filterText);
	},

	/*******************************/
	// Pagnation Events
	// Inputs defined in uiCompnents on render
	/*******************************/
	//Prvious pagnation button
	'click .recTblPag-prev': function (e) {
	  e.preventDefault();
		var tpI = Template.instance(),
				currentPage = tpI.data.currentPage.get();
		if (currentPage > 0) {
			//Set Page
			tpI.data.currentPage.set(currentPage - 1);
			//Reconfig table
			tpI.data.tableFixer();
			//Relaod body ui Compents
			tpI.data.uiComponents.body();
		}
	},
	//Next pagnation button
	'click .recTblPag-next': function (e) {
	  e.preventDefault();
		var tpI = Template.instance(),
				currentPage = tpI.data.currentPage.get(),
				pageCount = getPageCount.call(this)-1;
		if (currentPage < pageCount) {
			//Set Page
			tpI.data.currentPage.set(currentPage + 1);
			//Reconfig table
			tpI.data.tableFixer();
			//Relaod body ui Compents
			tpI.data.uiComponents.body();
		}
	},
	//Page Buttons (1,2,3, and last)
	'click .recTblPag-item': function (e, tmpl) {
	  e.preventDefault();
	  var tpI = Template.instance(),
	  		page = Number($(e.currentTarget).context.innerText);
	  //Set Page
	  tpI.data.currentPage.set(page-1);
		//Reconfig table
		tpI.data.tableFixer();
		//Relaod body ui Compents
		tpI.data.uiComponents.body();
	},
	/*******************************/
	/* Select/Highlight row */
	/*******************************/
	//Input switches
	'click .userCheckbox': function (e) {
	  var targetRow = $(e.target).closest("tr");
	  if (!targetRow.hasClass("tableHighlighted")) {
			$(e.target).closest("tr").addClass("tableHighlighted");
	  }else{
			$(e.target).closest("tr").removeClass("tableHighlighted");
	  }
	  //For button state controll - helper 'buttonState'
	  Template.instance().selectedUserCount.set(
	  	$('.tableBody').find('.tableHighlighted').length
	  );
	},
	//Select all button
	'click .selectAll': function () {
	  $('.tableBody').find('tr').addClass("tableHighlighted");
	  //For button state controll - helper 'buttonState'
	  Template.instance().selectedUserCount.set(
	  	$('.tableBody').find('.tableHighlighted').length
	  );
	},
	//Deselect all button
	'click .deselectAll': function () {
	  $('.tableBody').find('tr').removeClass("tableHighlighted");
	  //For button state controll - helper 'buttonState'
	  Template.instance().selectedUserCount.set(
	  	$('.tableBody').find('.tableHighlighted').length
	  );
	},

	/*******************************/
	/* Direct Message */
	/*******************************/
	'click .tableDirectMessage': function (evt, tmpl) {
	  var selected = tmpl.$('.tableHighlighted').children('.email\\.address'),
		  self = this,
		  recipiants = [];
	  //Finds selected users
	  recipiants = _.map(selected, function (value) {
		var email = $(value).text(),
			options = {fields: {_userId: 1, firstName: 1, lastName: 1}},
			id = self.collection.findOne({'email.address': email}, options);
			return id;
	  });
	  Session.set('directMessageRecipiants', recipiants);
	  //Modal Config
	  Modal.overlay({
		template: 'directMessageModal',
		dataContext:{
		  selected:  Session.get('directMessageRecipiants'),
		  action:{
			confirmModal: function() {},
			cancelModal: function() {}
		  }
		},
	  });
	}
});