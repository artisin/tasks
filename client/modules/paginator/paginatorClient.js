/******************************************************************************/
/******************************************************************************/
/*  Controller: Panginator 
/*  Template: /client/views/templateLocation
/******************************************************************************/
/******************************************************************************/

/***************************************************************/
/* Template States */
/***************************************************************/
/*******************************/
// Created
/*******************************/
Template.Paginator_UI.created = function () {
  this.pageSelection = new ReactiveVar(null);
  this.pageLocation = new ReactiveVar({});
  this.pagReady = new ReactiveVar(false);
};

/*******************************/
// Rendered
/*******************************/
Template.Paginator_UI.rendered = function () {
  var self = this;
  //Needs timeout otherwise it dose function on first click
  Meteor.setTimeout(function () {
    $('#pag_dropdownSearch')
    .dropdown({
      action: function (value) {
        $('#pag_dropdownSearch').dropdown('set selected', value);
        $('#pag_dropdownSearch').dropdown('toggle');
        $('#pag_dropdownSearch').dropdown('remove active');
        // self.data.goToPage(value - 1);
      }
    }); 
  }, 200);



};

/***************************************************************/
/* Template Helpers */
/***************************************************************/
//
Template.Paginator_UI.helpers({
  //Shows UI if needed
  pag_showUI: function() {
    //Wait for any for collection to laod
    if (this.collection !== undefined) {
      var showUI = this.totalPages() > 1;
      //Gloabl session helper
      Session.set('currentPage', this.currentPage());
      
      //Reload the dropdown evertime the UI is shown
      if (showUI) {
        Meteor.setTimeout(function () {
          $('#pag_dropdownSearch').dropdown('is visible');
        }, 200);
      }
      return showUI;
    }else{
      return false;
    }
  },
  //Determins if UI should be limited
  pag_Size: function (value) {
    //Wait for collection to load
    if (this.collection !== undefined) {
      return this.totalPages() > value;
    }
  },
  //Current page displate
  pag_currentPage: function () {
    //Wait for collection to load
    if (this.collection !== undefined) {
      console.log('current')
      var currentPage = this.currentPage()+1;
      //Sets drop down value to reflect current page
      $('#pag_dropdownSearch').dropdown('set selected', currentPage);
      return currentPage;
    }
  },
  //Determins what the last page
  pag_lastPage: function () {
    //Wait for collection to load
    if (this.collection !== undefined) {
      return this.totalPages();
    }
  },
  //Creates array of all the pages for selector
  pag_pageList: function () {
    //Wait for collection to load
    if (this.collection !== undefined) {
      var totalPages = this.totalPages(),
          pageList = [];
      //Wait for data
      if (totalPages > 1) {
        for (var i = totalPages; i >= 1; i--) {
          pageList.push(i);
        }
      }
      return pageList.reverse();
    }
  },
  //Displays the page number for the selector
  pag_pageNumber: function () {
    return this;
  },
  pag_disable: function () {
    //Wait for collection to load
    if (this.collection !== undefined) {
      var disabled = {},
          totalPages = this.totalPages()-1,
          currentPage = this.currentPage();
      currentPage === 0 ? disabled.prev = 'disabled' : disabled.prev = '';
      currentPage === totalPages ? disabled.next = 'disabled' : disabled.next = '';
      return disabled;
    }
  },
  //Determins what pag tab is active and appends class
  pag_active: function (value) {
    //Wait for colleciton to load
    if (this.collection !== undefined) {
      var currentPage = this.currentPage()+1,
          lastPage = this.totalPages();
      if (currentPage === value) {
        return 'active';
      }
      if (currentPage === lastPage && value === 'last') {
        return 'active';
      }
      //prbly could make a better iffy
      if (currentPage !== lastPage && value === 'selector' && currentPage !== 1 && currentPage !== 2 && currentPage !== 3) {
        return 'active';
      }
    }
  }
});


/***************************************************************/
/* Template Events */
/***************************************************************/
Template.Paginator_UI.events({
  //Previous Page
  'click .pag-prev': function(e) {
    e.preventDefault();
    return this.goToPage(this.currentPage() - 1);
  },
  //Next Page
  'click .pag-next': function(e) {
    e.preventDefault();
    return this.goToPage(this.currentPage() + 1);
  },
  //Dedicated Page Button
  'click .pag-item': function (e) {
    e.preventDefault();
    var page = $(e.target).text().trim(),
        pageNumer = parseInt(page);
    return this.goToPage(pageNumer - 1);
  },
  //Page Selection
  'click .pag-pageSelection': function (e, tmpl) {
    e.preventDefault();
    return tmpl.data.goToPage(this - 1);
  },
});
