/******************************************************************************/
/******************************************************************************/
/*  Controller: none 
/*  Template: /client/views/templateLocation
/******************************************************************************/
/******************************************************************************/

/***************************************************************/
/* Template States */
/***************************************************************/
/*******************************/
// Created
/*******************************/
Template.tagRemover.created = function () {
  var tagRemover = function (tagId) {
      var selectedMessages = Session.get('selectedMessages'),
          userId = Meteor.userId(),
          tagSelector = {},
          tagKey = 'tags.'+userId,
          tagObj;
      tagSelector[tagKey] = 1;

      /**
       * Finds message cursor for the selected messages
       * @return {cursor} - with the _id and tag obj
       */
      var findSelected = function () {
        return Messages.find({
          _id: {$in: selectedMessages}}, 
          {fields: tagSelector}).fetch();
      };
      /**
       * Maps out the messages that do not have said tag to advoid duplication
       * @return {[_id]}
       */
      var filteredSelection = function () {
        return _.map(findSelected(), function (obj) {
          var exsitingTags = _.flatten(_.values(obj.tags));
          if (exsitingTags.length !== 0) {
            if (_.contains(exsitingTags, tagId)) {
              return obj._id;
            }
          }
        });
      };
      /**
       * Cleans array and creats obj for call
       * @return {obj} 
       */
      var cleanAndCreate = function () {
        return tagObj = {
          tag_Id: tagId,
          doc_Id: _.compact(filteredSelection())
        };
      };
      /**
       * Clean/defualts session, tagAdder, and selected
       */
      var cleanUp = function () {
          var allCheckBoxs = $('.messageList').find('.toggleMessage');
          //Due to edge cases needs timeout otherwise it will sometimes
          //not uncheck all selected
          Meteor.setTimeout(function () {
            //Unchecks all checkboxs
            _.each(allCheckBoxs, function (checkbox) {
              $(checkbox).checkbox('uncheck');
            });
          }, 10);
          //Reset Session
          Session.set('selectedMessages', []);

        //Defualt
        $('.tagRemover').dropdown('hide');
        Session.set('tagRemover', {active: false});
      };
      /**
       * Method call to remove tags
       * @param  {obj} tagObj with tagId to be removed and the docIds to remove it from
       */
      var removeTag = function (tagObj) {
        return Meteor.call('removeMessageTag', tagObj, function (error) {
          if (error) {
            App.errorMessage(error.reason);
          }else{
            cleanUp();
          }
        });
      };

    removeTag(cleanAndCreate());
  };
  this.tagRemover = tagRemover;

};

/*******************************/
// Rendered
/*******************************/
Template.tagRemover.rendered = function () {};

/*******************************/
// Destroyed
/*******************************/
Template.tagRemover.destroyed = function () {};


/***************************************************************/
/* Template Helpers */
/***************************************************************/
Template.tagRemover.helpers({
  isTagRemoverActive: function () {
    return Session.get('tagRemover').active;
  },
  removableTags: function () {
    var selectedMessages = Session.get('selectedMessages'),
        tagSelector = {},
        tagKey = 'tags.'+Meteor.userId();
    tagSelector[tagKey] = 1;
    /**
     * Finds message cursor for the selected messages
     * @return {cursor} - with the _id and tag obj
     */
    var findSelected = function () {
      return Messages.find({
        _id: {$in: selectedMessages}}, 
        {fields: tagSelector}).fetch();
    };
    /**
     * Finds tags on selected messages
     * @return {[tadId]}
     */
    var findTags = function () {
      return _.map(findSelected(), function (obj) {
        var exsitingTags = _.flatten(_.values(obj.tags));
        if (exsitingTags.length !== 0) {
          return exsitingTags;
        }
      });
    };
    /**
     * Grabs users tag collection
     * @return {obj}
     */
    var generateTagCollection = function () {
      var userId = Meteor.userId();
      return Tags.findOne({user_Id: userId}, {fields: {tags: 1}});
    };
    /**
     * Generates a tag obj for each tag for blaze helpers
     * @return {obj}
     */
    var generateTagList = function () {
      var collection = generateTagCollection(),
          selectedTags = cleanArray(findTags());
      return _.map(collection.tags, function (tagObj) {
        if (_.contains(selectedTags, tagObj.tag_Id)) {
          return {
            tagName: tagObj.tagName,
            tagColor: tagObj.tagColor,
            tagId: tagObj.tag_Id
          };
        }
      });
    };
    /**
     * Cleans an array collection
     * @param  {array} 
     * @return {array} - cleaned
     */
    function cleanArray (array) {
      array = _.flatten(array);
      array = _.compact(array);
      array = _.uniq(array);
      return array;
    }

    return cleanArray(generateTagList());

  }
});


/***************************************************************/
/* Template Events */
/***************************************************************/
Template.tagRemover.events({
  'click .tagRemover': function (e) {
    e.preventDefault();
    Session.set('tagRemover', {active: true});
  },
  'click .removeSelectedTag': function (e) {
    e.preventDefault();
    Template.instance().tagRemover(this.tagId);
  },
});