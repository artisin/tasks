/******************************************************************************/
/******************************************************************************/
/*  Controller: none 
/*  Template: /client/views/templateLocation
/******************************************************************************/
/******************************************************************************/

//////////////////*********** TODO ***********//////////////////
/// 1. Clean up code, and func
///
//////////////////*********** TODO ***********//////////////////

/***************************************************************/
/* Template States */
/***************************************************************/
/*******************************/
// Created
/*******************************/
Template.tagAdder.created = function () {
  var self = this;

  /**
   * Addes a new tag into user tag collection
   * @param {String} newTagString - The value of the new tag string
   * @param {obj} tmpl - The current template insctance
   */
  var addNewTag = function (newTagString, docId) {
    if (newTagString.length <= 2) {
      App.errorMessage('Sorry, tags must be 3 characters or longer.');
    }else{
      newTagString = _.capitalize(newTagString);
      var tagObj = {
        tagName: newTagString,
      };
      Meteor.call('addNewTag', tagObj, function (error, res) {
        if (error) {
          App.errorMessage(error.reason);
        }else{
          //Success Message
          App.successMessage(newTagString+' tag has been added to your collection');
          //Call to add tag now that it exsits
          self.addExsitingTag(res.newTagId, res.newTagObj.tagName, docId);
        }
      }); 
    }
  };
  this.addNewTag = addNewTag;


  /**
   * Adds a exsiting tag to message
   * @param {_id} tagId  of tag to be added
   * @param {string} tagName - name of tag for duplicate error
   * @param {obj} tmpl referance
   */
  var addExsitingTag = function (tagId, tagName, docId) {
    var isGroupTag = this.isGroupTaggin.get(),
        userId = Meteor.userId(),
        selfRef = this,
        tagObj;

    //Determins path
    if (isGroupTag) {
      groupTagAdder();
    }else{
      indvTagAdder();
    }

    /**
     * Adds tag to indv message
     * @return {[type]} [description]
     */
    function indvTagAdder () {
      var tags = selfRef.data.tags,
          currentTags = tags[userId];
      tagObj = {
        tag_Id: tagId,
        doc_Id: [docId],
      };
      //Checks to see if user aleady has tag on messeage
      var checkForDuplicate = function () {
        return _.each(currentTags, function (indvTagObj) {
          if (indvTagObj === tagId) {
            App.errorMessage('You aleady have added the '+tagName+' tag!');
          }else{
            //Call to Method
            return addTag(tagObj);
          }
        });
      };
      //If there is a tag check for duplicates
      if (currentTags.length !== 0) {
        checkForDuplicate();
      }else{
        //Call to method
        return addTag(tagObj);
      }
    }

    /**
     * Adds tags to a group of selected messages
     * @return {tagObj}
     */
    function groupTagAdder () {
      var selectedMessages = Session.get('selectedMessages'),
          tagSelector = {},
          tagKey = 'tags.'+userId;
      tagSelector[tagKey] = 1;

      /**
       * Finds message cursor for the selected messages
       * @return {cursor} - with the _id and tag obj
       */
      var findSelected = function () {
        return Messages.find({
          _id: {$in: selectedMessages}}, 
          {fields: tagSelector}).fetch();
      };
      /**
       * Maps out the messages that do not have said tag to advoid duplication
       * @return {[_id]}
       */
      var filteredSelection = function () {
        return _.map(findSelected(), function (obj) {
          var exsitingTags = _.flatten(_.values(obj.tags));
          if (exsitingTags.length !== 0) {
            if (!_.contains(exsitingTags, tagId)) {
              return obj._id;
            }
          }else{
            return obj._id;
          }
        });
      };
      /**
       * Cleans array and creats obj for call
       * @return {obj} 
       */
      var cleanAndCreate = function () {
        return tagObj = {
          tag_Id: tagId,
          doc_Id: _.compact(filteredSelection())
        };
      };

      //Assing tarObj
      tagObj = cleanAndCreate();
      //Only run if tags are to be added
      if (tagObj.length !== 0) {
        //Method call
        return addTag(tagObj);
      }
    }


    /**
     * Makes method call to add tag
     * @param {obj} tagObj - tagId to be added and docIds to add it on
     */
    function addTag (tagObj) {
      return Meteor.call('addMessageTag', tagObj, function (error) {
        if (error) {
          App.errorMessage(error.reason);
        }else{
          cleanUp();
        }
      });
    }

    /**
     * Clean/defualts session, tagAdder, and selected
     */
    function cleanUp () {
      if (isGroupTag) {
        var allCheckBoxs = $('.messageList').find('.toggleMessage');
        //Due to edge cases needs timeout otherwise it will sometimes
        //not uncheck all selected
        Meteor.setTimeout(function () {
          //Unchecks all checkboxs
          _.each(allCheckBoxs, function (checkbox) {
            $(checkbox).checkbox('uncheck');
          });
        }, 10);
        //Reset Session
        Session.set('selectedMessages', []);
      }

      //Defualt
      $('.tagAdder').dropdown('hide');
      Session.set('tagAdder', {active: false, _id: null});
    }

  };
  this.addExsitingTag = addExsitingTag;


  /*******************************/
  // Vars
  /*******************************/
  this.isTagsActive = new ReactiveVar(false);
  //If user is searching for a tag
  this.isTagSearchActive = new ReactiveVar(false);
  //Search Results
  this.tagSearchResults = new ReactiveVar({});
  //Tag search Value 
  this.tagSearchValue = ReactiveVar(null);
  //Group tagging
  this.isGroupTaggin = new ReactiveVar(false);
};

/*******************************/
// Rendered
/*******************************/
Template.tagAdder.rendered = function () {

};

/*******************************/
// Destroyed
/*******************************/
Template.tagAdder.destroyed = function () {

};


/***************************************************************/
/* Template Helpers */
/***************************************************************/
Template.tagAdder.helpers({
  /**
   * Determins if the tag search is open and sets/defualts the recVars
   * @return {Boolean}
   */
  isTagsActive: function () {
    var tagAdder = Session.get('tagAdder');
    if (tagAdder !== undefined && tagAdder.active) {
      var tpI = Template.instance();
      
      //Single doc tag add
      if (this._id === tagAdder._id) {
        tpI.tagSearchResults.set({});
        tpI.isTagSearchActive.set(false);
        tpI.isGroupTaggin.set(false);
        return true;
      }

      //Group doc tag Add 
      if (tagAdder.groupSelection) {
        tpI.tagSearchResults.set({});
        tpI.isTagSearchActive.set(false);
        tpI.isGroupTaggin.set(true);
        return true;
      }
    }else{
      return false;
    }
  },
  /**
   * Blaze template helper if user is searching, remove common tag html
   * @return {Boolean}
   */
  isTagSearchActive: function () {
    return Template.instance().isTagSearchActive.get();
  },
  /**
   * Blaze template helper - show results or add new tag
   * @return {Boolean}
   */
  noTagSearchResults: function () {
      if (Template.instance().tagSearchResults.get().count === 0) {
        return true;
      }else{
        return false;
      }
  },
  /**
   * Current value in search input
   * @return {string}
   */
  tagSearchValue: function () {
    return Template.instance().tagSearchValue.get();
  },
  /**
   * Tag search results
   * @return {[obj]} of tag name/color
   */
  tagSearchResults: function () {
    var resultsInstance = Template.instance().tagSearchResults.get();
    if (resultsInstance.count > 0) {
      return resultsInstance.results;
    }
  },
  /**
   * Top four most common tags filter
   * @return {[obj]} - common tags
   */
  commonTags: function () {
    var tagObj = Tags.findOne({user_Id: Meteor.userId()}),
        //Sort tags by frequency
        tags = _.sortBy(tagObj.tags, function (obj) {
          return -obj.frequency;
        }),
        //Filter out first five for comman tags
        firstFive = _.map(tags, function (value, key) {
          if (key < 4) {
            return value;
          }
        });
    return _.compact(firstFive);
  },
});


/***************************************************************/
/* Template Events */
/***************************************************************/
Template.tagAdder.events({
  //Tag Search
  'keyup .tagSearch': function (e, tmpl) {
    e.preventDefault();
    //////////////////*********** TODO ***********//////////////////
    /// 1. Possible filter out tags from search that are already on
    ///    the message. Think about ux.
    //////////////////*********** TODO ***********//////////////////
    var searchString = $(e.target).val().trim(),
        tpI = Template.instance(),
        filter = new RegExp(searchString, 'i');

    //Set Tag Search to active
    tpI.isTagSearchActive.set(true);

    /*******************************/
    // Query Search
    /*******************************/
    /**
     * Querys local miniMongo collection
     * @return {[Objects]} of the indvTag objs that match regex
     */
    var queryCollection = function () {
      var collection = Tags.find(
        {'tags.tagName': filter}, 
        {limit: 8}
      ).fetch();
      return collection.length !== 0 ? collection[0].tags : collection;
    };
    /**
     * Maps out collection fields
     * @return {[Object]} of tag objects with tagName and tagColor 
     */
    var generateTagList = function () {
      var tagList = queryCollection();
      return _.map(tagList, function (indvTagObj) {
        return {
          tagName: indvTagObj.tagName,
          tagColor: indvTagObj.tagColor,
          tagId: indvTagObj.tag_Id
        };
      });
    };
    /**
     * Filters the the list since you are unable to do so on miniMongo
     * @return {[Object]} of tagObjs that meet the regex filter
     */
    var filterdTagList = function () {
      var tagList = generateTagList();
      return _.filter(tagList, function (tag) {
        var tagName = tag.tagName;
        if (tagName.match(filter)) {
          return tag;
        }
      });
    };
    /**
     * Sorts filtered list by tagName
     * @return {[object]} filterd
     */
    var sortByName = function () {
      var tagList = filterdTagList();
      return _.sortBy(tagList, 'tagName');
    };
    /**
     * Sets the recVar for {{each}} result helper
     */
    (function setSearchResults () {
      var tagList = sortByName();
      tpI.tagSearchResults.set({
        count: tagList.length, 
        results: tagList
      });
    })();

    
    /*******************************/
    // Enter Key Event
    /*******************************/
    //If user hist eneter
    if (e.keyCode === 13) {
      var results = tpI.tagSearchResults.get();
      //Single tag
      if (results.count === 1) {
        addExsitingTag(results);
      }

      //No results found
      if (results.count === 0) {
        addNewTag(); 
      }
    }

    function addExsitingTag (results) {
      var tagObj = results.results[0],
          tagId = tagObj.tagId,
          docId = tpI.data._id,
          tagName =tagObj.tagName;
      tpI.addExsitingTag(tagId, tagName, docId);
    }

    function addNewTag () {
      var docId = tpI.data._id;
      console.log(tmpl)
      console.log(docId)
      tpI.addNewTag(searchString, docId);
    }

  },
  //Adds an exsisting tag
  'click .addTag': function (e, tmpl) {
    e.preventDefault();
    var tagId = this.tag_Id,
        docId = tmpl.data._id,
        tagName = this.tagName;
    Template.instance().addExsitingTag(tagId, tagName, docId);
  },
  //Adds a newley created tag
  'click .addNewTag': function (e, tmpl) {
    e.preventDefault();
    var tagName = $(e.currentTarget).children('span').text().trim(),
        docId = tmpl.data._id;
    Template.instance().addNewTag(tagName, docId);
  },
});