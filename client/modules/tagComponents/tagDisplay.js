/******************************************************************************/
/******************************************************************************/
/*  Controller: none 
/*  Template: /client/views/templateLocation
/******************************************************************************/
/******************************************************************************/
//////////////////*********** TODO ***********//////////////////
/// 1. Animation of tags 
///
//////////////////*********** TODO ***********//////////////////
/***************************************************************/
/* Template States */
/***************************************************************/
/*******************************/
// Created
/*******************************/
Template.tagDisplay.created = function () {
  this.data.currentTags = new ReactiveVar(null);
};

/*******************************/
// Rendered
/*******************************/
Template.tagDisplay.rendered = function () {
  var tagDisplayList = this.find('.tagDisplayList');
  //Verify that it exsists otherwise it will throw an error
  if (tagDisplayList !== null) {
    //Fade In/Out Animation
    this.find('.tagDisplayList')._uihooks = {
      insertElement: function(node, next) {
        $(node)
          .hide()
          .insertBefore(next)
          .fadeIn();
      },
      removeElement: function (node) {
        $(node).fadeOut(function() {
          $(this).remove();
        });
      }
    };
  }

};

/*******************************/
// Destroyed
/*******************************/
Template.tagDisplay.destroyed = function () {};


/***************************************************************/
/* Template Helpers */
/***************************************************************/
Template.tagDisplay.helpers({
  currentTags: function () {
    var userId = Meteor.userId(),
        tpI = Template.instance().data; 

    //Determins if there is tags attached to the message
    var tags = _.map(this.tags, function (tags, user) {
      var userId = Meteor.userId();
      //Only look into current users tagObj
      if (user === userId) {
        //Check to make user there is a tag
        if (_.values(tags).length > 0) {
          // console.log(tags)
            return tags;
        }
      }
    });
    //Clean Obj
    tags = _.compact(tags);

    //Gathers the tag information
    var tagInfo = _.map(tags, function (tagIds) {
      var groupTagList = _.map(tagIds, function (indvTag) {
        //Search minimongo
        var tag = Tags.findOne({user_Id: userId});
        //Find the tagObj we are looking for by its id and return it
        tag = _.find(tag.tags, function (indvTagObj) {
          if (indvTagObj.tag_Id === indvTag) {
            return indvTagObj;
          }
        });
        return tag;
      });
      return groupTagList;
    });
    //Clean tagInfo obj
    tagInfo = _.compact(tagInfo);
    tagInfo = _.flatten(tagInfo);

    return {
      hasTags: tagInfo.length !== 0 ? true : false,
      tag: tagInfo,
    };

  }
});


/***************************************************************/
/* Template Events */
/***************************************************************/
Template.tagDisplay.events({
    /*******************************/
  // Tag Remover
  /*******************************/
  'click .deleteTag': function (e, tmpl) {
    e.preventDefault();
    var tagObj = {
      tag_Id: this.tag_Id,
      doc_Id: [tmpl.data._id]
    },
    tagName = this.tagName,
    tagId = this.tag_Id;


    //Tag Fade Out Aniation
    // var tagSelector = App.dataTarget('#PrivateContainer', 'tagid', tagId);
    // TweenMax.to($(tagSelector), 0.5, {autoAlpha:0, onComplete:removeTagFromDom});


    // console.log(tagSelector)

    // function removeTagFromDom () {
    //   $.when($(tagSelector).remove()).then(removeTagFromCollection());
    // }
    removeTagFromCollection();
    function removeTagFromCollection () {
      Meteor.call('removeMessageTag', tagObj, function (error) {
        if (error) {
          console.log(error);
          App.errorMessage(error.reason);
        }else{
          App.successMessage(tagName+' tag was removed.');
        }
      });
    }

  },
});