/***************************************************************/
/* Events */
/***************************************************************/
Template.modalConfirm.events({
  'click .confirmModal_No': function(){
    return Modal.dismiss(this.action.cancelModal.call(this));
  },
  'click .confirmModal_Yes': function(){
    return Modal.dismiss(this.action.confirmModal.call(this));
  }
});