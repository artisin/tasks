/***************************************************************/
/***************************************************************/
/*  Controller: directMessageModal
/*  Template: /client/views/directMessageModal.html
/***************************************************************/
/***************************************************************/
//////////////////*********** TODO ***********//////////////////
/// 1. Validation of the message. Due to edge cases jquery will
/// not due
/// 2. Error reporting on the subject and body when value = null 
//////////////////*********** TODO ***********//////////////////

/***************************************************************/
/* Template States */
/***************************************************************/
/*******************************/
/* Template Created */
/*******************************/
Template.directMessageModal.created = function() {
  //Keeps track of how many users are in the add search list
  this.userSearchCount = new ReactiveVar(Session.get('directMessageRecipiants').length);
};
/*******************************/
/* Template Rendered */
/*******************************/
Template.directMessageModal.rendered = function() {
  $('.addNewRecipiant').dropdown({transition: 'drop'});
  return this.$('#directMessage_Form').validate({
    rules: {
      subject: {
        required: true,
        minlength: 3,
        maxlength: 60,
      }
    },
    focusCleanup: true,
    highlight: function(element) {
      $(element).parent().addClass("error");
    },
    unhighlight: function(element) {
      $(element).parent().removeClass("error");
    },
    subject: {
      firstName: {
        required: "Please enter a subject of your message",
        minlength: "Your Subject must be three characters or longer.",
        maxlength: "Your Subject cannot be longer than 60 characters."
      }
    },
  });
};

/***************************************************************/
/* Helpers */
/***************************************************************/
Template.directMessageModal.helpers({
  //Returns a collection of the recipiants that the user has
  //selected to set a message to. It is originally set when the
  //user selects recipiants from the tabel
  selectedRecipiants: function () {
    return Session.get('directMessageRecipiants');
  },
  //This is a state helper and if no recipiants are selected
  //it will both disblay a warning lable and disable the send button
  noRecipiants: function () {
    if (Session.get('directMessageRecipiants').length === 0) {
      return {
        visable: 'visable',
        state: 'disabled'
      };
    }else{
      return {
        visable: 'hidden',
        state: ''
      };
    }
  },
  //Returns a collection of users who can be added to said message
  userSearch: function () {
    var options = {
      sort: {firstName: -1},
      fields: {firstName:1, lastName: 1, _id: 1}
    };
    var userList = UserGroup.find({accountCreated: true}, options).fetch(),
        currentRecipiants = Session.get('directMessageRecipiants'),
        currentIds = _.map(currentRecipiants, function (value) {
          return value._id;
        });
    var newList = [];
    //Pushes any user that is not selected into new list
    _.each(userList, function (value) {
      if (!_.contains(currentIds, value._id)) {newList.push(value);}
    });
    //Used for the searchState method    
    Template.instance().userSearchCount.set(newList.length);
    return newList;
  },
  //Tells user if there are or are not anymore users to select
  searchState: function () {
    if (Template.instance().userSearchCount.get() === 0) {
      $('.text').text(' ');
      return {
        state: 'disabled',
        text: 'No More User To Select'
      };
    }else{
      return {
        state: '',
        text: 'Add another Recipiant'
      };
    }
  }
});

/***************************************************************/
/* Events */
/***************************************************************/
Template.directMessageModal.events({
  //Close Icon
  'click .close': function (evt) {
    evt.preventDefault();
    return Modal.dismiss();
  },
  //Action 'no' button
  'click .confirmModal_No': function(evt){
    evt.preventDefault();
    return Modal.dismiss();
  },
  //Removes user from selected list
  'click .removeUser': function (evt) {
    evt.preventDefault();
    var currentRecipiants = Session.get('directMessageRecipiants'),
        newRecipiants = [],
        self = this;   
    _.each(currentRecipiants, function (value) {
      if (value._id !== self._id) {
        newRecipiants.push(value);
      }
    });
    //Fadeout Animation
    TweenMax.to($(evt.currentTarget).parent(), 0.5, {autoAlpha:0, onComplete:removeUser});
    //Removes user after the animation
    function removeUser () {
      Session.set('directMessageRecipiants', newRecipiants);
    }
  },
  //Adds user from search input
  'click .userName': function (evt) {
    evt.preventDefault();
    var currentRecipiants = Session.get('directMessageRecipiants');
    currentRecipiants.push(this);
    Session.set('directMessageRecipiants', currentRecipiants);
    $('.text').text('Add another Recipiant');
  },
  //Adds user from search input on enter or tab keypress
  'keyup .search': function (evt) {
    evt.preventDefault();
    var results = null;
    //If enter or tab event
    if(evt.keyCode === 13 || evt.keyCode === 13){
      //Get the input name
      results = $('.text').text();
      //Extracts the first and last name
      var getNthWord = function(string, n){
          var words = string.split(" ");
          return words[n-1];
      };
      //Optiosn for collection lookup
      var options = {
        sort: {firstName: -1},
        fields: {firstName:1, lastName: 1, _id: 1}
      };
      //Name lookup
      var firstName = getNthWord(results, 1),
          lastName = getNthWord(results, 2),
          newUser = UserGroup.findOne({
            accountCreated: true,
            firstName: firstName,
            lastName: lastName
          }, options);
      //Pushes new name into recipiants
      var currentRecipiants = Session.get('directMessageRecipiants');
      currentRecipiants.push(newUser);
      Session.set('directMessageRecipiants', currentRecipiants);
      //Puts back placeholder text
      $('.text').text('Add another Recipiant');
      //Needed otherwise event runs twice
      return false;
    }
  },
  //Action 'yes' buton
  'click .confirmModal_Yes': function(evt, tmpl){
    evt.preventDefault();
    var subject = App.inputValue('subject', tmpl),
        message = App.inputValue('directMessage', tmpl),
        recipiants = _.map(Session.get('directMessageRecipiants'), function (value) {
          return value._userId;
        });

    var newMessage = {
      sender: Meteor.userId(),
      recipiants: recipiants,
      subject: subject,
      message: message
    };

    Meteor.call('newDirectMessage', newMessage, function (error, result) {
      if (error) {
        App.errorMessage(error);
        App.inputError($('.subject'), error.reason)
      }else{
        console.log(result)
      }
    });
    //return Modal.dismiss();
  },
});