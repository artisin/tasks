/***************************************************************/
/***************************************************************/
/*  Controller: modal
/*  Template: includes/helpers/modal/modal.html
/***************************************************************/
/***************************************************************/
Modal = {};
/***************************************************************/
/* Template States */
/***************************************************************/
/*******************************/
/* Template Created */
/*******************************/
Template.modal.created = function() {};
/*******************************/
/* Template Rendered */
/*******************************/
Template.modal.rendered = function() {
  positionModal($('.myModal'));
  //Posible Helper Function
  function positionModal (element){
    var bodyHeight = $('.modal-overlay').outerHeight(),
        elmHeight = element.outerHeight();
    element.css('margin-top', (bodyHeight - elmHeight)/2.5);
  }
};

/***************************************************************/
/* Helpers */
/***************************************************************/
Modal.overlay = function(options, callback){
  //Set up dynamic template and its data context
  Template.modal.helpers({
    getTemplate: function () {
      return options.template;
    },
    getDataContext: function(){
      return options.dataContext;
    }
  });
  
  //Define Vars
  var overlay = document.createElement('div'),
      $overlay = $(overlay),
      layoutContainer = $('.LayoutDefault_Container'),
      animDur= 0.5,
      tm = TweenMax,
      tl = new TimelineMax();

  //Check if there is a callback function
  var callbackCheck = App.typeofFilter('function', arguments);
  //If it is a function assigin it 
  if(callbackCheck !== null){callback = callbackCheck;}
  
  //Set the stage
  $overlay.addClass('modal-overlay');
  tm.set(overlay, {alpha:0});
  //Append template to the body
  overlay._modalView = Blaze.renderWithData(Template.modal, options, overlay);
  $('body').append(overlay);

  //Timeline/animation
  tl.to(overlay, animDur, {backgroundColor: "rgba(0,0,0,0.45)"})
  .to(overlay, animDur, {alpha: 1})
  .to(layoutContainer, 1, {webkitFilter:"blur(" +1+ "px)", force3D: true, onComplete:callback});
};

Modal.dismiss = function () {
    //Define var
    var overlay = $('.modal-overlay'),
        layoutContainer = $('.LayoutDefault_Container'),
        tl = new TimelineMax(),
        animDur = 0.5,
        callback = null;

    //Check the argument
    var callbackCheck = App.typeofFilter('function', arguments);
    //If there is a function, assign callback
    if(typeof callbackCheck === 'function'){ callback = callbackCheck}

    //Timeline/animation
    tl.to(layoutContainer, animDur, {webkitFilter:"blur(" +0+ "px)"}, "init")
      .to(overlay, animDur, {autoAlpha: 0, onComplete: cleanUp}, "init");
    
    //Cleanup/callback
    function cleanUp () {
      overlay.remove();
      if (callback !== null) {callback();}
    }
};