/***************************************************************/
/***************************************************************/
/* Helper
/* Cuz everyone needs a shoulder to lean on.
/***************************************************************/
/***************************************************************/


/***************************************************************/
/*  UI Helpers
/*  Define UI helpers for common template functionality. */
/***************************************************************/



/*******************************/
/* isAdmin Role Heler */
//Checks to see if the user is in the Admin Role 
/*******************************/
UI.registerHelper('inRole', function(role) {
    var id = Meteor.userId(),
        group = Partitioner.group()
    return Roles.userIsInRole(id, [role], group);
});



/*******************************/
/* Current Route Helper */
// {{isActiveRoute 'route name'}} ex: {{isActiveRoute 'login.link'}}
// {{isActivePath 'url path'}} ex: {{isActivePath 'products' }}
// As long as 'products' is in the url it will remain active
// The defualt class is 'active' but if you include a addition args
// those will replace defult, ex: 'test1' 'test2' ect.
/*******************************/
var routeUtils = {
  context: function() {
    return Router.current();
  },

  regex: function(url) {
    return url.split(/\/+/g);
  },

  testRoutes: function(routeName) {
    return routeName === this.context().route.getName() ? true : false;
  },

  testPaths: function(path) {
    //Extracts paths
    var reg = this.regex(Iron.Location.get().path),
        urlPath = [],
        outcome = false;
    //Checks if path is root '/'
    if (Iron.Location.get().path === path) {
      return outcome; 
    }else{
      //Pushes all parts of the url to array
      _.each(reg, function (value) {
        urlPath.push(value);
      });
      //Checks to see if path is contained in arguments
      _.each(arguments[0], function (value) {
        if (_.contains(urlPath, value)) {
          outcome = true;
        }
      });
      return outcome;
    }
  },

  classNameCheck: function (argument, defualt) {
    var className;
    if (argument.length > 2) {
      className = [];
      _.each((_.rest(argument)), function (arg) {
        if (typeof arg === 'string') {
          className.push(arg);
        }
      });
      return className;
    }else{
      className = defualt;
      return className;
    }
  }
};


UI.registerHelper('isActiveRoute', function(routes) {
  var className = routeUtils.classNameCheck(arguments, 'active');
  if (routeUtils.testRoutes(routes)) {
    return typeof className === 'string' ? className : className.join(" ");
  }else{
    className = '';
    return className;
  }
});

//Todo - add the ablity for aux classed, only active right now
UI.registerHelper('isActivePath', function() {
  var className = 'active';
  //Puts all arguments into an array to be checked by
  //the routeUtils.testPaths function
  var pathListArrry = _.map(arguments, function (value) {
    if (_.isString(value)) {return value;}
  });
  if (routeUtils.testPaths(_.compact(pathListArrry))) {
    return typeof className === 'string' ? className : className.join(" ");
  }else{
    className = '';
    return className;
  }
});


UI.registerHelper('isNotActiveRoute', function(routes) {
  var className = routeUtils.classNameCheck(arguments, 'disabled');
  console.log(this)
  if (!routeUtils.testRoutes(routes)) {
    return typeof className === 'string' ? className : className.join(" ");
  }else{
    className = '';
    return className;
  }
});


UI.registerHelper('isNotActivePath', function(paths) {
  var className = routeUtils.classNameCheck(arguments, 'disabled');
  if (!routeUtils.testPaths(paths)) {
    return typeof className === 'string' ? className : className.join(" ");
  }else{
    className = '';
    return className;
  }
});


/*******************************/
/* Dynamic Template Active Link Helper */
/*******************************/
UI.registerHelper('isDynamicTmpl', function(template) {
  var className = routeUtils.classNameCheck(arguments, 'active');
  return template === Session.get('dynamicTmpl') ? className : className = '';
});


/*******************************/
/* True Or False */
/*******************************/
UI.registerHelper("YesOrNo", function (value) {
  if (!value) {
    return "Yes";
  }else{
    return "No";
  }
});

UI.registerHelper("iffy", function (value) {
  if (value) {
    return true;
  }else{
    return false;
  }
});


/*******************************/
/* Time Helper (requires moment)*/
/*******************************/
UI.registerHelper("FormatDate", function(datetime, format) {
  var dateFormats = {
     short: "DD MMMM - YYYY",
     long: "dddd DD.MM.YYYY HH:mm"
  };
  if (moment) {
    var f = dateFormats[format];
    return moment(datetime).format(f);
  }
  else {
    return datetime;
  }
});

/*******************************/
/* Phone formate */
/*******************************/
//Extraxcts the first 10 and formats and if the string is longer then ten
//it will tack that onto the end. Allows for the following entry 2624444125 ext.23
//to be formated to (262) 444-4125 ext.23
UI.registerHelper("FormatPhone", function(phone) {
  if (phone === undefined) {return "No Phone Number On Recored";}

  //Extracts just the phone number
  var telNum = arguments[0];
      _.clean(telNum);
      _.chars(telNum);
  var extractTelly = [],
      extractOther = [];
  for (var i = 0; i < telNum.length; i++) {
    if (i < 10) {
      extractTelly.push(telNum[i]);
    }else{
      extractOther.push(telNum[i]);
    }
  }

  //Formats phone number
  function formatPhoneNumber(n) {
    var s2 = (""+n).replace(/\D/g, '');
    var m = s2.match(/^(\d{3})(\d{3})(\d{4})$/);
    return (!m) ? null : "(" + m[1] + ") " + m[2] + "-" + m[3];
  }
  var telly = formatPhoneNumber(extractTelly),
      otherInfo = _.compact(extractOther);
  return telly + otherInfo.join('');
});

/*******************************/
/* Item State Check */
/*******************************/
UI.registerHelper("itemIsChecked", function(desiredValue, itemValue) {
  if(!desiredValue && !itemValue){return ""}
  if(_.isArray(desiredValue)){
    return desiredValue.indexOf(itemValue) >= 0 ? "checked" : "";}
  return desiredValue === itemValue ? "checked" : "";
});

/*******************************/
/* Option State Check */
/*******************************/
UI.registerHelper("optionIsSelected", function(desiredValue, itemValue) {
  if(!desiredValue && !itemValue){return "";}
  if(_.isArray(desiredValue)){
    return desiredValue.indexOf(itemValue) >= 0 ? "selected" : "";}
  return desiredValue === itemValue ? "selected" : "";
});


/*******************************/
/* Identicon */
/*******************************/
Package.blaze.UI.registerHelper('identicon', function(user) {
  user = user.toString();
  var url = Gravatar.imageUrl(user, {
      size: 34,
      default: 'identicon'
  });
  return url;
});


/*******************************/
/* String To Color */
/*******************************/
Package.blaze.UI.registerHelper('colorGenerator', function() {
    var r = (Math.round(Math.random()* 127) + 127).toString(16);
    var g = (Math.round(Math.random()* 127) + 127).toString(16);
    var b = (Math.round(Math.random()* 127) + 127).toString(16);
    return "background:"+ '#' + r + g + b;

});

/*******************************/
/* Ediding States */
//For editing feilds
//Used on: editProfile
/*******************************/
Tracker.autorun(function () {
  UI.registerHelper('isEditing', function(context) {
    var value = null,
        state = null,
        isEditingArray = _.flatten(Session.get('isEditing'));
      if (_.contains(isEditingArray, context)) {
        value = "Click To Save";
        state = 'red';
      }else{
        value = "Click To Edit";
        state = 'primary';
      }
    return {
      textValue: value,
      classValue: state
    } 
  });
});

Tracker.autorun(function () {
  UI.registerHelper('isSuccess', function(context) {
    // console.log(context)
    // console.log(Session.get('isSuccess'))
    if (Session.get('isSuccess') === context) {
      Meteor.setTimeout(function () {
        Session.set('isSuccess', null);
      }, 4000);
      return true;
    }
    return false;
  });
});


/***************************************************************/
/* Custome App Helpers*/
/***************************************************************/
App = {};

/*******************************/
// Toogle Session State
/*******************************/
App.toggleSessionState = function (session) {
  var currentState = Session.get(session);
  currentState ? Session.set(session, false) : Session.set(session, true);
};


/*******************************/
/* Get account info */
/*******************************/
App.userCredentials = function () {
  var id = Meteor.userId(),
      group = Partitioner.group();
  return {
    id: id,
    group: group
  };
};


/***************************************************************/
/* JQuery */
/***************************************************************/

/*******************************/
/* Extracts input value */
/*******************************/
App.inputValue = function (nameValue, tmpl) {
  return tmpl.find('[name="'+nameValue+'"]').value;
};

/**
 * Targets data attributes
 * @param  {class} selector - for the class you wish to targe
 * @param  {string} data - data id
 * @param  {string} attr  - data attribute
 * @return {jquery obj}
 */
App.dataTarget = function (selector, data, attr) {
  return ($(selector).find('[data-' +data+ ' = "' +attr+ '"]'));
};

/***************************************************************/
/* Success Repoting Helpers */
/***************************************************************/
App.successMessage = function (successMessage) {
  return SuccessMessage.insert({successmessage: successMessage});
};



/***************************************************************/
/* Error Repoting Helpers */
/***************************************************************/

/*******************************/
/* Error Popup message */
// An error message will appear at the bottom of the screen for a few secs
/*******************************/
App.errorMessage = function (errorMessage) {
  return ErrorMessage.insert({errormessage: errorMessage});
};

/*******************************/
/* Error Label Alert
/* This will append an error class and label to an ipunt
/*******************************/
App.inputError = function (element, message) {
  //Add an error class to the element
  element.parent().addClass("error");
  //Calculate error label width
  var labelWidth = element.outerWidth();
  //Append error message to the label
  element.parent().append("<label class='error'>"+message+"</label>");
  //Set error label width
  element.find('label').css('width', labelWidth);

  //Will focus on error input after timeout
  setTimeout(function () {
    element.focus();
  }, 600);

  //
  //Two clean up methods one watches for a key down and other for a click
  //

  //Once user keydown this will clean up the error
  element.keydown(function(){
    //Fade out error label
    element.parent().find('label.error').fadeOut(400, function() {
      this.remove();
    });
    //Remove error class
    setTimeout(function(){
      element.parent().removeClass('error');
    }, 425); 
  });

  //Once user clicks this will clean up the error
  element.click(function(){
    //Fade out error label
    element.parent().find('label.error').fadeOut(400, function() {
      this.remove();
    });
    //Remove error class
    setTimeout(function(){
      element.parent().removeClass('error');
    }, 425); 
  });
};

/*******************************/
/* Remove and Add Active tab */
/*******************************/
App.changeActiveTab = function (parent, newActiveTab){
  $(parent+', .active').removeClass('active');
  $(newActiveTab).addClass('active');
};

/*******************************/
/* Type of check that returns type (true/) */
/*******************************/
App.typeofFilter = function (type){
  var list = arguments[1];
  var value = null;
  _.each(list, function(arg, iter){
    if (typeof arg === type) {
      value = list[iter];}
  });
  return value;
};

/*******************************/
/* Clean Tmpl tranz for blaze */
/*******************************/
//Refractor this
App.componentTransition = function(){
  return {
    components: function() {
      var reactiveElm = arguments[0],
          tmpl    = arguments[1],
          initElm = arguments[2],
          secElm  = arguments[3],
          animation = {
            anim: arguments[4],
            anim2: arguments[5]
          },
          callback = _.last(arguments);
      var comp = {
        _self: this,
        _reactElm: reactiveElm,
        _typeReact: null,
        _state: null,
        _tmpl: tmpl,
        _callback: callback,
        _anim: animation,
        _initElm: initElm,
        _secElm: secElm,
      };
      this.configure(comp);
    },
    configure: function(comp) {
      var rectElm = comp._reactElm;
      //Determins if you are using a session or recVar
      if (typeof rectElm === 'object') {
        comp._typeReact = 'ReactiveVar';}
      if (typeof rectElm === 'string'){
        comp._typeReact = 'Session';}
      if (rectElm === undefined) {
      console.log('Your ReactiveVar or Session should not be "undefined"');}

      //Configures animation settings
      var anim = comp._anim,
          value = _.values(anim);
      //If both undefined set defults
      if (!(value[0]===undefined && value[1]===undefined)) {
        //Check to see if first value is a numbe 
        if(typeof value[0] === 'number'){
          //If valid set time duration to said time
          comp._animDur = value[0];
        //If the first value is not a number but a string  
        }else if (typeof value[0] === 'string'){
          //Set animation duration to defualt
          comp._animDur = 1;
          //Varify string has the "word" ease in it
          //This will act as a basic filter
          if (value[0].match(/ease/g)) {
            //If all is good set ease type
            comp._animEase = value[0];
          //Throw error in console if it does not match regex
          }else{console.log('Hmm, your ease type must have the "word" ease in it  \nthe value you entered is: '+value[0]);}
        }
        //If there is a second value verify it is a string
        if(typeof value[1] === 'string'){
          //Varify string has the "word" ease in it
          //This will act as a basic filter
          if (value[1].match(/ease/g)) {
            //If all is good set ease type
            comp._animEase = value[1];
          //Throw error in console if it does not match regex
          }else{console.log('Hmm, your ease type must have the "word" ease in it  \nthe value you entered is: '+value[1]);}}
      //If the user does not include animation durration or type set defualts    
      }else{
        comp._animDur = 1;
        comp._animEase = 'ease:Linear.easeNone';
      }
      //If animation durration or ease type is still undefined then set defualt
      if(comp._animEase === undefined){comp._animEase = 'ease:Linear.easeNone';}
      if(comp._animDur === undefined){comp._animDur = 1;}

      comp._self.tempState(comp);
    },
    fadeElmOut: function (comp) {
      if(comp._state){
        //Wrap element in temporary parent container
        _.each(comp._initElm, function(x){
          x.parent().wrap( "<div class='_TempSessionContainer'></div>" );
        });
        //Fade said element
        TweenMax.to(comp._initElm, comp._animDur, {autoAlpha:0,
          ease:comp._animEase,
          onComplete:comp._self.fadeContainerOut,
          onCompleteParams:[comp]});
      }else{
        _.each(comp._secElm, function(x){
          x.parent().wrap( "<div class='_TempSessionContainer'></div>" );
        });
        TweenMax.to(comp._secElm, comp._animDur, {autoAlpha:0,
          ease:comp._animEase,
          onComplete:comp._self.fadeContainerOut,
          onCompleteParams:[comp]});
      }
    },
    fadeContainerOut: function (comp) {
      //Hide temperary parent container
      comp._container = $('._TempSessionContainer');
      TweenMax.to(comp._container, 0.1, {autoAlpha:0,
        onComplete:comp._self.stateChange,
        onCompleteParams:[comp]});
    },
    fadeContainerIn: function (comp){
      //Fade in parent container to show newly injected template
      TweenMax.to(comp._container, comp._animDur, {autoAlpha:1,
        ease:comp._animEase,
        onComplete:comp._self.removeContainer,
        onCompleteParams:[comp]});
    },
    removeContainer: function(comp){
      //Remove the temp parent container
      comp._container.removeClass();
      if (typeof comp._callback === 'function') {
        return comp._callback();
      }
    },
    tempState: function (comp){
      var currentState = null;
      //Sets current state depending on type of var
      if(comp._typeReact === 'ReactiveVar'){
        var rect = comp._reactElm;
        currentState = rect.get();}
      if (comp._typeReact === 'Session') {
        //Get current session value
        var sess = comp._reactElm;
        currentState = Session.get(sess);}

      //Sets temporary state to determin what elements to fade    
      if (!currentState) {
        comp._state = true;
        comp._self.fadeElmOut(comp);
      }else{
        comp._state = false;
        comp._self.fadeElmOut(comp);
      }
    },
    stateChange: function (comp) {
      if(comp._typeReact === 'ReactiveVar'){
        //Change Rect to oppiset value so new templates are injected,
        //Although the temporary parent container will hide the new template
        var rect = comp._reactElm,
            rectState = rect.get();
        rectState === true ? rect.set(false) : rect.set(true);}
      if (comp._typeReact === 'Session') {
      //Change session to oppiset value so new templates are injected,
      //Although the temporary parent container will hide the new template
      var sess = comp._reactElm,
          sessionState = Session.get(sess);
      sessionState === true ? Session.set(sess, false) : Session.set(sess, true);}

      comp._self.fadeContainerIn(comp);
    }
  };
};

App.animateComp = App.componentTransition();

/*******************************/
/* Simple template fade in */
// Could use some love later
/*******************************/

App.blazeTransition = function () {
return {
    container: function () {
      var tm = TweenMax,
          tl = new TimelineMax(),
          animDur = 0.75;
     
      if (_.isNumber(arguments[0])) {animDur = arguments[0];}
      
      var list = _.map(arguments, function (value) {
        return this.$(value);
      });
      
      var set = _.map(list, function (value) {
        return tm.set(value, {autoAlpha: 0});
      });
      return _.map(list, function (value, key) {
        if (key = 0) {
          tl.to(value, animDur, {autoAlpha: 1}, "init+=0.1");
        }
        tl.to(value, animDur, {autoAlpha: 1}, "init");
      });
    },
    containerDelay: function () {
      var tm = TweenMax,
          tl = new TimelineMax(),
          animDur = 1;
     
      if (_.isNumber(arguments[0])) {animDur = arguments[0];}
      
      var list = _.map(arguments, function (value) {
        return this.$(value);
      });
      
      tm.set(list, {autoAlpha: 0});
      tl.to(list, animDur, {autoAlpha: 1}, "+=0.1");

    }
  };
};

App.animateTmpl = App.blazeTransition();


/***************************************************************/
/* Cleans up Animation */
//removes all inline style attr from GSAP to prevent erros
/***************************************************************/
App.cleanAnim = function (elements) {
  return Meteor.setTimeout(function () {
    _.each(elements,function(value){
      $(value).removeAttr('style');
    });
  }, 200);
}