/***************************************************************/
/***************************************************************/
/*  AdmincInvites
/***************************************************************/
/***************************************************************/

UserGroup = new Mongo.Collection('usergroup');
Partitioner.partitionCollection(UserGroup, {index: {timestamp: 1}});
//No need for allow/deny permisions since everything done with this
//Collection is done through server side method calls

// Example.allow({
//   insert: function(userId, doc) {},
//   update: function(userId, doc, fields, modifier) {},
//   remove: function(userId, doc) {},
//   fetch: ['owner'],
//   transform: function() {}
// });

// //Deny
// Example.deny({
//   insert: function(userId, doc) {},
//   update: function(userId, doc, fields, modifier) {},
//   remove: function(userId, doc) {},
//   fetch: ['locked'],
//   transform: function() {}
// });
