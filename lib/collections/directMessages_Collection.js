Messages = new Mongo.Collection('messages');

Partitioner.partitionCollection(Messages, {index: {
    timestamp: 1,
    recipiant_Id: 1
}});

EasySearch.createSearchIndex('messages', {
  'field' : ['senderName', 'subject', 'message'],
  'collection' : Messages,
  'limit' : 20,
  'props' : {
    'filteredCategories' : []
  },
  //Sort by timestamp
  'sort': function () {
    return {timestamp: -1};
  },  
  'query' : function (searchString) {
    // Default query that will be used for searching
    var query = EasySearch.getSearcher(this.use).defaultQuery(this, searchString);

    // filter for categories if set
    if (this.props.filteredCategories.length > 0) {
      query.categories = { $in : this.props.filteredCategories };
    }

    return query;
  }
});