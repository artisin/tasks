/***************************************************************/
/***************************************************************/
/*  Route Hooks
/***************************************************************/
/***************************************************************/

//////////////////*********** TODO ***********//////////////////
/// 1. On 'http://localhost:3000/admin/users' login does not redirect
///
//////////////////*********** TODO ***********//////////////////

/***************************************************************/
/* Check if a User is Logged In */
/*
  If a user is not logged in and attempts to go to an authenticated route,
  re-route them to the login screen.
*/
/***************************************************************/
var checkUserLoggedIn = function() {
  if (!Meteor.loggingIn() && !Meteor.user()) {
    return Router.go('/login');
  } else {
    return this.next();
  }
};
Router.onBeforeAction(checkUserLoggedIn, {
  except: [
  'register.link',
  'register/:token', 
  'login.link', 
  'recover-password', 
  'reset-password']
});


/***************************************************************/
/* Check if a User Exists */

  // If a user is logged in and attempts to go to a public route, re-route
  // them to the main "logged in" screen.

/***************************************************************/
var userAuthenticated = function() {
  if (!Meteor.loggingIn() && Meteor.user()) {
    return Router.go('/');
  } else {
    return this.next();
  }
};
Router.onBeforeAction(userAuthenticated, {
  only: [
  'register.link',
  'register/:token', 
  'login.link', 
  'recover-password', 
  'reset-password']
});