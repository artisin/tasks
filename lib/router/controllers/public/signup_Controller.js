
register_Controller = RouteController.extend({
  onBeforeAction: function () {
    Session.set('userInputToken', false);
    this.next();
  },
  action: function () {
    Session.set('currentRoute', 'register');
    this.render();
  } 
});