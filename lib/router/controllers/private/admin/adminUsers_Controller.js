adminUsers_Controller = RouteController.extend({
  onBeforeAction: function () {
    //Dynamic Template 
    Session.set('dynamicTmpl', 'adminCurrentUsers');
    Session.set('dynamicContext', 'currentUserContext()');
    var user = Meteor.user(),
        group = Roles.getGroupsForUser(user).toString();
    if (Roles.userIsInRole(user, ['Admin'], group)) {
      this.next();
    }else{
      this.redirect('index.link');
      this.stop();
    }
  },
  waitOn: function () {
    var group = Partitioner.group();
    return Meteor.subscribe("usergroup", {}, group);
  },
  currentUserContext: function () {
    return {};
  },
  addUserContex: function () {
    return{};
  }, 
  data: function () {
    var self = this;
    return {
      userTabActive: function () {
        return true;
      },
      getTemplate: function () {
        return Session.get('dynamicTmpl');
      },
      getDataContext: function () {
        var context = "self."+Session.get('dynamicContext');
        return eval(context);
      }
    };
  },
  action: function () {
    this.render();
  } 
});

adminUsers_Controller.events({
  'click .currentUser': function () {
    Session.set('dynamicTmpl', 'adminCurrentUsers');
    Session.set('dynamicContext', 'currentUserContext()');
  },
  'click .addUser': function () {
    Session.set('dynamicTmpl', 'adminAddUser');
    Session.set('dynamicContext', 'addUserContex()');
  },
  'click .inviteStatus': function () {
    Session.set('dynamicTmpl', 'adminInviteStatus');
  }
});