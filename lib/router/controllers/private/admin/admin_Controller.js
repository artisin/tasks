admin_Controller = RouteController.extend({
  yieldTemplates: {
    'adminDashboard': {to: 'body'}
  },
  onBeforeAction: function () {
    var user = Meteor.user(),
        group = Roles.getGroupsForUser(user).toString();
    if (Roles.userIsInRole(user, ['Admin'], group)) {
      this.next();
    }else{
      this.redirect('index.link');
      this.stop();
    }
  },
  data: function () {
    return {
      getTemplate: function () {
        return 'adminDashboard';
      }
    }
  },
  action: function () {
    this.render();
  } 
});