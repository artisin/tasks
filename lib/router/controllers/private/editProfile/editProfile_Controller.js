editProfile_Controller = RouteController.extend({
  waitOn: function () {
    var cred = App.userCredentials()
    return Meteor.subscribe("usergroup", {_userId: cred.id}, cred.group);
    // return UserGroup.findOne({_userId: cred.id});
  },
  data: function () {
    return {
      user: function () {
        var user = UserGroup.find().fetch();
        return user[0];
      },
      editButton: function () {
        
      }
    }
  },
  action: function () {
    this.render();
  } 
}); 

// editProfile_Controller.events({
//   'click .epLastNameLable': function (evt, tmpl) {
//     console.log(this)
//     console.log(tmpl)
//     var input = $(evt.target).prev('input'),
//         nameVal = $(input[0]).attr('name'),
//         inputOrignialVal = $(input[0]).val(),
//         buttonText = $(evt.target).text();

//     $(input[0]).prop("disabled", false);
//     console.log($(input[0]).attr('name'))
//   }
// });