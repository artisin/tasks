messages_Controller = RouteController.extend({
  //////////////////*********** TODO ***********//////////////////
  /// 1. filterCursor?
  ///
  //////////////////*********** TODO ***********//////////////////
  onBeforeAction: function () {

    //Controlls if the sidebar message is active
    Session.set('isSidebarActive', 'messages');

    //Sets the default cursor options and sets route which defines cursor
    Session.set('messageCursorOptions', {sort: {timestamp: -1}});

    //Set temple context, which controls the cursor
    Session.set('messageTemplateContext', this.params.messageType);

    this.next();
  },
  //Defins Cursor
  findCursor: function () {
    var filterCursor = Session.get('messageCursorFilter'), 
        templateCursor = Session.get('messageTemplateContext'),
        params = this.params;
    // console.log(this.params)

      //*Possible verify through params
      //inbox - all messages
      if (params.messageType === 'inboxMessages') {
        return this.inboxMessages();
      }
      //New
      if (params.messageType === 'newMessages') {
        return this.newMessages();
      }
      if (params.messageType === 'readMessages') {
        return this.readMessages();
      }
      if (params.messageType === 'sentMessages') {
        return this.sentMessages();
      }
      if (params.messageType === 'tagFolder' && templateCursor === 'tagFolder') {
        return this.tagFolder(filterCursor);
      }
      //Defualt
      return this.newMessages();

  },
  //Defins Cursor Optiosn
  findOptions: function() {
    var options = Session.get('messageCursorOptions');
    return options;
  },
  subscriptions: function() {
    var subscriptions = new SubsManager(),
        group = Partitioner.group(),
        filterCursor = Session.get('messageCursorFilter');

    //Messages
    if (filterCursor !== 'noMessages') {
      this.messagesSub = subscriptions.subscribe('messages',
          this.findCursor(),
          this.findOptions(), group);
    }

    //Tags for tag searchs
    //Sort is done on client minMongo
    subscriptions.subscribe('tags', group);
  },
  //Message Cursor
  messages: function () {
    var cursor = this.findCursor(),
        itemsPerPage = Math.floor(Session.get('messageListHeight')/50)-1;
    return messagePagnation.find(cursor, {
      itemsPerPage:itemsPerPage, 
      itemSort: {timestamp: -1}});
  },
  customCursor: function (cursor, options) {
    return Messages.find(cursor, options).fetch();
  },
  //**Count on the server
  messageCount: function () {
    return Messages.find({}).count();
  },

  //Inbox Message Cursor
  inboxMessages: function () {
    var userId = Meteor.userId();
    var cursor = {recipiant_Id: userId};
    return cursor;
  },  
  //Read Message Cursor
  readMessages: function () {
    var userId = Meteor.userId();
    var cursor = {$and: [{hasRead: userId}, {recipiant_Id: userId}]};
    return cursor;
  },
  //New Message Cursor
  newMessages: function () {
    var userId = Meteor.userId();
    var cursor = {$and: [{hasNotRead: userId}, {recipiant_Id: userId}]};
    return cursor;
  },
  //Sent Message Cursor
  sentMessages: function () {
    var userId = Meteor.userId(),
        //The 'in' ensure the user has not deleted that message
        cursor = {$and: [{sender_Id: userId}, {hasRead: {$in: [userId]}}]};
    return cursor;
  },
  tagFolder: function () {
    var cursor = {_id: {$in: Session.get('messageCursorFilter')}};
    return cursor;
  },
  //Data Context
  data: function () {
      var self = this;
      return {
        messageSubReady: function () {
          return self.messagesSub.ready();
        },
        messagesToBeDisplayed: function () {
          if (self.messageCount() > 0) {
            Session.set('messagesToBeDisplayed', true);
            return true;
          }else{
            Session.set('messagesToBeDisplayed', false);
            return false;
          }
        },
        messages: function () {
          if (this.messageSubReady()) {
            return self.messages();
          }
        },
        currentCursor: function () {
          return self.findCursor();
        },
        customCursor: function (cursor, options) {
          if (this.messageSubReady()) {
            return self.customCursor(cursor, options);
          }
        },
        noMessageFound: function () {
          var page;
          switch (Session.get('messageTemplateContext')) {
            case "newMessages": 
              page = "No New Messages";
              break;
            case "readMessages":
              page = 'No Read Messages';
              break;
            default:
              page = 'No Messages Found';
          }
          return page;
        },
    };
  },
  action: function () {
    this.render();
  }
});

