/***************************************************************/
/* Config Routes */
/***************************************************************/

/*******************************/
/* Global Config */
/*******************************/

Router.configure({
  layoutTemplate: 'LayoutDefault',
  loadingTemplate: 'Loading',
  notFoundTemplate: 'notFound',
});


/*******************************/
/* Index Route */
/*******************************/

Router.route('/', {
  name: 'index.link',
  template: 'index',
  controller: 'index_Controller'
});


/***************************************************************/
/* Public Routes */
/***************************************************************/

Router.route('/register', {
  name: 'register.link',
  template: 'register',
  controller: 'register_Controller'
});

Router.route('register/:token', {
  path: '/register/:token',
  template: 'register',
  onBeforeAction: function() {
    Session.set('currentRoute', 'register');
    Session.set('emailToken', this.params.token);
    return this.next();
  }
});


Router.route('/login', {
  name: 'login.link',
  template: 'login',
  controller: 'login_Controller'
});

Router.route('recover-password', {
  path: '/recover-password',
  template: 'recoverPassword',
  onBeforeAction: function() {
    Session.set('currentRoute', 'recover-password');
    return this.next();
  }
});

Router.route('reset-password', {
  path: '/reset-password/:token',
  template: 'resetPassword',
  onBeforeAction: function() {
    Session.set('currentRoute', 'reset-password');
    Session.set('resetPasswordToken', this.params.token);
    return this.next();
  }
});


/***************************************************************/
/* Private Routes */
/***************************************************************/

/*******************************/
/* Admin */
/*******************************/

//Dashboard (main)
Router.route('/admin', {
  name: 'admin.link',
  template: 'admin',
  controller: 'admin_Controller',
});

Router.route('/admin/users', {
  name: 'adminUsers.link',
  template: 'admin',
  controller: 'adminUsers_Controller',
});


// Router.route('/messages/:_id', {
//   name: 'messages.link',
//   template: 'fullScreenMessages',
//   controller: 'messages_Controller',
// });

/*******************************/
/* Edit profile */
/*******************************/

Router.route('/editprofile', {
  name: 'editProfile.link',
  template: 'editProfile',
  controller: 'editProfile_Controller'
});


/*******************************/
/* Messages */
/*******************************/

// Router.route('/messages/:messageType/:messagesLimit?', {
Router.route('messages/:messageType/:messagePage?', {
  name: 'messages.link',
  template: 'messages',
  controller: 'messages_Controller',
});
