Schema.userGroup = new SimpleSchema({
    firstName: {
        type: String,
        label: "First Name",
        regEx: /^[a-zA-Z]{2,25}$/,
        trim: true,
        min: 2,
        max: 35,
        //Capitilize First Letter
        autoValue: function () {
            var content = this.value;
            if (this.isInsert) {
                return content.charAt(0).toUpperCase() + content.slice(1);
            }else if (this.isSet) {
                return content.charAt(0).toUpperCase() + content.slice(1);
            }else{
                this.unset();
            }
        }
    },
    lastName: {
        type: String,
        label: "Last Name",
        regEx: /^[a-zA-Z]{2,25}$/,
        trim: true,
        min: 2,
        max: 35,
        //Capitilize First Letter
        autoValue: function () {
            var content = this.value;
            if (this.isInsert) {
                return content.charAt(0).toUpperCase() + content.slice(1);
            }else if (this.isSet) {
                return content.charAt(0).toUpperCase() + content.slice(1);
            }else{
                this.unset();
            }
        }
    },
    email: {
        type: Object,
        label: "Email",
        optional: false,
        //If the user changes their email set verified to false
        autoValue: function () {
            var email = this.field('email.address');
            if (email.isSet) {
                if (this.isUpdate) {
                    return {
                        address: email.value,
                        verified: false,
                    };
                }
            }
        }
    },
    "email.address": {
        type: String,
        label: "Email Address",
        regEx: SimpleSchema.RegEx.Email,
        min: 2,
        max: 45,
        optional: false
    },
    "email.verified": {
        type: Boolean,
        label: "Email Verification",
        optional: false
    },
    phone: {
        type: Object,
        optional: true,
    },
    "phone.cell": {
        type: String,
        label: "Cell Phone",
        regEx: /.*/,
        optional: true,
        min: 10,
        max: 40,
    },
    "phone.work": {
        type: String,
        label: "Work Phone",
        regEx: /.*/,
        optional: true,
        min: 10,
        max: 40,
    },
    "phone.home": {
        type: String,
        label: "Home Phone",
        regEx: /.*/,
        optional: true,
        min: 10,
        max: 40,
    },
    displayName: {
        type: String,
        label: "Display Name",
        regEx: /^[a-zA-Z]{2,25}$/,
        trim: true, 
        optional: true,
        min: 2,
        max: 45,
    },
    role: {
        type: String, 
        optional: false
    },
    invitedSent: {
        type: Boolean,
        optional: false
    },
    accountCreated: {
        type: Boolean,
        optional: false
    },
    identicon: {
        type: String,
        optional: true
    },
    timestamp: {
        type: Date,
        optional: false
    },
    profile: {
        type: Object,
        optional: true,
    },
    "profile.location": {
        type: Object,
        optional: true
    },
    "profile.location.city": {
        type: String,
        optional: true,
        min: 2,
        max:45,
        //Capitilize First Letter
        autoValue: function () {
            var content = this.value;
            if (this.isInsert) {
                return content.charAt(0).toUpperCase() + content.slice(1);
            }else if (this.isSet) {
                return content.charAt(0).toUpperCase() + content.slice(1);
            }else{
                this.unset();
            }
        }
    },
    "profile.location.state": {
        type: String,
        min: 2,
        max:45,
        //Capitilize First Letter
        autoValue: function () {
            var content = this.value;
            if (this.isInsert) {
                return content.charAt(0).toUpperCase() + content.slice(1);
            }else if (this.isSet) {
                return content.charAt(0).toUpperCase() + content.slice(1);
            }else{
                this.unset();
            }
        }
    },
    "profile.company": {
        type: String
    },
    "profile.bio": {
        type: String,
        min: 5,
        max: 8000,
    },
    _groupId: {
        type: String,
        optional: true
    },
    _userId: {
        type: String,
        optional: true
    },
    _id: {
        type: String, 
        optional: true
    }
});

UserGroup.attachSchema(Schema.userGroup);