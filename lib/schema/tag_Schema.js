/*******************************/
/* Tag  */
/*******************************/
Schema.tags = new SimpleSchema({
    user_Id: {
      type: String,
      label: "User Id",
      optional: false
    },
    tagIds: {
      type: [String],
      label: "Tag Ids",
      optional: true
    },
    tagNames: {
      type: [String],
      label: "Tag Names",
      optional: true
    },
    tags: {
      type: [Object],
      label: "Tag Object",
      optional: false
    },
    'tags.$.tag_Id': {
      type: String,
      label: "Indv Tag Id",
      optional: false
    },
    'tags.$.tagName': {
      type: String,
      optional: false,
      label: "Indv Tag Name",
      min: 2,
      max: 25,
      //Capitilize Each Word
      autoValue: function () {
          var content = this.value;
          if (this.isInsert) {
              return _.capitalize(content);
          }else if (this.isSet) {
              return _.capitalize(content);
          }else{
              this.unset();
          }
      }
    },
    'tags.$.tagColor': {
      type: String,
      label: "Tag Color",
      optional: false
    },
    'tags.$.frequency': {
      type: Number,
      label: "Tag Frequency",
      optional: false
    },
    'tags.$.inDocs': {
      type: [String],
      label: "Tag InDocs",
      optional: true
    },
    'tags.$.group': {
      type: [String],
      label: "Tag Group",
      optional: true
    },
    _groupId: {
        type: String,
        label: "groupId",
        optional: true
    },
    _id: {
        type: String,
        label: "_id", 
        optional: true
    }
});

Tags.attachSchema(Schema.tags);