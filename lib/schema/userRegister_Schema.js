/*******************************/
/* New User Registation Schema */
/*******************************/
Schema.newUser = new SimpleSchema({
    username: {
        type: String,
        label: "username",
        regEx: /^[a-z0-9A-Z_]{3,15}$/,
        optional: false,
        max: 15,
        min: 3,
    },
    email: {
        type: String,
        label: "email",
        regEx: SimpleSchema.RegEx.Email,
        optional: false,
        max: 35
    },
    password: {
        type: String,
        label: "password",
        optional: false,
        min: 6
    },
    token: {
        type: String,
        label: "token",
        optional: true,
    }
});
