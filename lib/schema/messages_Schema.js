/*******************************/
/* Tag  */
/*******************************/
Schema.Messages = new SimpleSchema({
    timestamp: {
        type: Date,
        label: "Time Stamp",
        optional: false
    },
    senderName: {
        type: String,
        label: "Senders Name",
        optional: false
    },
    sender_Id: {
        type: String,
        label: "Sender _id",
        optional: false
    },
    recipiantList: {
        type: [Object],
        label: "Recipiant List",
        optional: false,
        blackbox: true
    },
    recipiant_Id: {
        type: [String],
        label: "Recipiant Id",
        optional: false
    },
    subject: {
        type: String,
        label: "Subject",
        optional: false,
        min: 2,
        max: 160
    },
    message: {
        type: String,
        label: "Message",
        optional: false,
        min: 2,
        max: 20000
    },
    hasNotRead: {
        type: [String],
        label: "Has Not Read",
        optional: true,
    },
    hasRead: {
        type: [String],
        label: "Has Read",
        optional: true,
    },
    stared: {
        type: [String],
        label: "Stared",
        optional: true
    },
    tags: {
        type: Object,
        label: "Tags",
        optional: true,
        blackbox: true
    },
    parent: {
        type: [String],
        label: "Parent",
        optional: true,
    },
    path: {
        type: [String],
        label: "Path",
        optional: true,
    },
    _groupId: {
        type: String,
        optional: true
    },
    _id: {
        type: String, 
        optional: true
    }
});

Messages.attachSchema(Schema.Messages);