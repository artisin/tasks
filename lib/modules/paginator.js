//***************** Note ***********************//
//Has Client Side code as well

var Paginator = function (collection, templateContext) {
    if (!(collection instanceof Mongo.Collection)) {
      throw new Error('Paginator only accepts Mongo.Collections at the moment');
    }

    var currentPageVar = new ReactiveVar(0),
        pagDataContext =  new ReactiveVar({});

    
    //Reset
    this.reset = function() {
      return currentPageVar.set(0);
    };

    
    //Find
    this.find = function(query, options) {
      var currentPage, cursor, itemsPerPage, itemsSort;
      if (query === null) {
        query = {};
      }
      if (options === null) {
        options = {};
      }
      if (options.limit || options.skip) {
        console.warn('`limit` and `skip` ignored due to pagination');
      }
      currentPage = function() {
        var tmplContext = Session.get(templateContext),
            dataContext = pagDataContext.get();
        if (dataContext[tmplContext] === undefined) {
          return 0;
        }
        return dataContext[tmplContext];
      };
      itemsPerPage = options.itemsPerPage || 10;
      itemsSort = options.itemSort || {};
      options.limit = itemsPerPage;
      options.sort = itemsSort;
      options.skip = currentPage() * itemsPerPage;
      cursor = collection.find(query, options);
      cursor.currentPage = currentPage;
      // console.log(options)
      cursor.totalPages = function() {
        var count = collection.find(query).count();
        return Math.ceil(count / itemsPerPage);
      };
      cursor.goToPage = function(pageNumber) {
        if (pageNumber >= 0 && pageNumber < cursor.totalPages()) {
          var tmplContext = Session.get(templateContext);
          //Check to see if the dataContext Containes the template instance
          if (!_.has(pagDataContext.get(), tmplContext)) {
            //Add new dataContext
            var newDataContext = {},
                key = tmplContext,
                value = pageNumber;
            newDataContext[key] = value;
            var prevContext = pagDataContext.get();
            pagDataContext.set(_.extend(prevContext, newDataContext));
          }else{
            //Modify exsisting dataContext
            var newContext = _.omit(pagDataContext.get(), tmplContext);
                newContext[tmplContext] = pageNumber;
            pagDataContext.set(newContext);
          }
          return currentPageVar.set(pageNumber);
        }
      };
      return cursor;
    };
};

//Possibly move to iron router
messagePagnation = new Paginator(Messages, 'messageTemplateContext');

