  (function($) {
    var tpI = Template.instance();
     $.fn.fixMe = function(container, tHeadPosition, tFootPosition) {
        return this.each(function() {
           var $this = $(this),
              cellSizes = [],
              containerPostition = ($(window).height()-$(container).outerHeight()),
              $t_fixed;

           //initilize
           function init() {
            $t_fixed = ($this);
            resizeFixed();
           }

           //Finds out is container is scrollable
           function isScrollable (container) {
            var innerContinaer = $(container).children()[0];
            return innerContinaer ? innerContinaer.scrollHeight > $(innerContinaer).innerHeight() : false;
           }

           //Resized cells
           function resizeFixed() {
            //Clear cell sizes
            cellSizes.length = 0;
            $t_fixed.find("tr.tableRow").each(function(index, value) {
              //Calculates the defualt size for all the cell widths through the first row.
              //This is needed not only for sizing of of the fixed srolling of the thead and tfoot
              //but more importaintly all cells widths need to be resized when there field change
              if (index === 0) {
                ////Cell width setup
                var numberOfVisableFields = tpI.data.visibleFields.get().length+1,
                    orginalTableWidth = $this.outerWidth(),
                    avgCellWidth = (Math.floor((orginalTableWidth-(82+16))/(numberOfVisableFields-1))),
                    nonAvgTableWidth = 0,
                    nonAvgCells = 0;

                console.log(avgCellWidth)
                //Determins what cells are larger than the avg 
                $.each($(this).find('td'), function (index, value) {
                  var size;
                  //First cell static
                  if (index === 0) {
                    size = 82;
                    nonAvgTableWidth += size;
                    nonAvgCells += 1;
                  }
                  //Rest of cells
                  if ($(value).outerWidth() > avgCellWidth) {
                    size = $(value).outerWidth();
                    nonAvgTableWidth += size;
                    nonAvgCells += 1;
                  }
                });
                ////Cell width calc and set
                var normalCells = numberOfVisableFields - nonAvgCells,
                    normalTableWidth = (orginalTableWidth - nonAvgTableWidth),
                    normalAvgCellWidth = Math.floor((normalTableWidth/normalCells));

                //console.log(normalAvgCellWidth)
                $.each($(this).find('td'), function (index, value) {
                  var size;
                  //First Cell, static size
                  if (index === 0) {
                    size = 82;
                  }
                  // //Handes cell size is less then 5 fields
                  // if (numberOfVisableFields < 5 && index !==0) {
                  //  size = avgCellWidth;
                  // }
                  // //If cell content width is larger than avg
                  if ($(value).outerWidth() > avgCellWidth) {
                    size = $(value).outerWidth();
                  }
                  //If cell is avg width or smaller set to avg
                  if (size === undefined) {
                    size = normalAvgCellWidth;
                  }
                  //Pushed values to array so the rest of the cells can follow suit
                  cellSizes.push(size);
                });
              }
              //Set the rest of the body cells
              $.each($(this).find('td'), function (i) {
                $(this).css({width: cellSizes[i]});
              });
            });

            //Set the thead cells widths
            $t_fixed.find("th").each(function(index) {
              $(this).css({width: cellSizes[index]});
            });

            if (isScrollable(container)) {
              var tFoot = $t_fixed.find('tfoot'),
                  tFootCells = tFoot.find('th'),
                  theadWidth = $($t_fixed.find('thead')).outerWidth(),
                  tableContainerWidth = $($(container).children()[0]).outerWidth(),
                  cellWidthSum = theadWidth;

                //Due to the damn jq scroller the initial offset has to be calced
                //otherwise on the first scroll the tfoot is 16px to long
                if (theadWidth+2 === tableContainerWidth) {
                  cellWidthSum = (theadWidth-16);
                }

              $.each(tFootCells, function (index) {
                if (index === 0) {
                  $(this).css({width: cellSizes[index]});
                }else{
                  $(this).css({width: (cellWidthSum-cellSizes[0])});
                }
              });
              tFoot.css({position: 'fixed', 
                  top: containerPostition+($(container).outerHeight()+tFootPosition)});
            }else{
              $t_fixed.find('tfoot').css({position: '', });
            }

            Session.set('recTblcellSizes', cellSizes);
            console.log(Session.get('recTblcellSizes'))
            var sum = _.reduce(Session.get('recTblcellSizes'), function(memo, num){ return memo + num; }, 0);
           console.log(sum)
           }

           //Sets postition of tHead is scrolling
           function scrollFixed() {
              //tHead Scroller
              var offset = $(this).scrollTop();
              if (offset === 0) {
                $t_fixed.find("thead").css({position: ''});
              }else{
                $t_fixed.find("thead").css({position: 'fixed', 
                  top: containerPostition+tHeadPosition});
              }
           }

           $(window).resize(resizeFixed);
           $(container).scroll(scrollFixed);
           init();
        });
     };
  })(jQuery);


var tableFixer = function (table, container) {
  var $table = $(table),
      $container = $(container),
      containerPostition = ($(window).height()-$(container).outerHeight()),
      innerContinaer = $container.children()[0],
      cellSize = [],
      self = this;
  return {
    init: function () {
      return this.resizeTableElm();
    },
    isScrollable: function (container) {
      return innerContinaer ? innerContinaer.scrollHeight > $(innerContinaer).innerHeight() : false;
    }
  }
}



        function sizeUpCells () {
          return {
            work: function (avgWidth) {
              var a = this.size(avgWidth);
              var b = this.sizeTest();
              console.log(this.size(avgWidth));
              console.log(this.sizeTest())
              if (a.nonAvgCells !== b.nonAvgCells) {
                console.log('rerun')
                this.size(this.normilize().normalAvgCellWidth)
              }else{
                console.log('finished')
              }
            },
            size: function (avgWidth) {
              var nonAvgTableWidth = 0,
                  nonAvgCells = 0,
                  nonAvgCellIndex = [];

              $.each($(firstRow).find("td"), function (index, value) {
                var size;
                //First Cell
                if (index === 0) {
                  size = firstCellWidth;
                  nonAvgTableWidth += size;
                  nonAvgCells += 1;
                }else{
                  var textValue = $(value).text(),
                      textWidth = textValue.stringWidth();
                  if (textWidth > avgWidth) {
                    size = $(value).outerWidth();
                    nonAvgTableWidth += size;
                    nonAvgCells += 1;
                    nonAvgCellIndex.push(index);
                  }
                }
              });

              return {
                nonAvgTableWidth: nonAvgTableWidth,
                nonAvgCells: nonAvgCells,
                nonAvgCellIndex: nonAvgCellIndex
              };
            },
            normilize: function () {
              var self = this;
              return {
                normalCells: numberOfVisableFields - self.size().nonAvgCells,
                normalTableWidth: (tableWidth - self.size().nonAvgTableWidth),
                normalAvgCellWidth: Math.floor((this.normalTableWidth/this.normalCells))
              };
            },
            sizeTest: function () {
              return this.size(this.normilize().normalAvgCellWidth)
            }
          }
        


                // var sizeup = function (avgWidth) {
        //  $.each($(firstRow).find("td"), function (index, value) {
      //      var size;
      //      //First Cell
      //      if (index === 0) {
      //        size = firstCellWidth;
      //        nonAvgTableWidth += size;
      //        nonAvgCells += 1;
      //      }else{
      //        var textValue = $(value).text(),
      //            textWidth = textValue.stringWidth();
      //        if (textWidth > avgWidth) {
        //        size = $(value).outerWidth();
        //        nonAvgTableWidth += size;
        //        nonAvgCells += 1;
        //        nonAvgCellIndex.push(index);
      //        }
      //      }
      //    });
      //    return nonAvgCells;
        // }

        // console.log(sizeup(avgCellWidth))
        // if (sizeup(avgCellWidth) > 1) {
        //  console.log('yes')
        //  sizeup(normalTableWidth)
        // };


        var checkCellSizes = function () {
          var avgCellWidth = getAvgCellSize(),
              nonAvgCells = false,
              nonAvgIndex = [],
              currentCellWidths = [];

          $.each($(firstRow).find("td"), function (index, value) {
            if (index !== 0) {
            var textValue = $(value).text(),
                textWidth = Math.floor(getTextWidth(textValue));
             currentCellWidths.push($(value).outerWidth());

              if (currentCellWidths.length === (tpI.data.visibleFields.get().length - 1)) {
                var max = _.max(currentCellWidths) > textWidth ? _.max(currentCellWidths) : textWidth;

                if (max > avgCellWidth) {
                  var cellSum = _.reduce(currentCellWidths, function(memo, num){ return memo + num; }, 0),
                      cellSumAvg = getAvgCellSize(currentCellWidths.length, cellSum);
                  if (textWidth > cellSumAvg) {
                    nonAvgCells = true;
                  }else{
                    nonAvgCells = true;
                    nonAvgIndex.push(index);
                  }
                }
   
              }
            }
          });
          return {
            nonAvgCells: nonAvgCells,
            nonAvgIndex: nonAvgIndex,
            currentCellWidths: currentCellWidths
          };
        };